__MemSpeed__

* Introduction
* How to compile
* How to run
* Maintainers


__INTRODUCTION__

Memory benchmark - test your memory speed


__HOW TO COMPILE__

1. Connecting via SSH to ex. pc-tbed-felix-04: ```$ ssh <username>@pc-tbed-felix-04```
2. source <your felix work directory>/software/cmake_tdaq/bin/setup.sh x86_64-slc6-gcc62-opt
3. Navigate to the mem-speed folder
4. compile :
  * g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt
  * g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DRANDOM_ACCESS
  * g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DENABLE_PREFETCH
  * g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DNUM_OF_BLOCKS=\<X\>
  * g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -march=native


__HOW TO RUN__

1. In order to give the permission to execute - run the following ```$ chmod +x mem-speed```
2. To run the mem-speed, enter ```$ mem-speed```

example of a successful run
```
[<username>@pc-tbed-felix-04 mem-speed]$ mem-speed
memcpy (OK): 2639.82 MBytes in 0.499272 s : 5.28733 GB/s
memcpy (OK): 2613.51 MBytes in 0.49935 s : 5.23383 GB/s
memcpy (OK): 2652.94 MBytes in 0.499302 s : 5.31331 GB/s
memcpy (OK): 2614.28 MBytes in 0.499349 s : 5.23538 GB/s

memcpy1KiB (OK): 2786.67 MBytes in 0.499291 s : 5.58125 GB/s
memcpy1KiB (OK): 2768.83 MBytes in 0.499348 s : 5.54488 GB/s
memcpy1KiB (OK): 2768.46 MBytes in 0.49932 s : 5.54446 GB/s
memcpy1KiB (OK): 2773.05 MBytes in 0.499351 s : 5.5533 GB/s

memcpy1KiB_v2 (OK): 2663.54 MBytes in 0.499304 s : 5.33451 GB/s
memcpy1KiB_v2 (OK): 2638.81 MBytes in 0.499343 s : 5.28456 GB/s
memcpy1KiB_v2 (OK): 2641.68 MBytes in 0.499309 s : 5.29068 GB/s
memcpy1KiB_v2 (OK): 2649.52 MBytes in 0.499352 s : 5.30593 GB/s

ompMemcpy (OK): 471.779 MBytes in 0.499353 s : 0.944781 GB/s
ompMemcpy (OK): 463.904 MBytes in 0.499296 s : 0.929116 GB/s
ompMemcpy (OK): 466.449 MBytes in 0.499348 s : 0.934116 GB/s
ompMemcpy (OK): 442.19 MBytes in 0.499295 s : 0.885628 GB/s
```


__MAINTAINERS__

Current maintainer(s):
 * mkvist - mikael.stefan.kvist@cern.ch
