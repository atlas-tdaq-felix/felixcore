// compile me :
// g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt
// g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DRANDOM_ACCESS
// g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DENABLE_PREFETCH
// g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -DNUM_OF_BLOCKS=<X>
// g++ --std=c++11 -O3 -Wall -o mem-speed mem-speed.cpp -fopenmp -lpthread -lrt -march=native
//
// or examining the assembly listing :
// g++ -S mem-speed.cpp
//
// How to run :
// taskset -c 0-7 mem-speed
//
#include <ctime>
#include <mutex>
#include <atomic>
#include <thread>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <mm_malloc.h>
#include <condition_variable>
#include <omp.h>

using namespace std;

const unsigned _1KiB = 1024;
const unsigned SZ_OF_EACH_BLOCK = (1 * _1KiB);
#ifndef NUM_OF_BLOCKS
const unsigned NUM_OF_BLOCKS = 100000;
#endif
static uint64_t* src[NUM_OF_BLOCKS];
static uint64_t* dst[NUM_OF_BLOCKS];
#ifdef RANDOM_ACCESS
static int rnd[NUM_OF_BLOCKS];
#endif

void __init__()
{
  srand(time(NULL));
  for(unsigned i=0; i<NUM_OF_BLOCKS; ++i)
  {
#ifdef RANDOM_ACCESS
    rnd[i] = rand() % NUM_OF_BLOCKS;
#endif
    src[i] = reinterpret_cast<uint64_t*>(_mm_malloc(SZ_OF_EACH_BLOCK,64));
    dst[i] = reinterpret_cast<uint64_t*>(_mm_malloc(SZ_OF_EACH_BLOCK,64));
  }
}

void __deinit__()
{
  for(unsigned i=0; i<NUM_OF_BLOCKS; ++i)
  {
    _mm_free(src[i]);
    _mm_free(dst[i]);
  }
}

class Timer
{
public:
  Timer() {
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &beg_);
  }

  double elapsed() {
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_);
    return end_.tv_sec - beg_.tv_sec +
          (end_.tv_nsec - beg_.tv_nsec) / 1000000000.;
  }

  void reset() {
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &beg_);
  }

private:
  timespec beg_, end_;
};

void copy_opaque(void* to, void* from)
{
  memcpy(to, from, SZ_OF_EACH_BLOCK);
}

void task1(atomic_bool& run,
           uint64_t& bytes_copied,
           double& elapsed)
{
  uint64_t num_of_blocks = 0;
  Timer tmr;

  for(unsigned i=0; run; )
  {
#ifdef ENABLE_PREFETCH
    __builtin_prefetch(dst);
#endif
#ifdef RANDOM_ACCESS
    copy_opaque(dst[rnd[i]], src[i]);
#else
    copy_opaque(dst[i], src[i]);
#endif
    if(++i == NUM_OF_BLOCKS)
      i = 0;
    ++num_of_blocks;
  }

  elapsed = tmr.elapsed();
  bytes_copied = num_of_blocks * SZ_OF_EACH_BLOCK;
}

// prevent gcc optimizing
#pragma GCC push_options
#pragma GCC optimize ("O0")
inline uint64_t * memcpy1KiB(uint64_t* d, const uint64_t* s)
{
  __asm__(
    "vmovups (%rsi), %ymm0\n\t"
    "vmovups 32(%rsi), %ymm1\n\t"
    "vmovups 64(%rsi), %ymm2\n\t"
    "vmovups 96(%rsi), %ymm3\n\t"
    "vmovups 128(%rsi), %ymm4\n\t"
    "vmovups 160(%rsi), %ymm5\n\t"
    "vmovups 192(%rsi), %ymm6\n\t"
    "vmovups 224(%rsi), %ymm7\n\t"
    "vmovups 256(%rsi), %ymm8\n\t"
    "vmovups 288(%rsi), %ymm9\n\t"
    "vmovups 320(%rsi), %ymm10\n\t"
    "vmovups 352(%rsi), %ymm11\n\t"
    "vmovups 384(%rsi), %ymm12\n\t"
    "vmovups 416(%rsi), %ymm13\n\t"
    "vmovups 448(%rsi), %ymm14\n\t"
    "vmovups 480(%rsi), %ymm15\n\t"
    "vmovups %ymm15, 480(%rdi)\n\t"
    "vmovups %ymm14, 448(%rdi)\n\t"
    "vmovups %ymm13, 416(%rdi)\n\t"
    "vmovups %ymm12, 384(%rdi)\n\t"
    "vmovups %ymm11, 352(%rdi)\n\t"
    "vmovups %ymm10, 320(%rdi)\n\t"
    "vmovups %ymm9, 288(%rdi)\n\t"
    "vmovups %ymm8, 256(%rdi)\n\t"
    "vmovups %ymm7, 224(%rdi)\n\t"
    "vmovups %ymm6, 192(%rdi)\n\t"
    "vmovups %ymm5, 160(%rdi)\n\t"
    "vmovups %ymm4, 128(%rdi)\n\t"
    "vmovups %ymm3, 96(%rdi)\n\t"
    "vmovups %ymm2, 64(%rdi)\n\t"
    "vmovups %ymm1, 32(%rdi)\n\t"
    "vmovups %ymm0, (%rdi)\n\t"
    "vmovups 512(%rsi), %ymm0\n\t"
    "vmovups 544(%rsi), %ymm1\n\t"
    "vmovups 576(%rsi), %ymm2\n\t"
    "vmovups 608(%rsi), %ymm3\n\t"
    "vmovups 640(%rsi), %ymm4\n\t"
    "vmovups 672(%rsi), %ymm5\n\t"
    "vmovups 704(%rsi), %ymm6\n\t"
    "vmovups 736(%rsi), %ymm7\n\t"
    "vmovups 768(%rsi), %ymm8\n\t"
    "vmovups 800(%rsi), %ymm9\n\t"
    "vmovups 832(%rsi), %ymm10\n\t"
    "vmovups 864(%rsi), %ymm11\n\t"
    "vmovups 896(%rsi), %ymm12\n\t"
    "vmovups 928(%rsi), %ymm13\n\t"
    "vmovups 960(%rsi), %ymm14\n\t"
    "vmovups 992(%rsi), %ymm15\n\t"
    "vmovups %ymm15, 992(%rdi)\n\t"
    "vmovups %ymm14, 960(%rdi)\n\t"
    "vmovups %ymm13, 928(%rdi)\n\t"
    "vmovups %ymm12, 896(%rdi)\n\t"
    "vmovups %ymm11, 864(%rdi)\n\t"
    "vmovups %ymm10, 832(%rdi)\n\t"
    "vmovups %ymm9, 800(%rdi)\n\t"
    "vmovups %ymm8, 768(%rdi)\n\t"
    "vmovups %ymm7, 736(%rdi)\n\t"
    "vmovups %ymm6, 704(%rdi)\n\t"
    "vmovups %ymm5, 672(%rdi)\n\t"
    "vmovups %ymm4, 640(%rdi)\n\t"
    "vmovups %ymm3, 608(%rdi)\n\t"
    "vmovups %ymm2, 576(%rdi)\n\t"
    "vmovups %ymm1, 544(%rdi)\n\t"
    "vmovups %ymm0, 512(%rdi)");
  return d;
}
#pragma GCC pop_options

void task2(atomic_bool& run,
           uint64_t& bytes_copied,
           double& elapsed)
{
  uint64_t num_of_blocks = 0;
  Timer tmr;

  for(unsigned i=0; run; )
  {
#ifdef ENABLE_PREFETCH
  __builtin_prefetch(dst);
#endif
#ifdef RANDOM_ACCESS
    memcpy1KiB(dst[rnd[i]], src[i]);
#else
    memcpy1KiB(dst[i], src[i]);
#endif
    if(++i == NUM_OF_BLOCKS)
      i = 0;
    ++num_of_blocks;
  }

  elapsed = tmr.elapsed();
  bytes_copied = num_of_blocks * SZ_OF_EACH_BLOCK;
}


// prevent gcc optimizing
#pragma GCC push_options
#pragma GCC optimize ("O0")
inline void memcpy1KiB_v2(void* d, const void* s)
{
  __asm__(
    "vmovups    (%rsi), %ymm0\n\t"
    "movq       %rdi, %rcx\n\t"
    "leaq       32(%rdi), %rdi\n\t"
    "vmovups	%ymm0, -32(%rdi)\n\t"
    "vmovups	992(%rsi), %ymm0\n\t"
    "vmovups	%ymm0, 960(%rdi)\n\t"
    "andq	$-32, %rdi\n\t"
    "subq	%rdi, %rcx\n\t"
    "subq	%rcx, %rsi\n\t"
    "addl	$1024, %ecx\n\t"
    "shrl	$3, %ecx\n\t"
    "rep; movsq");
}
#pragma GCC pop_options

void task3(atomic_bool& run,
           uint64_t& bytes_copied,
           double& elapsed)
{
  uint64_t num_of_blocks = 0;
  Timer tmr;

  for(unsigned i=0; run; )
  {
#ifdef ENABLE_PREFETCH
  __builtin_prefetch(dst);
#endif
#ifdef RANDOM_ACCESS
  memcpy1KiB_v2(dst[rnd[i]], src[i]);
#else
  memcpy1KiB_v2(dst[i], src[i]);
#endif
    if(++i == NUM_OF_BLOCKS)
      i = 0;
    ++num_of_blocks;
  }

  elapsed = tmr.elapsed();
  bytes_copied = num_of_blocks * SZ_OF_EACH_BLOCK;
}

// prevent gcc optimizing
// #pragma GCC push_options
// #pragma GCC optimize ("O0")
// inline void memcpy1KiB_v3(void* d, const void* s)
// {
//   __asm__(
//     "movl	$1024, %edx\n\t"
//     "jmp	memcpy\n\t");
// }
// #pragma GCC pop_options

void ompMemcpy(void* to, void* from, size_t sz) {
#pragma omp parallel num_threads(4)
{
  int nthreads = omp_get_num_threads();
  int ithread = omp_get_thread_num();
  size_t p = ithread*sz/nthreads;
  size_t n = (ithread+1)*sz/nthreads - p;
  memcpy(&reinterpret_cast<char *>(to)[p],
         &reinterpret_cast<char *>(from)[p],
         n);
}
}

void task4(atomic_bool& run,
           uint64_t& bytes_copied,
           double& elapsed)
{
  uint64_t num_of_blocks = 0;
  Timer tmr;

  for(unsigned i=0; run; )
  {
#ifdef ENABLE_PREFETCH
  __builtin_prefetch(dst);
#endif
#ifdef RANDOM_ACCESS
  ompMemcpy(dst[rnd[i]], src[i], SZ_OF_EACH_BLOCK);
#else
  ompMemcpy(dst[i], src[i], SZ_OF_EACH_BLOCK);
#endif
    if(++i == NUM_OF_BLOCKS)
      i = 0;
    ++num_of_blocks;
  }

  elapsed = tmr.elapsed();
  bytes_copied = num_of_blocks * SZ_OF_EACH_BLOCK;
}

void speed_test(
  const char* title, void (*task)(atomic_bool&, uint64_t&, double&))
{
  mutex mtx;
  double elapsed;
  bool ok = true;
  uint64_t bytes_copied;
  condition_variable cv;
  atomic_bool run(true);

  for(unsigned i=0; i<NUM_OF_BLOCKS; ++i)
  {
    memset(dst[i], 0xff, SZ_OF_EACH_BLOCK);
    memset(src[i], 0x00, SZ_OF_EACH_BLOCK);
  }

  thread t = thread([&task,&mtx,&cv,&run,&bytes_copied,&elapsed]()
  {
    task(run, bytes_copied, elapsed);
    cv.notify_one();
  });

  {
    unique_lock<mutex> lck(mtx);
    if( cv.wait_for(lck, chrono::milliseconds(500), [](){ return 0; }) )
    {
      // do nothing
    }
    run.store(false);
  }

  t.join();

  for(unsigned i=0; i<NUM_OF_BLOCKS; ++i)
  {
#ifdef RANDOM_ACCESS
    if(memcmp(dst[rnd[i]], src[i], SZ_OF_EACH_BLOCK))
#else
    if(memcmp(dst[i], src[i], SZ_OF_EACH_BLOCK))
#endif
    {
      ok = false;
      break;
    }
  }

  double gigabytes_per_second = bytes_copied / elapsed / 1e9;

  cout << title << " (" << (ok?"OK":"NOK") <<"): "
       << (bytes_copied/1e6) << " MBytes in "
       << elapsed << " s : "
       << gigabytes_per_second << " GB/s\n";
}

int main(int argc, char* argv[])
{
  __init__();
  speed_test("memcpy", task1);
  speed_test("memcpy", task1);
  speed_test("memcpy", task1);
  speed_test("memcpy", task1);
  cout << endl;
  if(SZ_OF_EACH_BLOCK == _1KiB)
  {
    speed_test("memcpy1KiB", task2);
    speed_test("memcpy1KiB", task2);
    speed_test("memcpy1KiB", task2);
    speed_test("memcpy1KiB", task2);
    cout << endl;
    speed_test("memcpy1KiB_v2", task3);
    speed_test("memcpy1KiB_v2", task3);
    speed_test("memcpy1KiB_v2", task3);
    speed_test("memcpy1KiB_v2", task3);
    cout << endl;
  }
  speed_test("ompMemcpy", task4);
  speed_test("ompMemcpy", task4);
  speed_test("ompMemcpy", task4);
  speed_test("ompMemcpy", task4);
  __deinit__();
  return 0;
}
