function isInteger(value) {
  "use strict";
  return isFinite(value) ? (value == Math.round(value)) : false;
  // Note: we use ==, not ===, as we can have Booleans as well
}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function key_value_table(parent, category, table, timeout) {
  "use strict";

  var template = document.querySelector('#key_value_table');
  var clone = document.importNode(template.content, true);

  var heading = clone.querySelector('.panel-heading');
  heading.textContent = capitalize(table);

  var element = clone.querySelector('.table');
  element.id = category+"_"+table+"_table";

  document.querySelector("#"+parent).appendChild(clone);


  var t = $('#'+category+"_"+table+"_table").DataTable( {
    "ajax": {
      "url": "./v1/json/"+category+"/"+table+"/table",
      "type": "GET",
    },
    "columns": [
      { "data": 0 },
      { "data": 1,
        "render": function ( data, type, row, meta ) {
          var value = row[1];
          var unit = row[2];
          if (typeof(value) === 'number') {
            if (unit == "") {
              return value;
            } else {
              var f = math.unit(value, unit).format(2);
              return f.substr(0,f.indexOf(' '));
            }
          }
          return value;
        },
        "className": "text-right",
      },
      { "data": 2,
        "render": function ( data, type, row, meta ) {
          var value = row[1];
          var unit = row[2];
          if (typeof(value) === 'number') {
            if (unit == "") {
              return unit;
            } else {
              var f = math.unit(value, unit).format(2);
              return f.substr(f.indexOf(' ')+1);
            }
          }
          return unit;
        },
      },
    ],
    "paging": false,
    "info": false,
    "scrollCollapse": true,
    "searching": false,
    "sort": false,
    "drawCallback": function ( settings ) {
      $("#"+category+"_"+table+"_table thead").remove();
    }
  });

  if (timeout > 0) {
    setInterval( function() {
      t.ajax.reload(null, false);
    }, timeout);
  }
  return t;
}

/**
 * Request data from the server, add it to the graph and set a timeout
 * to request again
 */
function requestHistogram(h, category, histogram, timeout) {
  "use strict";
  $.ajax({
    url: "./v1/json/"+category+"/"+histogram+"/histogram",
    success: function(result) {
      // console.log("histo: "+category+" "+histogram+" "+result);
      if (typeof result.categories != 'undefined') {
        h.xAxis[0].setCategories(result.categories, false);
      }
      var subtitle = "#: "+result.entries;
      subtitle += result.underflow > 0 ? ", u: "+result.underflow : "";
      subtitle += result.overflow > 0 ? ", o: "+result.overflow : "";
      h.setTitle({ text: result.title }, { text: subtitle }, false);
      h.xAxis[0].setTitle({ text: result.unitX }, false);
      h.yAxis[0].setTitle({ text: result.unitY }, false);
      h.series[0].setData(result.data, true);

      // call it again after one second
      setTimeout(requestHistogram, timeout, h, category, histogram, timeout);
    },
    cache: false
  });
}

function histogram(parent, category, histogram, timeout) {
  "use strict";
  var template = document.querySelector('#histogram');
  var clone = document.importNode(template.content, true);

  var element = clone.querySelector('.histogram');
  element.id = category+"_"+histogram+"_histogram";

  document.querySelector("#"+parent).appendChild(clone);

  var tooltip = {};
  if ((histogram == "elinks") || (histogram == "elinks_to_flx")) {
    tooltip = {
      formatter: function () {
        // console.log(this.x);
        if (this.y == 0) {
          return false;
        }
        return this.points.reduce(function (s, point) {
          return s + '<br/>Data: ' + point.y;
        }, '<b>' + this.x + ' ( 0x' + this.x.toString(16) + ')</b>');
      },
      shared: true
    };
  }

  var h = new Highcharts.Chart({
    chart: {
      renderTo: category+"_"+histogram+"_histogram",
      type: 'column',
      zoomType: 'x',
      height: 300
    },
    title: {
      text: '',
    },
    subtitle: {
      text: '',
    },
    legend: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    tooltip: tooltip,
    plotOptions: {
      series: {
        pointPadding: 0,
        groupPadding: 0,
        borderWidth: 0.5,
      }
    },
    xAxis: {
      title: {
        text: ''
      }
    },
    yAxis: {
      title: {
        text: ''
      }
    },
    series: [{
      name: 'Data',
      data: []
    }],
  });
  requestHistogram(h, category, histogram, timeout);
  return h;
}

function requestGraph(g, category, graph, timeout, duration, history) {
  $.ajax({
    url: "./v1/json/"+category+"/"+graph+"/graph"+(history ? "?history" : ""),
    success: function(result) {
      g.setTitle({ text: result.title }, { text: result.subtitle }, false);
      g.yAxis[0].setTitle({ text: result.unit }, false);

      // run over all the returned series
      for (i = 0; i < result.series.length; i++) {
        var id = result.series[i].name + (result.series[i].unit != "" ? " [" + result.series[i].unit + "]": "");
        // look for id in current series
        var j = 0;
        while((j < g.series.length) && (g.series[j].name != id)) {
          j++;
        }

        // not found, add series
        if (j >= g.series.length) {
          g.addSeries({
            data: [],
            name: id
          }, false);

          if (!isInteger(result.series[i].data[1])) {
            // NOTE: hc 5.0.0 seems not to pick this up yet
            g.update({
              tooltip: {
                valueDecimals: 2,
              }
            }, false);
          }
        }

        // add point
        var shift = g.series[j].data.length > duration/1000;
        var length = result.series[i].data.length;
        g.series[j].addPoint(result.series[i].data[length-1], false, shift);
      }
      g.redraw();

      // call it again after one second
      setTimeout(requestGraph, timeout, g, category, graph, timeout, duration, false);
    },
    cache: false
  });
}

function graph(parent, category, graph, timeout, duration) {
  "use strict";
  var template = document.querySelector('#graph');
  var clone = document.importNode(template.content, true);

  var element = clone.querySelector('.graph');
  element.id = category+"_"+graph+"_graph";

  document.querySelector("#"+parent).appendChild(clone);

  var g = new Highcharts.Chart({
    chart: {
        renderTo: category+"_"+graph+"_graph",
        defaultSeriesType: 'spline',
        zoomType: 'x',
        height: 300
    },
    title: {
        text: ''
    },
    legend: {
      enabled: true
    },
    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    plotOptions: {
      spline: {
          marker: {
              enabled: false
          }
      }
    },
    tooltip: {
      valueDecimals: 2,
    },
    xAxis: {
      title: {
        text: ''
      },
      type: 'datetime',
      tickPixelInterval: 150,
      maxZoom: 20 * 1000
    },
    yAxis: {
      title: {
        text: ''
      },
      minPadding: 0.2,
      maxPadding: 0.2,
    }
  });
  requestGraph(g, category, graph, timeout, duration, true);
  return g;
}

$(document).ready(function() {
  "use strict";
  var milliseconds = 1;
  var seconds = 1000;
  var minutes = 60 * seconds;
  var hours = 60 * minutes;
  var days = 24 * hours;

  var interval = 1 * seconds;
  var duration = 3 * hours;

  var byte_prefix = 'short';
  math.createUnit('B', { prefixes: byte_prefix, aliases: ['byte']}, {override: true});
  math.createUnit('Blocks', { definition: '1024 bytes', prefixes: byte_prefix });
  math.createUnit('Chunks', { prefixes: byte_prefix });

  Highcharts.setOptions({
    // This is for all plots, change Date axis to local timezone
    global : {
      useUTC : false
    }
  });

  // Configuration
  key_value_table("config_1", "configuration","general", 0);
  key_value_table("config_1", "configuration","cmd", 0);
  key_value_table("config_2", "configuration","card", 0);
  key_value_table("config_2", "configuration","debug", 0);
  key_value_table("config_3", "configuration","data_generator", 0);

  // Statistics
  key_value_table("statistics_1", "statistics","general", interval);
  key_value_table("statistics_1", "statistics","card", interval);
  key_value_table("statistics_1", "statistics","errors", interval);

  graph("statistics_2", "statistics", "memory", interval, duration);
  graph("statistics_2", "statistics", "rates", interval, duration);
  graph("statistics_2", "statistics", "queue_size", interval, duration);
  graph("statistics_2", "statistics", "throughput", interval, duration);

  histogram("statistics_3", "statistics", "elinks", interval);
  histogram("statistics_3", "statistics", "elinks_to_flx", interval);
  histogram("statistics_3", "statistics", "ports", interval);
  histogram("statistics_3", "statistics", "queues", interval);
//  histogram("statistics_3", "statistics", "seqno", interval);

  histogram("statistics_4", "statistics", "threads", interval);
  histogram("statistics_4", "statistics", "blocks_per_transfer", interval);
  histogram("statistics_4", "statistics", "chunk_lengths", interval);
  histogram("statistics_4", "statistics", "subchunk_lengths", interval);
  histogram("statistics_4", "statistics", "subchunk_types", interval);
});
