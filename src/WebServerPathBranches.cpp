/*
 * WebServerPathBranches.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: gleventi
 */

#include "WebServerPathBranches.hpp"

namespace felix {
namespace core {

std::map<std::string, pathType> WebPathsLegends::pathTypeMap;
std::map<std::string, pathName> WebPathsLegends::pathNameMap;
std::map<unsigned short int, LegendStruct> WebPathsLegends::pathLegends;

WebPathsLegends::WebPathsLegends() {
	pathTypeMap["histogram"]				= tHistogram;
	pathTypeMap["graph"]					= tGraph;
	pathTypeMap["table"]					= tTable;

	pathNameMap["elinks"]					= nElinksToHost;
	pathNameMap["elinks_to_flx"]			= nElinksFromHost;
	pathNameMap["chunk_lengths"]			= nChunksLength;
	pathNameMap["subchunk_lengths"]			= nSubchunksLength;
	pathNameMap["queues"]					= nQueues;
	pathNameMap["threads"]					= nBlocksPerThread;
	pathNameMap["blocks_per_transfer"]		= nBlocksPerTransfer;
	pathNameMap["ports"]					= nElinksPorts;
	pathNameMap["subchunk_types"]			= nSubchunkType;
	pathNameMap["memory"]					= nMemory;
	pathNameMap["rates"]					= nRates;
	pathNameMap["queue_size"]				= nQueueSize;
	pathNameMap["throughput"]				= nThroughput;
	pathNameMap["general"]					= nGeneral;
	pathNameMap["card"]						= nCard;
	pathNameMap["errors"]					= nErrors;
	pathNameMap["cmd"]						= nCmd;
	pathNameMap["debug"]					= nDebug;
	pathNameMap["data_generator"]			= nDataGenerator;

	pathLegends[tHistogram | nElinksToHost]			= {"Elinks To-Host",		"Elink #",		"Blocks"	};
	pathLegends[tHistogram | nElinksFromHost]		= {"Elinks From-Host",		"Elink #",		"Blocks"	};
	pathLegends[tHistogram | nChunksLength]			= {"Chunk Length",			"Byte #",		"Chunks"	};
	pathLegends[tHistogram | nSubchunksLength]		= {"Subchunk Length",		"Byte #",		"Subchunks"	};
	pathLegends[tHistogram | nQueues]				= {"Queue Size",			"Blocks",		""			};
	pathLegends[tHistogram | nBlocksPerThread]		= {"Threads",				"Thread #",		"Blocks"	};
	pathLegends[tHistogram | nBlocksPerTransfer]	= {"Blocks per Transfer",	"",				"Blocks"	};
	pathLegends[tHistogram | nElinksPorts]			= {"Ports",					"Elink #",		"Port #"	};
	pathLegends[tHistogram | nSubchunkType]			= {"Subchunk Types",		"",				"Subchunks"	};
}


} /* namespace core */
} /* namespace felix */


