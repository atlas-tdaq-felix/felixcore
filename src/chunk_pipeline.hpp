#ifndef FELIX_CORE_PIPELINE_CHUNK
#define FELIX_CORE_PIPELINE_CHUNK

#include <fstream>
#include <sstream>
#include <memory>
#include <random>
#include <thread>

#include "packetformat/block_format.hpp"
#include "netio/netio.hpp"
#include "felixbase/logging.hpp"
#include "felixbase/client.hpp"

#include "configuration.hpp"
#include "common.hpp"
#include "statistics.hpp"
#include "trace.hpp"

namespace felix
{
namespace core
{

/**
 * Pipeline element that outputs chunk data in a file.
 */
struct FileOutPipelineElement
{
  typedef std::shared_ptr<std::ofstream> FilePointer;
  std::map<unsigned, FilePointer> file_handles;

  FilePointer file_handle(unsigned elink)
  {
    if(file_handles.count(elink) == 0)
      {
        std::stringstream filename;
        filename << "/tmp/felix-dump-chunks-t" << std::this_thread::get_id() << "-elink-" << elink;
        file_handles[elink] = FilePointer(new std::ofstream(filename.str(), std::ofstream::app));
      }

    return file_handles[elink];
  }

  void print(const char* d, unsigned length, unsigned elink)
  {
    std::ofstream& stream = *file_handle(elink);

    for (unsigned i=0; i < length; i++)
      {
        stream << " " << std::hex << int(uint8_t(d[i]));
        if((i+1)%64 == 0)
          stream << std::endl;
      }
    stream << std::endl << std::endl;
  }

  void process(const felix::packetformat::chunk chunk, ChunkMetadata& meta)
  {
    if(!FelixConfiguration::instance().values().debug_dump_chunks)
      return;

    std::unique_ptr<char> d(chunk.data());
    std::ofstream& stream = *file_handle(meta.elink);
    stream << "Chunk of length " << std::dec << chunk.length() << ": " << std::endl;
    print(d.get(), chunk.length(), meta.elink);
  }

  void process(const felix::packetformat::shortchunk chunk, ChunkMetadata& meta)
  {
    if(!FelixConfiguration::instance().values().debug_dump_chunks)
      return;

    std::ofstream& stream = *file_handle(meta.elink);
    stream << "Shortchunk of length " << std::dec << chunk.length << ": " << std::endl;
    print(chunk.data, chunk.length, meta.elink);
  }
};


struct CleanupMessage
{
  SmartBlockPtr block;
  CleanupMessage(SmartBlockPtr block) : block(block) {}
  void operator()()
  {
    block.reset();
  }
};

struct SendMessagePipelineElement
{
  static const size_t sizeOfSIDAndMeta = sizeof(uint8_t) + (long unsigned int)16;
  std::unique_ptr<netio::tag_publisher> pub;
  bool first = true;
  bool streamEnabled = false;

  SendMessagePipelineElement()
  {
  }

  ~SendMessagePipelineElement()
  {
  }

  void setup_publisher(ChunkMetadata& meta)
  {
    if(first)
    {
      first = false;
      pub = meta.socket->get_tag_publisher(meta.elink);
      if (FelixConfiguration::instance().values().streamEnabledElinks.find(meta.elink)
          != FelixConfiguration::instance().values().streamEnabledElinks.end())
      {
        streamEnabled = true;
      }
      else
      {
        streamEnabled = false;
      }
    }
  }

  /**
    Given a chunk, if the E-link is configured as stream, it strips the SID
    and publishes the data over the SID channel.
    (Feature FLX-590.)

    @param data An array of pointers to the subchunks' data pointers.
    @param meta The metadata of the chunk (E-link, etc).
    @param chunksLength An array of sizes of the subchunks data of the chunk.
    @param chunksNum The number of subchunks the chunk contains.
    @return The amount of data sent as a message.
  */
  size_t processStream(const char* * data, const ChunkMetadata& meta, size_t* chunksLength, const size_t chunksNum) {
    if (streamEnabled) {
      uint8_t curSIDfd = *((uint8_t*)(data[1]));
      data[1] = data[1] + sizeof(uint8_t);
      // if (chunksLength[1] != 1) {
      //   std::cerr << "SID chunk was malformed or unexpected." << std::endl;
      //   return 0;
      // }
      chunksLength[1] = chunksLength[1] - sizeof(uint8_t);
      netio::message msg((uint8_t**)data, chunksLength, chunksNum);
      tracer::instance().trace_msg_send(curSIDfd);
      return meta.socket->publish(curSIDfd, msg);
    }
    return 0;
  }

  /**
    Takes a (long) already sunthesized chunk with its metadata and publishes it.

    @param chunk The chunk that will be published (whole chunk).
    @param meta The metadata of the chunk given in the previous argument
  */
  void process(felix::packetformat::chunk chunk, ChunkMetadata& meta)
  {
    setup_publisher(meta);

    felix::base::FromFELIXHeader hdr;
    hdr.length = chunk.length() + 8;
    hdr.status = 0;
    hdr.elinkid = meta.elink & 0x3F;
    hdr.gbtid = meta.elink >> 6;

    chunk.set_subchunk(0, (const char*)&hdr, sizeof(hdr));

    netio::message msg((uint8_t**)chunk.subchunks(), chunk.subchunk_lengths(), chunk.subchunk_number());
     tracer::instance().trace_msg_send(meta.elink);
    //size_t size = pub->publish(msg);
    size_t size = meta.socket->publish(meta.elink, msg);

    processStream((const char**)chunk.subchunks(), meta, (size_t*)chunk.subchunk_lengths(), chunk.subchunk_number());

    /*
     We need to make sure that blocks properly get freed. Each chunk only carries
     a pointer to the last block from which it contains data. The blocks represent
     a linked list of smart pointers. We have to break the list to make sure blocks
     get retired.
    */
    meta.last_block.cut_reference_chain();

    meta.last_block.reset();
    if (!FelixConfiguration::instance().values().nostats)
        meta.stats->fill_message_size(meta.queueIndex, size);
  }

  void process(felix::packetformat::shortchunk chunk, ChunkMetadata& meta)
  {
    setup_publisher(meta);

    felix::base::FromFELIXHeader hdr;
    hdr.length = chunk.length + 8;
    hdr.status = 0;
    hdr.elinkid = meta.elink & 0x3F;
    hdr.gbtid = meta.elink >> 6;

    const uint8_t* v_dat[2] = { (const uint8_t*)&hdr, (uint8_t*)chunk.data };
    size_t v_len[2] = { sizeof(hdr), chunk.length };

    netio::message msg(v_dat, v_len, 2);
    tracer::instance().trace_msg_send(meta.elink);
    //size_t size = pub->publish(msg);
    size_t size = meta.socket->publish(meta.elink, msg);

    processStream((const char**)v_dat, meta, v_len, (unsigned int)2);

    /*
     We need to make sure that blocks properly get freed. Each chunk only carries
     a pointer to the last block from which it contains data. The blocks represent
     a linked list of smart pointers. We have to break the list to make sure blocks
     get retired.
    */
    meta.last_block.cut_reference_chain();

    meta.last_block.reset();
    if (!FelixConfiguration::instance().values().nostats)
        meta.stats->fill_message_size(meta.queueIndex, size);
  }
};

/**
 * Copies chunks into a pre-allocated buffer. Useful as a dummy output.
 */
struct BufferCopyPipelineElement
{
  char* buffer;
  long unsigned buffer_pos = 0;
  const unsigned BUFSIZE = 65536;
  unsigned long long bytes_copied_chunk = 0;
  unsigned long long bytes_copied_shortchunk = 0;
  tbb::tick_count t0, t1;


  BufferCopyPipelineElement()
  {
    buffer = new char[BUFSIZE];
    t0 = tbb::tick_count::now();
  }

  ~BufferCopyPipelineElement()
  {
    delete[] buffer;
  }

  void dump_to_buffer(const char* data, long unsigned size)
  {
    long unsigned bytes_to_copy = size;
    while(bytes_to_copy > 0)
      {
        int n = std::min(bytes_to_copy, BUFSIZE-buffer_pos);
        memcpy(buffer+buffer_pos, data, n);
        buffer_pos += n;
        bytes_to_copy -= n;
        if(buffer_pos == BUFSIZE)
          {
            buffer_pos = 0;
          }
      }

    /*t1 = tbb::tick_count::now();
    double seconds = (t1-t0).seconds();
    if(seconds > 1)
    {
      if(bytes_copied_shortchunk)
        std::cout << "Buffer Output (shortchunk): \t" << bytes_copied_shortchunk/seconds*1e-9 << " GB/s" << std::endl;
      if(bytes_copied_chunk)
        std::cout << "Buffer Output (chunk):      \t" << bytes_copied_chunk/seconds*1e-9 << " GB/s" << std::endl;
      bytes_copied_shortchunk = 0;
      bytes_copied_chunk = 0;
      t0 = t1;
    }*/
  }

  void process(const felix::packetformat::chunk chunk, ChunkMetadata&)
  {
    auto subchunk_data = chunk.subchunks();
    auto subchunk_sizes = chunk.subchunk_lengths();
    unsigned n_subchunks = chunk.subchunk_number();

    for(unsigned i=0; i<n_subchunks; i++)
      {
        dump_to_buffer(subchunk_data[i], subchunk_sizes[i]);
        bytes_copied_chunk += subchunk_sizes[i];
      }
  }

  void process(const felix::packetformat::shortchunk chunk, ChunkMetadata&)
  {
    dump_to_buffer(chunk.data, chunk.length);
    bytes_copied_shortchunk += chunk.length;
  }
};



/**
 * Pipeline to process chunks, called by the Block Pipeline.
 */
typedef StaticPipeline<felix::packetformat::chunk, ChunkMetadata&,
        //FileOutPipelineElement,
        CountingPipelineElement,
	ElinkPipelineElement,
        SendMessagePipelineElement
        //BufferCopyPipelineElement
        > ChunkPipeline;

/**
* Pipeline to process shortchunks, called by the Block Pipeline.
*/
typedef StaticPipeline<felix::packetformat::shortchunk, ChunkMetadata&,
        //FileOutPipelineElement,
        CountingPipelineElement,
	ElinkPipelineElement,
        SendMessagePipelineElement
        //BufferCopyPipelineElement
        > ShortChunkPipeline;

}
}

#endif
