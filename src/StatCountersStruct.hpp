/*
 * statCounters.hpp
 *
 *  Created on: Oct 11, 2018
 *      Author: gleventi
 */

#ifndef FELIXCORE_SRC_STATCOUNTERSSTRUCT_HPP_
#define FELIXCORE_SRC_STATCOUNTERSSTRUCT_HPP_

#include <atomic>

namespace felix {
namespace core {

/**
 * Bitmap containing info about which of the
 * atomics the current statistics struct
 * instance is using.
 */
enum StatisticsCategory : unsigned short int {
  pipelineStats    = 0x01,
  queueSourceStats = 0x02,
  toFLXTrheadStats = 0x04,
  //has4           = 0x08,
  //has5           = 0x10,
  //has6           = 0x20,
  //has7           = 0x40,
  //has8           = 0x80,
  allStats         = 0xFF
};

class SingleCounterStruct {
public:
  SingleCounterStruct(unsigned long int mainCounterArraySize);
  ~SingleCounterStruct();

  std::atomic<unsigned long long int> * counter;
  std::atomic<unsigned long long int>   underFlowCounter = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   overFlowCounter  = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   totalCounter     = ATOMIC_VAR_INIT(0ull);
  unsigned long int counterSize;

  static  void accumulate(SingleCounterStruct*& part, SingleCounterStruct*& accumulated);
  static  void initAtomicArray(std::atomic<unsigned long long int>*& arrayPtr, unsigned long int arraySize);
};

class StatCountersStruct {
public:
  StatCountersStruct();
  virtual ~StatCountersStruct();

  std::atomic<unsigned long long int>   chunks             = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   shortchunks        = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   blocks             = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   faultyChunks       = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   sizeFaultyChunks   = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   truncatedChunks    = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   crcerrChunks       = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   malformedSubchunks = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   malformedChunks    = ATOMIC_VAR_INIT(0ull);
  std::atomic<unsigned long long int>   malformedBlocks    = ATOMIC_VAR_INIT(0ull);
  //std::atomic<unsigned long long int> seqnoTotalC        = ATOMIC_VAR_INIT(0ull);

  SingleCounterStruct * eLinkFromFelix;
  SingleCounterStruct * eLinkToFelix;
  SingleCounterStruct * chunkLength;
  SingleCounterStruct * subchunkLength;
  SingleCounterStruct * subchunkType;
  SingleCounterStruct * blocksInQueue;
  SingleCounterStruct * blocksPerThread;
  SingleCounterStruct * blocksPerTransfer;
  SingleCounterStruct * elinkPorts;
  SingleCounterStruct * messageSizeInQueue;

  std::atomic<unsigned long long int> * queueInstancesCurrentSizeC;
  std::atomic<unsigned long long int> * freeBufferC;
  std::atomic<unsigned long long int> * freeBlocksC;
//  std::atomic<unsigned long long int> * seqnoC;

  static void accumulate (StatCountersStruct& part, StatCountersStruct& accumulated);
  inline void setCategory(StatisticsCategory category) { statisticsCategory = category; }
private:
  StatisticsCategory statisticsCategory;
};

} /* namespace core */
} /* namespace felix */

#endif /* FELIXCORE_SRC_STATCOUNTERSSTRUCT_HPP_ */
