#ifndef FELIXCORE_QueueSource_HPP
#define FELIXCORE_QueueSource_HPP

#include <set>
#include <map>
#include <fstream>

#include "common.hpp"
#include "block_queue.hpp"
#include "trace.hpp"
#include "StatsN.hpp"

namespace felix {
namespace core {

class QueueSource {

public:
	QueueSource(unsigned int index, BlockQueue** buffers, BlockQueue** queues, size_t num_queues, unsigned int offset);
	virtual ~QueueSource();

	/**
	* return true to have read() called again.
	* return false to signal an error.
	*/
	virtual bool read() = 0;
	std::set<unsigned int> getElinks() { return elinks; }
	unsigned int getQueueIndex(unsigned int elink, unsigned int noOfQueues);
	StatsN& getStats() { return nstats; }
	template<class T>
	void runQueueSourceThread() {
		unsigned short int threadNo = queueSourceThreadCounter.fetch_add(1);
		felix::core::pinThreadToCPUThread(threadNo);
		populationHasStarted.store(true);
	    char tname[16];
	    snprintf(tname, 16, "source-%02d", threadNo);
		set_thread_name(queueSourceThread, tname);
		try {
			bool ok = true;
			// TODO Is this ok with the others?!?
			while (ok)
				ok = static_cast<T*>(this)->read();
			ERROR("Reader Thread died");
		} catch (std::exception& e) {
			CRITICAL("Uncaught exception in source thread: " << e.what());
			throw;
		}
	}
	static inline unsigned short int getSourceThreadsCounter() { return queueSourceThreadCounter.load(); }
	static std::atomic<bool> populationHasStarted;

protected:
	unsigned int index;
	FelixConfiguration::Values cfg;
	tracer& trace;
	StatsN nstats{queueSourceStats};
	BlockQueue** buffers;
	BlockQueue** queues;
	size_t num_queues;
	std::set<unsigned int> elinks;
	std::thread queueSourceThread;
	u_long blockSize;

	void dispatch(const Block* block);

private:
	int *queueSelector;
	unsigned offset;
	static std::atomic<unsigned short int> queueSourceThreadCounter;
};

} /* namespace core */
} /* namespace felix */

#endif
