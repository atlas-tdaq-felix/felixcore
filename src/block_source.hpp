#ifndef FELIXCORE_SMART_QUEUE_HPP
#define FELIXCORE_SMART_QUEUE_HPP

#include <istream>

#include "packetformat/block_format.hpp"
#include "statistics.hpp"

#include "block_queue.hpp"

namespace felix
{
namespace core
{

class SmartBlockPtrData;

class SmartBlockPtr
{
private:
  SmartBlockPtr(felix::packetformat::block* b, BlockQueue* buffer, unsigned queueIndex, bool shouldDelete);

public:

  SmartBlockPtr();
  SmartBlockPtr(const SmartBlockPtr& s);
  ~SmartBlockPtr();

  SmartBlockPtr& operator=(const SmartBlockPtr& other);
  felix::packetformat::block& operator*();
  felix::packetformat::block* get();
  void reset();

  unsigned use_count() const;
  void set_prev(SmartBlockPtr& p);

  void cut_reference_chain();

private:
  SmartBlockPtrData* data;

  void destroy();
public:
  friend class BlockSource;
  friend class QueueBlockSource;
};

class SmartBlockPtrData
{
public:
  SmartBlockPtrData(felix::packetformat::block* b, BlockQueue* buffer, unsigned queueIndex, bool shouldDelete) :
    pointee(b), buffer(buffer), queueIndex(queueIndex), shouldDelete(shouldDelete), count(1) {;}

  felix::packetformat::block* pointee;
  BlockQueue* buffer;
  unsigned queueIndex;
  bool shouldDelete;
  int count;
  SmartBlockPtr prev;
};


class BlockSource
{
public:
  virtual SmartBlockPtr pop() = 0;
};

}
}

#endif
