#include "ttc_generator_queue_source.hpp"

felix::core::TTCGeneratorQueueSource::TTCGeneratorQueueSource(BlockQueue** buffers, BlockQueue** queues, size_t num_queues) : QueueSource(1, buffers, queues, num_queues, 0) {
  rate_control_bool = cfg.rate_control_bool;

  limit = RateLimiter();
  period_ts = cfg.period * us;
  max_overshoot_ts = cfg.max_overshoot * ms;
  duration_ts = cfg.duration * s;

  now = limit.gettime();
  deadline = now + period_ts;

  unsigned int elink = cfg.ttc_generator;

  blockChecker = new felix::base::BlockChecker(elink, elink);


  state = new State;


  state->seqnr = 0;
  state->BCID = 0;
  state->L1ID = 0;

  elinks.insert(elink);

  queueSourceThread = std::thread(&QueueSource::runQueueSourceThread<TTCGeneratorQueueSource>, this);

}

felix::core::TTCGeneratorQueueSource::~TTCGeneratorQueueSource() {
  delete blockChecker;
  delete state;
}

bool felix::core::TTCGeneratorQueueSource::read() {

  Block* block = new Block();

  unsigned int elink = cfg.ttc_generator;

  //block header
  block->elink = elink;
  block->seqnr = state->seqnr;
  block->sob = 0xABCD;

  // increment sequence number
  state->seqnr = (state->seqnr+1) % 32;



  if (rate_control_bool) {
    if (now > deadline + max_overshoot_ts) {
      deadline = now + period_ts;

    }
    // FIXME random not set ???
    else {
      while (now < deadline) {
        now = limit.gettime();
      }

      now = limit.gettime();
      deadline += period_ts;
    }
  } else {



  }

  uint16_t i = 0;

  felix::packetformat::subchunk_trailer s;
  s.type = felix::packetformat::subchunk::BOTH;
  s.length = 20;

  for (int chunk_num = 0;chunk_num < 32; chunk_num++){

    block->data[i++] = 0x00; //FMT
    block->data[i++] = 0x00; //LEN
    block->data[i]   = (char)((state->BCID & 0x00F) << 4);
    block->data[i++] += 0x0; //reserved0
    block->data[i++] = (char)((state->BCID & 0xFF0) >> 4);
    block->data[i++] = 0x00; //XL1ID
    block->data[i++] = (char)(state->L1ID & 0xFF);
    block->data[i++] = (char)((state->L1ID & 0xFF00) >> 8);
    block->data[i++] = (char)((state->L1ID & 0xFF0000) >> 16);
    block->data[i++] = 0x00; //Orbit
    block->data[i++] = 0x00; //Orbit
    block->data[i++] = 0x00; //Orbit
    block->data[i++] = 0x00; //Orbit
    block->data[i++] = 0x00; //Trigger type
    block->data[i++] = 0x00; //Trigger type
    block->data[i++] = 0x00; //reserved1
    block->data[i++] = 0x00; //reserved1
    block->data[i++] = 0x00; //L0ID
    block->data[i++] = 0x00; //L0ID
    block->data[i++] = 0x00; //L0ID
    block->data[i++] = 0x00; //L0ID
    state->BCID = state->BCID+1;
    state->L1ID = state->L1ID+1;
    if (state->BCID == 65536){
      state->BCID = 0;
    }
    if (state->L1ID == 65536){
      state->L1ID = 0;
    }
    //trailer for each subchunk
    block->data[i++] = (char)(s.length & 0xFF);
    block->data[i] = (char)(s.type << 5);
    block->data[i++] += (char)((s.length & 0x300) >> 8);
    //Null trailer1
    block->data[i++] = 0x00;
    block->data[i++] = 0x00;
    //Null trailer2
    block->data[i++] = 0x00;
    block->data[i++] = 0x00;
    //Null trailer3
    block->data[i++] = 0x00;
    block->data[i++] = 0x00;
    if (chunk_num > 0){
      //Null trailer4
      block->data[i++] = 0x00;
      block->data[i++] = 0x00;
      //Null trailer5
      block->data[i++] = 0x00;
      block->data[i++] = 0x00;
    }
  }

  if (i != 1020) {
    ERROR("SOMETHING WRONG"<<i);
    return false;
  }

  if (!blockChecker->check(block)) {
    return false;
  }

  dispatch(block);
  if(cfg.pmstats && cfg.nostats) {
  }

  delete block;

  return true;
}
