#include <iostream>
#include <algorithm>
#include <string>

#include <unistd.h>

#include "felixtag.h"

#include "netio/netio.hpp"
#include "felixbase/client.hpp"

using felix::base::BusyMessage;

const char* HELP =
    R"FOO(Usage: felix_busy [options] MESSAGE

Controls BUSY status of FELIX hosts.

MESSAGE can be one of:
  XON        - Set XON for the given link
  XOFF       - Set XOFF for the given link
  BUSYON     - Set global BUSY
  BUSYOFF    - Release global BUSY

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12341.
  -e ELINK        Control XON/XOFF of the specified e-link. Default: 0.
  -V              Display version.
  -h              Display this help message.
)FOO";


int
main(int argc, char** argv)
{
	std::string host = "127.0.0.1";
	unsigned short port = 12341;
	std::string backend = "posix";
	uint64_t elink = 0;

	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:e:Vh")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'e':
			elink = atoll(optarg);
			break;
		case 'V':
			std::cerr << "Version: " << FELIX_TAG << std::endl;
			return -1;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

  std::string command;
  if(optind < argc)
  {
    command = argv[optind];
  }
  std::transform(command.begin(), command.end(), command.begin(), toupper);

  BusyMessage msg;
  msg.elink = elink;
  if(command == "XON") {
    msg.command = BusyMessage::XON;
  } else if(command == "XOFF") {
    msg.command = BusyMessage::XOFF;
  } else if(command == "BUSYON") {
    msg.command = BusyMessage::BUSYON;
  } else if(command == "BUSYOFF") {
    msg.command = BusyMessage::BUSYOFF;
  } else {
    std::cerr << "Command '" << command << "' not recogonized." << std::endl;
    return 1;
  }

  netio::context ctx(backend.c_str());
	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});

	netio::low_latency_send_socket socket(&ctx);

	socket.connect(netio::endpoint(host, port));
  netio::message m((uint8_t*)&msg, sizeof(BusyMessage));
  socket.send(m);

	ctx.event_loop()->stop();
	bg_thread.join();

	return 0;
}
