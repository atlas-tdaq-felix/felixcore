#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <atomic>
#include <unistd.h>

#include "felixtag.h"

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: fm_client [options]

Connects to a remote FULL-mode FELIX host and subscribes to all links.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12350.
  -t THREADS      Number of threads (=ports) of the felixcore instance. Default: 2.
  -i              Interactive mode, prints received messages to the screen.
  -z              Enable zero-copy mode.
  -V              Display version.
  -h              Display this help message.
)FOO";

unsigned ELINKS[] = {
   0*64, 1*64, 2*64, 3*64, 4*64, 5*64,
   2048+0*64, 2048+1*64, 2048+2*64, 2048+3*64, 2048+4*64, 2048+5*64
 };

 inline void set_thread_name(const char* name, unsigned n)
 {
     char tname[16];
     snprintf(tname, 16, "fm-%s-%d", name, n);
     pthread_setname_np(pthread_self(), tname);
 }

void subscriber_thread(std::string backend, const char* host, unsigned start_port, unsigned n_ports, netio::tag elink, std::atomic_ulong& bytes_received, std::atomic_ulong& messages_received, bool zerocopy)
{
  netio::context ctx(backend.c_str());
  std::thread bg_thread([&ctx, elink](){
    set_thread_name("evl", elink);
    ctx.event_loop()->run_forever();
  });

  set_thread_name("sub", elink);

  auto cfg = netio::sockcfg::cfg();
  if(zerocopy) {
    cfg(netio::sockcfg::ZERO_COPY);
  }

  netio::subscribe_socket socket(&ctx, cfg);
  for(unsigned i=0; i<n_ports; i++)
  {
    socket.subscribe(elink, netio::endpoint(host, start_port+i));
  }
  messages_received.store(0);
  bytes_received.store(0);

  while(true)
  {
    netio::message m;
    socket.recv(m);
    messages_received.fetch_add(1);
    bytes_received.fetch_add( m.size());
  }
}



int
main(int argc, char** argv)
{
	std::string host = "127.0.0.1";
	unsigned short port = 12350;
	std::string backend = "posix";
  unsigned num_ports = 2;
  bool zerocopy = false;
  std::vector<std::thread> subscriber_threads;
  std::atomic_ulong messages_received_vec[12];
  std::atomic_ulong bytes_received_vec[12];

    char opt;
  	while ((opt = getopt(argc, argv, "b:H:t:p:zVh")) != -1)
  	{
  		switch (opt)
  		{
  		case 'b':
  			backend = optarg;
  			break;
  		case 'H':
  			host = optarg;
  			break;
      case 't':
        num_ports = atoi(optarg);
        break;
  		case 'p':
  			port = atoi(optarg);
  			break;
      case 'z':
        zerocopy = true;
        break;
  		case 'h':
  		default:
  			std::cerr << HELP << std::endl;
  			return -1;
  		}
  	}

  for(unsigned i=0; i<12; i++)
  {
    subscriber_threads.emplace_back(subscriber_thread,
      backend, host.c_str(), port, num_ports,
      ELINKS[i], std::ref(bytes_received_vec[i]), std::ref(messages_received_vec[i]),
      zerocopy);
  }

  typedef std::chrono::high_resolution_clock clock;
	auto t0 = clock::now();

  while(true)
  {
    sleep(1);
    unsigned long long messages_received = 0;
    unsigned long long bytes_received = 0;
    for(unsigned i=0; i<12; i++)
    {
      unsigned long long m = messages_received_vec[i].exchange(0);
      unsigned long long b = bytes_received_vec[i].exchange(0);
      messages_received += m;
      bytes_received += b;
    }

    auto t1 = clock::now();
    double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
    std::cout << "timestamp:          " << std::chrono::duration_cast<std::chrono::seconds>(t1.time_since_epoch()).count() << std::endl;
    std::cout << "message rate [MHz]: " << messages_received/seconds/1000./1000. << std::endl;
    std::cout << "data rate [GB/s]:   " << bytes_received/seconds/1024./1024./1024. << std::endl;
    std::cout << "data rate [Gb/s]:   " << bytes_received/seconds/1024./1024./1024.*8 << std::endl;
    std::cout << std::endl;
    t0 = t1;
  }
}
