#include "felixtag.h"

#include "netio/netio.hpp"
#include "felixbase/client.hpp"

using felix::base::ToFELIXHeader;

#include <iostream>
#include <sstream>
#include <csignal>

const char* HELP = R"FOO(Usage: felix_dcs [options]

Connects to a remote FELIX host and sends a DCS message to an e-link. The
message is either given via the -m command line parameter, or read from
standard input, or generated in a loop with -g option. In case of standard
input, each line is treated as a separate message. With -g option, the messages
contain an increasing uint64_t (starting from 0).

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12340.
  -e ELINK        Send data to the specified e-link. Default: 0.
  -m MSG          The message that is sent to the e-link. Default: read from stdin.
  -g GROUP_SIZE   Send generated data in a loop (ctrl+c to stop), GROUP_SIZE
                  messages at a time.
  -n              Number of netio messages to send when using the -g option
  -V              Display version.
  -h              Display this help message.
)FOO";


std::atomic<bool> running;
static void signal_handler(int sig)
{
	std::cout << "terminating on signal: " << sig << std::endl;
	running.store(false);
}


static void
send_dcs_msg(netio::low_latency_send_socket& socket, uint64_t elink, std::string str)
{
	std::vector<const uint8_t*> data;
	std::vector<size_t> sizes;

	ToFELIXHeader header;
	header.elinkid = elink;
	header.length = str.size();

	data.push_back((uint8_t*)&header);
	sizes.push_back(sizeof(ToFELIXHeader));

	data.push_back((uint8_t*)str.c_str());
	sizes.push_back(str.size());

	netio::message m(data, sizes);
	socket.send(m);
}

#if 0 // used only in desperate cases
#include <iomanip>

std::string encode_msg_data_hex(std::vector<uint8_t> const & data)
{
	std::stringstream output;

	for(unsigned i = 0; i < data.size(); i++)
	{
		if(i%32 == 0)
		{
			if(i != 0)
				output << '\n';
			output << "0x" << std::hex << std::setfill('0') << std::setw(8) << i << ": ";
		}
		output << std::hex << std::setfill('0') << std::setw(2) << ((unsigned int)data[i] & 0xFF) << " " << std::dec;
	}
	return output.str();
}
#endif

void loop_send_grouped_msg(netio::low_latency_send_socket& socket,
		uint64_t elink, unsigned group_size, unsigned nb_msg_to_send)
{
	uint64_t sent_bytes = 0, sent_msg = 0;;
	uint64_t contents = 0;
	std::vector<uint8_t> data;

	while (running.load() && ((nb_msg_to_send == 0) || (sent_msg < nb_msg_to_send))) {
		data.clear();

		for (unsigned i = 0; i < group_size; ++i) {
			ToFELIXHeader hdr;
			hdr.elinkid = elink;
			hdr.reserved = 0;
			hdr.length = sizeof(uint64_t);

			for (uint8_t* p = (uint8_t*)(&hdr); p < ((uint8_t*)(&hdr))+sizeof(ToFELIXHeader); ++p)
			data.push_back(*p);
			for (std::size_t i = 0; i < sizeof(uint64_t); ++i)
			data.push_back(*(((uint8_t*)&contents)+i));

			++contents;
		}

		netio::message m(data.data(), data.size());

		socket.send(m);
		if (socket.is_closed()) {
			std::cout << "error sending data: connection lost" << std::endl;
			running.store(false);
		} else {
			sent_bytes += m.size();
			sent_msg += 1;
		}

		usleep(10);
	}

	std::cout << "total sent: " << sent_bytes << " B, " << sent_msg << " msg"<< std::endl;
}

int
main(int argc, char** argv)
{
	std::string host = "127.0.0.1";
	unsigned short port = 12340;
	std::string backend = "posix";
	uint64_t elink = 0;
	std::string cli_msg = "";
	bool generate_data = false;
	unsigned group_size = 0;
	unsigned nb_msg_to_send = 0;

	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:e:m:Vhg:n:")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'e':
			elink = atoll(optarg);
			break;
		case 'm':
			cli_msg = optarg;
			break;
		case 'n':
			nb_msg_to_send = atoll(optarg);
			break;
		case 'g':
		{
			generate_data = true;
			std::istringstream iss(optarg);
			iss >> group_size;
			if (!iss) {
				std::cerr << "error: group size must be an unsigned integer" << std::endl;
				return -1;
			}
			break;
		}
		case 'V':
			std::cerr << "Version: " << FELIX_TAG << std::endl;
			return 0;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	netio::context ctx(backend.c_str());
	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});

	netio::low_latency_send_socket socket(&ctx);
	socket.connect(netio::endpoint(host, port));

	if (generate_data) {
		running.store(true);
		std::signal(SIGINT, signal_handler);
		loop_send_grouped_msg(socket, elink, group_size, nb_msg_to_send);
	} else if (cli_msg != "")
	{
		send_dcs_msg(socket, elink, cli_msg);
	} else
	{
		std::string str;
		while(std::getline(std::cin, str)) {
			send_dcs_msg(socket, elink, str);
		}
	}

	socket.disconnect();

	ctx.event_loop()->stop();
	bg_thread.join();

	return 0;
}
