/*
 * definitions.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: gleventi
 */

#include "definitions.hpp"

#include "card_queue_source.hpp"				// getMaxeLinksGeneral
#include "packetformat/block_format.hpp"		// getBlockSize
#include "configuration.hpp"					// getMaxThreads

unsigned short int felix::core::Definitions::numberOfDevices =	(unsigned short int)felix::core::CardQueueSource::getNumberOfDevices();

size_t							getBlockSize()					{ return felix::packetformat::BLOCKSIZE; }

unsigned short int				getNoOfDevices()				{ return felix::core::Definitions::numberOfDevices; }

unsigned long int				getMaxThreads()					{ return felix::core::FelixConfiguration::instance().values().threads; }

unsigned int					getAvailableHardwareCores()		{ return std::thread::hardware_concurrency(); }

unsigned short int				getNoOfSourceThreads()			{ unsigned short int result = felix::core::FelixConfiguration::instance().values().devices.size();
																	return result > 0 ? result : getNoOfDevices(); }
