#include "toflx.hpp"
#include "common.hpp"
#include "trace.hpp"

#include <thread>
#include <functional>
using namespace std::placeholders;

using felix::base::ToFELIXHeader;

felix::core::ToFLXThread::ToFLXThread(unsigned int card_no, unsigned short port, int toflx_dmaid, int toflx_format)
  : cfg(FelixConfiguration::instance().values()),
    continuous_dma(cfg.toflx_cdma),
    initialized(false),
    card_no(card_no),
    port(port),
    ctx("posix"),
    socket(&ctx, port,
      std::bind(&ToFLXThread::toflx_callback, this, std::placeholders::_1, std::placeholders::_2),
        netio::sockcfg::cfgll()
          (netio::sockcfg::ZERO_COPY)
    ),
    stats(toFLXTrheadStats),
    flxcard_max_tlp(0),
    dma_id(toflx_dmaid),
    fromhost_data_format(toflx_format),
    cardbuf_ptr(nullptr)
{
  if(cfg.toflx_cdma){
    INFO("To-FLX thread initialised with continuos DMA transfer");
    this->transfer_callback(std::bind(&ToFLXThread::transfer_buffer_via_continuous_dma, this, std::placeholders::_1,  std::placeholders::_2));
  }
  else{
    INFO("To-FLX thread initialised with one-shot DMA transfer");
    this->transfer_callback(std::bind(&ToFLXThread::transfer_buffer_via_dma, this, std::placeholders::_1,  std::placeholders::_2));
  } 
}

felix::core::ToFLXThread::~ToFLXThread()
{
  flxcard.dma_stop(dma_id);
  CMEM_SegmentFree(cmem_handle);
  CMEM_Close();
  flxcard.card_close();

  ctx.event_loop()->stop();
  bg_thread->join();
}


void felix::core::ToFLXThread::start() {
  bg_thread = std::unique_ptr<std::thread>(new std::thread([&](){
                                                             ctx.event_loop()->run_forever();
                                                           }));
  set_thread_name(*bg_thread, "ToFLX");
}


void
felix::core::ToFLXThread::alloc_cmem()
{
  int ret = CMEM_Open();

  if (!ret)
    // To make sure there is always enough space to align data to the next TLP,
    // size = TOFLX_CMEM_BUFFERSIZE + flxcard_max_tlp
    //ret = CMEM_GFPBPASegmentAllocate(TOFLX_CMEM_BUFFERSIZE + flxcard_max_tlp, (char*)"FelixCore", &cmem_handle);
    // We assume the minimum size of a message to send is equal to the set TLP size (i.e. 32 bytes = 256 bits)
    ret = CMEM_GFPBPASegmentAllocate(TOFLX_CMEM_BUFFERSIZE, (char*)"FelixCoreToFlx", &cmem_handle);

  if (!ret)
    ret = CMEM_SegmentPhysicalAddress(cmem_handle, &cmem_paddr);

  if (!ret)
    ret = CMEM_SegmentVirtualAddress(cmem_handle, &cmem_vaddr);

  if (!ret)
    ret = CMEM_SegmentSize(cmem_handle, &cmem_size);

  cmem_paddr_end = cmem_paddr + cmem_size;
  cmem_vaddr_write = cmem_vaddr;

  if (ret) {
    ERROR("Could not allocate CMEM buffer for To-FLX path");
    rcc_error_print(stdout, ret);
    exit(EXIT_FAILURE);
  }
}

void
felix::core::ToFLXThread::transfer_callback(TransferCallback transfer){
  transfer_buffer = transfer;
}

static void
reject(netio::endpoint& ep, std::string reason)
{
  WARNING("Rejected message from " << ep.address() << ":" << ep.port()
          << " ('" << reason << "')");
}

void
felix::core::ToFLXThread::toflx_callback(netio::endpoint& ep, netio::message& msg)
{
  if(!initialized) {
    INFO("Opening DEVICE " << card_no << " for the To-FLX thread");
    flxcard.card_open(card_no, LOCK_NONE);
    //INFO("The TLP function returns " << flxcard.dma_max_tlp_bytes());
    flxcard_max_tlp = 32; //flxcard.dma_max_tlp_bytes(); returns the tohost value
    alloc_cmem();

    if(continuous_dma){
      // Initiate a continuous-mode DMA of the CMEM buffer
      DEBUG("Going to call dma_from_host");
      flxcard.dma_from_host(dma_id, cmem_paddr, cmem_size, FLX_DMA_WRAPAROUND );
      dma_softw_ptr = flxcard.dma_get_read_ptr(dma_id);
      DEBUG("toflx_callback: dma_softw_ptr=" << std::hex << dma_softw_ptr);
    }

    initialized = true;
  }

  DEBUG("Received message from " << ep.address() << ":" << ep.port()
        << ", size="<<msg.size() << ", parts="<<msg.num_fragments());

  if(msg.size() < sizeof(ToFELIXHeader)) {
    reject(ep, "Message too small");
    return;
  }

  size_t size = msg.size();
  if(size > TOFLX_MAX_RECV_SIZE) {
    reject(ep, "Message too large");
    return;
  }

  msg.serialize_to_usr_buffer(msgbuf.data());

  // reset output buffer
  cardbuf_ptr = cardbuf.data();

  size_t pos = 0;
  ToFELIXHeader* header = nullptr;

  while(pos < size) {
    if(size-pos < sizeof(ToFELIXHeader)) {
      reject(ep, "Leftover data in NetIO message too small to contain a ToFELIXHeader");
      return;
    }

    header = (ToFELIXHeader*)(msgbuf.data()+pos);
    pos += sizeof(ToFELIXHeader);

    if(size-pos < header->length) {
      reject(ep, "Message is truncated");
      return;
    }

    if(header->length == 0) {
      reject(ep, "Illegal length, header->length is zero");
      return;
    }
    handle_message(*header, msgbuf.data()+pos);
    pos += header->length;
  }

  if (cardbuf_ptr != cardbuf.data()) {
    //felix::base::dump_buffer((u_long)cardbuf.data(), (size_t)bufsize);
    //tracer::instance().trace_fromhost_block_write_start(header->elinkid);
    transfer_buffer(cardbuf.data(), cardbuf_ptr - cardbuf.data());
    //tracer::instance().trace_fromhost_block_write_complete(header->elinkid);
    cardbuf_ptr = cardbuf.data();
  }
}


void
felix::core::ToFLXThread::handle_message(ToFELIXHeader header, uint8_t* data)
{
  DEBUG("MSG received for elink " << header.elinkid);

  tracer::instance().trace_msg_recv(header.elinkid);

  int bufsize;

  while ((bufsize = felix::base::encode_data(data,
                                             header.length,
                                             cardbuf_ptr,
                                             TOFLX_CMEM_BUFFERSIZE - (cardbuf_ptr - cardbuf.data()),
                                             header.elinkid,
                                             fromhost_data_format)) < 0)
    {
      if (cardbuf_ptr == cardbuf.data()) {
        // not enough space to encode a single message: too small
        ERROR("Card message buffer too small (size=" << TOFLX_CMEM_BUFFERSIZE << ") for the data requested."
              " Returning...");
        return;
      } else {
        // buffer is full: transfer and reset it
        //tracer::instance().trace_fromhost_block_write_start(header.elinkid);
        transfer_buffer(cardbuf.data(), cardbuf_ptr - cardbuf.data());
        //tracer::instance().trace_fromhost_block_write_complete(header.elinkid);
        cardbuf_ptr = cardbuf.data();
      }
    }

  cardbuf_ptr += bufsize;

  int const no_of_blocks = bufsize/TOGBT_BLOCK_BYTES;
  for(int block = 0; block < no_of_blocks; ++block) {
    if(!cfg.nostats && !cfg.nohistos) {
      stats.eLinkToFelixIncr(header.elinkid);
    }
  }

  DEBUG("MSG encoded size=" << (size_t)bufsize);
}

// Keeping the following function for reference:
void
felix::core::ToFLXThread::transfer_buffer_via_dma(char* buf, size_t size)
{
  memcpy((void*)cmem_vaddr, buf, size);

  // Round up to the nearest TLP so that all data are consumed at once
  // We have enough space by construction: see alloc_cmem(..)
  size_t const padded_size = ((size - 1) / flxcard_max_tlp + 1) * flxcard_max_tlp;
  for (size_t i = size; i < padded_size; ++i) {
    ((uint8_t*)cmem_vaddr)[i] = 0xFF;
  }

  //DEBUG("Dumping buffer before transfer");
  //felix::base::dump_buffer(vaddr, padded_size);

  try {
    DEBUG("one-shot: toflx card " << card_no << ", dma transfer: size = " << padded_size);

    size_t to_transfer = padded_size;
    ulong transfer_ptr = cmem_paddr;
    while (to_transfer > 0) {
      int const transfer_size = to_transfer;
      DEBUG("toflx card " << card_no << ", transferring fragment: ptr = 0x"
            << std::hex << transfer_ptr << std::dec
            << ", size = " << transfer_size);

      flxcard.dma_from_host(dma_id, transfer_ptr, transfer_size, 0);

      DEBUG("toflx card " << card_no << ", waiting for dma transfer to finish...");
      flxcard.dma_wait(dma_id);
      DEBUG("toflx card " << card_no << ", fragment dma transfer finished");

      transfer_ptr += transfer_size;
      to_transfer -= transfer_size;
    }

    DEBUG("toflx card " << card_no << ", dma transfer finished");
  } catch (const FlxException& e) {
    DEBUG("DMA transfer to FLX card " << card_no << " failed: " << e.what());
  }
}


void
felix::core::ToFLXThread::transfer_buffer_via_continuous_dma(char* buf, size_t size)
{
  DEBUG("cDMA toflx card " << card_no << ", DMA ID = " << dma_id << ", message size = " << size);

  // Check for DMA in-progress
  // (and wait if it is, although in fact we could continue filling the buffer until full,
  //  so we need a time-out!)
  u_long t_total = 0;
  bool timeout = false;
  bool dma_in_progress = true;

  while( dma_in_progress ) {
    
    u_long curr_addr = flxcard.dma_get_current_address(dma_id);

    // This condition should be sufficient:
    dma_in_progress = (curr_addr != dma_softw_ptr);

    // Time out after 1 sec ?
    // (Good choice? same as transfer_buffer_via_dma() when FlxCard::dma_wait() is called)
    if( t_total > 1000000 ) {
      timeout = true;
      dma_in_progress = false;
    }

    if( dma_in_progress ) {
      // Wait: roughly the time it would take to transfer the remaining size
      // into an 8-bit 8b10b E-link @ 320Mb/s (32 bytes/s);
      // it doesn't make sense to check sooner
      u_long t; // In microseconds
      if( curr_addr < dma_softw_ptr )
        t = (curr_addr - dma_softw_ptr)/32;
      else
        t = ((cmem_size + dma_softw_ptr) - curr_addr)/32;
      if( t > 1 ) { // No wait for 32 bytes or less
        t > 1000000 ? t = 1000000 : t = t; 
        usleep( t );
        t_total += t;
      }
      else {
        // No reason to wait any longer
        dma_in_progress = false;
      }
    }
  }
  
  if( timeout ) {
    ERROR("DMA transfer to FLX card " << card_no << " timed out");
    //return;
  }

  // Copy buffer into the CMEM buffer
  if( dma_softw_ptr + size >= cmem_paddr_end ) {

    // Buffer wrap-around: split copy operation into CMEM buffer into two segments
    u_long subsize = cmem_paddr_end - dma_softw_ptr;
    memcpy((void*)cmem_vaddr_write, buf, subsize);
    buf += subsize;
    cmem_vaddr_write = cmem_vaddr;

    // Second segment
    subsize = size - subsize;
    if( subsize > 0 ) {
      memcpy((void*)cmem_vaddr_write, buf, subsize);
      cmem_vaddr_write += subsize;
    }

    // Update our copy of the DMA pointer
    DEBUG("ToFlx DMA buffer WRAPAROUND");
    dma_softw_ptr += size;
    dma_softw_ptr -= cmem_size;

  } else {

    // Copy into CMEM buffer
    memcpy((void*)cmem_vaddr_write, buf, size);
    cmem_vaddr_write += size;

    // Update our copy of the DMA pointer
    dma_softw_ptr += size;
  }

  // Update the DMA controller software pointer
  flxcard.dma_set_ptr(dma_id, dma_softw_ptr);

  DEBUG("toflx card " << card_no << ", DMA initiated, dma_softw_ptr="
        << std::hex << dma_softw_ptr);
}
