/*
 * ToJSON.cpp
 *
 *  Created on: Oct 18, 2018
 *      Author: gleventi
 */

#include "ToJSON.hpp"
#include "definitions.hpp"

namespace felix {
namespace core {

unsigned long int	ToJSON::maxeLink			= 0;
unsigned long int	ToJSON::maxThreadIndex		= 0;
unsigned long int	ToJSON::devices				= 0;
Statistics*			ToJSONmainStatistics		= NULL;

using namespace nlohmann;

ToJSON::ToJSON() {
//	sleep(1);
	mainStatistics		= &Statistics::getInstance();
	maxeLink			= getMaxeLinksGeneral();
	maxThreadIndex		= getMaxThreadAllLinks();
	devices				= getNoOfDevices();
	accumulatedData		= mainStatistics->getAccumulatedDataStruct();
}

json ToJSON::constructContHist(SingleCounterStruct*& data, LegendStruct& legend) {
	json o;
	json a				= json::array();
	for (unsigned int i = 0; i < data->counterSize; ++i)
		a.push_back( { i, data->counter[i].load() } );
	o["title"]			= legend.title;
	o["unitX"]			= legend.unitX;
	o["unitY"]			= legend.unitY;
	o["entries"]		= data->totalCounter.load();
	o["underflow"]		= data->underFlowCounter.load();
	o["overflow"]		= data->overFlowCounter.load();
	o["data"]			= a;
	return o;
}

json ToJSON::constructChunkTypeHist(SingleCounterStruct*& data, LegendStruct& legend) {
	json o;
	json a				= json::array();
	for (unsigned int i = 0; i < data->counterSize; ++i)
		a.push_back( { i, data->counter[i].load() });
	o["title"]			= legend.title;
	o["unitX"]			= legend.unitX;
	o["unitY"]			= legend.unitY;
	o["categories"]		= {"null", "first", "last", "both", "middle", "timeout", "reserved", "out-of-band"};
	o["entries"]		= data->totalCounter.load();
	o["underflow"]		= data->underFlowCounter.load();
	o["overflow"]		= data->overFlowCounter.load();
	o["data"]			= a;
	return o;
}

template<typename T>
json ToJSON::toTimeSeriesJson(std::string name, T value,
		unsigned long long timestamp, std::string unit) {
	json o;
	json a				= json::array();
	json p				= json::array( { timestamp * 1000, value });
	a.push_back(p);
	o["name"]			= name;
	o["unit"]			= unit;
	o["data"]			= a;
	return o;
}

void ToJSON::prepareSeriesJson(json& cj, std::string title, std::string unit) {
	cj["title"]			= title;
	cj["unit"]			= unit;
	cj["series"]		= json::array();
}

json ToJSON::memoryGraph() {
	unsigned long long int timestamp = getCurrentTimestamp();
	json o;
	prepareSeriesJson(o, "Free Memory", "Blocks");
	unsigned short int counter = 0;
	for (			; counter < maxThreadIndex;	++counter)
			o["series"].push_back(toTimeSeriesJson(				std::to_string(counter), accumulatedData->freeBufferC[counter].load(), timestamp));
	for (counter = 0; counter < devices;		++counter)
			o["series"].push_back(toTimeSeriesJson("CMEM " +	std::to_string(counter), accumulatedData->freeBlocksC[counter].load(), timestamp));
	return o;
}

json ToJSON::ratesGraph() {
	unsigned long long int timestamp = getCurrentTimestamp();
	json o;
	prepareSeriesJson(o, "Rates", "M/s");
	o["series"] 		= {
						toTimeSeriesJson("Block Rate", mainStatistics->getBlockRate(), timestamp),
						toTimeSeriesJson("Chunk Rate", mainStatistics->getChunkRate(), timestamp)
						};
	return o;
}

json ToJSON::queueSizeGraph() {
	unsigned long long int timestamp = getCurrentTimestamp();
	json o;
	prepareSeriesJson(o, "Queue Size", "Blocks");
	unsigned short int counter = 0;
	for (counter = 0; counter < maxThreadIndex; ++counter)
		o["series"].push_back(toTimeSeriesJson(std::to_string(counter), accumulatedData->queueInstancesCurrentSizeC[counter].load(), timestamp));
	return o;
}

json ToJSON::throughputGraph() {
	unsigned long long int timestamp = getCurrentTimestamp();
	json o;
	prepareSeriesJson(o, "Throughput", "Mbyte/s");
	unsigned short int counter = 0;
	for (counter = 0; counter < maxThreadIndex; ++counter)
		o["series"].push_back(toTimeSeriesJson(std::to_string(counter), mainStatistics->getMessageRate()[counter], timestamp));
	return o;
}

json ToJSON::generalTable() {
	unsigned long long int timestamp = getCurrentTimestamp();
	json o;
	o["data"]			= json::array({
						json::array({	"timestamp",			timestamp,														""			}),
						json::array({	"blocks",				accumulatedData->blocks.load(),									"Blocks"	}),
						json::array({	"chunks",				accumulatedData->chunks.load(),									"Chunks"	}),
						json::array({	"shortchunks",			accumulatedData->shortchunks.load(),							"Chunks"	}),
						json::array({	"block_rate",			mainStatistics->getBlockRate(), 								"MBlocks/s"	}),
						json::array({	"chunk_rate",			mainStatistics->getChunkRate(), 								"MChunks/s"	}),
						json::array({	"throughput",			mainStatistics->getThroughput(),								"Mbyte/s"	}),
						json::array({	"max_queue_size",		mainStatistics->getMaxQueueSize(), 								"Blocks"	}),
						json::array({	"avg_queue_size",		mainStatistics->getAvgQueueSize(),								"Blocks"	}),
						});
	return o;
}

json ToJSON::cardsTable() {
	json o;
	o["data"]			= json::array();
	for (unsigned int i = 0; i < devices; ++i)
		o["data"].push_back(json::array({"blocks_free " + std::to_string(i), accumulatedData->freeBlocksC[i].load(), "Blocks"}));
	return o;
}

json ToJSON::errorsTable() {
	json o;
	o["data"]			= json::array({
						json::array({	"malformedSubchunks",	accumulatedData->malformedSubchunks.load(),						"Chunks"	}),
						json::array({	"malformedChunks",		accumulatedData->malformedChunks.load(),						"Chunks"	}),
						json::array({	"malformedBlocks",		accumulatedData->malformedBlocks.load(),						"Blocks"	}),
						json::array({	"total_bad_subchunks",	accumulatedData->sizeFaultyChunks.load(),						"Chunks"	}),
						json::array({	"error_subchunks",			accumulatedData->faultyChunks.load(),							"Chunks"	}),
						json::array({	"truncated_subchunks",		accumulatedData->truncatedChunks.load(),						"Chunks"	}),
						json::array({	"crcerr_subchunks",		accumulatedData->crcerrChunks.load(),						"Chunks"	}),
						});
	return o;
}

json ToJSON::pathDependentJSONrespone(std::vector<std::string> path) {
	unsigned short int option = (wpl.pathNameMap[path[1]] | wpl.pathTypeMap[path[2]]);
	switch (option) {
	case tHistogram | nElinksToHost:
		return constructContHist(accumulatedData->eLinkFromFelix,				wpl.pathLegends[option]);
		break;
	case tHistogram | nElinksFromHost:
		return constructContHist(accumulatedData->eLinkToFelix,					wpl.pathLegends[option]);
		break;
	case tHistogram | nChunksLength:
		return constructContHist(accumulatedData->chunkLength,					wpl.pathLegends[option]);
		break;
	case tHistogram | nSubchunksLength:
		return constructContHist(accumulatedData->subchunkLength,				wpl.pathLegends[option]);
		break;
	case tHistogram | nQueues:
		return constructContHist(accumulatedData->blocksInQueue,				wpl.pathLegends[option]);
		break;
	case tHistogram | nBlocksPerThread:
		return constructContHist(accumulatedData->blocksPerThread,				wpl.pathLegends[option]);
		break;
	case tHistogram | nBlocksPerTransfer:
		return constructContHist(accumulatedData->blocksPerTransfer,			wpl.pathLegends[option]);
		break;
	case tHistogram | nElinksPorts:
		return constructContHist(accumulatedData->elinkPorts,					wpl.pathLegends[option]);
		break;
	case tHistogram | nSubchunkType:
		return constructChunkTypeHist(accumulatedData->subchunkType,			wpl.pathLegends[option]);
		break;
	case tGraph | nMemory:
		return memoryGraph();
		break;
	case tGraph | nRates:
		return ratesGraph();
		break;
	case tGraph | nQueueSize:
		return queueSizeGraph();
		break;
	case tGraph | nThroughput:
		return throughputGraph();
		break;
	case tTable | nGeneral:
		return generalTable();
		break;
	case tTable | nCard:
		return cardsTable();
		break;
	case tTable | nErrors:
		return errorsTable();
		break;
	default:
		json o;
		return o;
		break;
	}
	return NULL;
}

} /* namespace core */
} /* namespace felix */
