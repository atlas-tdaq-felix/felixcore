#ifndef FELIX_CORE_HISTOGRAM
#define FELIX_CORE_HISTOGRAM

#include <string>
#include <vector>
#include <limits>

#include "json.hpp"

namespace felix {
namespace core {
using namespace nlohmann;

/**
 * Note: entries, underflow and overflow not locked, takes too much time.
 */
class Histogram
{
public:
  Histogram(std::string title, unsigned min, unsigned max, std::string unitX = "", std::string unitY = "") :
        title(title), convertAt(0), min(min), max(max), unitX(unitX), unitY(unitY), no_of_bins(max - min + 1) {

    entries = 0;
    underflow = 0;
    overflow = 0;

    bin = new unsigned long long[no_of_bins];

    for (unsigned i=0; i<no_of_bins; i++)
      bin[i] = 0;
  }

  Histogram(std::string title, unsigned convertAt = 100, std::string unitX = "", std::string unitY = "") :
        title(title), convertAt(convertAt), min(0), max(-1), unitX(unitX), unitY(unitY), no_of_bins(0) {
    entries = 0;
    underflow = 0;
    overflow = 0;

    bin = new unsigned long long[1];
    values = new unsigned long long[convertAt];
    weights = new unsigned long long[convertAt];
  }

  ~Histogram() {
    delete bin;
    if (values) delete values;
    if (weights) delete weights;
  }

  inline void fill(unsigned v, unsigned weight = 1) {
    if (entries < convertAt) {
      // keep value and weight
      values[entries] = v;
      weights[entries] = weight;
      entries++;

      // look up min and max
      min = UINT_MAX;
      max = 0;
      for (unsigned i=0; i<entries; i++) {
        if (values[i] < min) {
          min = values[i];
        }
        if (values[i] > max) {
          max = values[i];
        }
      }

      // convert to histogram
      delete bin;
      no_of_bins = max - min + 1;
      bin = new unsigned long long[no_of_bins];
      for (unsigned i=0; i<entries; i++) {
        bin[values[i] - min] += weights[i];
      }
    } else {
      // do a normal fill
      if (v < min) {
        underflow += weight;
        return;
      }

      if (v > max) {
        overflow += weight;
        return;
      }

      bin[v - min] += weight;
      entries++;
    }
  }

  inline void fill(unsigned v[], unsigned count) {
    for (unsigned i=0; i<count; i++) {
      fill(v[i]);
    }
  }

  /**
   * returns index of lowest (valued) bin
   */
  unsigned minIndex() {
    unsigned index = 0;
    unsigned long long lowestValue = ULLONG_MAX;
    for (unsigned i=0; i < no_of_bins; i++) {
      if (bin[i] <= lowestValue) {
        lowestValue = bin[i];
        index = i;
      }
    }
    return min + index;
  }

  json toJson() {
    json o;
    o["title"] = title;
    o["unitX"] = unitX;
    o["unitY"] = unitY;
    if (subtitle != "") {
      o["subtitle"] = subtitle;
    }
    if (categories.size() > 0) {
      o["categories"] = categories;
    }

    o["entries"] = entries;
    o["underflow"] = underflow;
    o["overflow"] = overflow;

    json a = json::array();
    // json temp;
    for (unsigned i=0; i<no_of_bins; i++) {
      // if (bin[i] > 0 )
        a.push_back( { min + i, bin[i] });
        // o[std::to_string(min+i)] = bin[i];
      // temp[min+i] = bin[i];
      // a.push_back( temp );
      // json temp;
      // temp[min + i] = bin[i];
      // a.push_back(temp);
    };
    o["data"] = a;
    return o;
  }

  std::string toLogstashJson() {
    std::string j;
    j = "";
    for (unsigned i=0; i<no_of_bins; i++) {
      if ( bin[i] > 0 ) {
        j.append("{\"E-link\":");
        j.append(std::to_string(min+i));
        j.append(",\"Count\":");
        j.append(std::to_string(bin[i]));
        j.append("}\n");
      }
    }

    return j;
  }

  void setSubtitle(std::string s) {
    subtitle = s;
  }

  void setCategories(std::vector<std::string> v) {
    categories = v;
  }

private:
  std::string title;
  unsigned convertAt;
  unsigned min;
  unsigned max;
  std::string unitX;
  std::string unitY;
  unsigned no_of_bins;

  unsigned long long entries;
  unsigned long long underflow;
  unsigned long long overflow;
  unsigned long long* bin;
  unsigned long long* values;
  unsigned long long* weights;

  std::string subtitle;
  std::vector<std::string> categories;
};

}
}

#endif
