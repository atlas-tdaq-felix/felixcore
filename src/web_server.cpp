/*
 * web_server.cpp
 *
 *  Created on: Nov 1, 2018
 *      Author: gleventi
 */

#include "web_server.hpp"
#include "configuration.hpp"

#include <functional>

namespace felix {
namespace core {

WebServer::WebServer(boost::filesystem::path web_root) :
		web_root(web_root) {
	server = NULL;
}

WebServer::~WebServer() {
	if (server)
		delete server;
}

void WebServer::defaultGet(std::shared_ptr<HttpServer::Response> response,
		std::shared_ptr<HttpServer::Request> request) {
	boost::filesystem::path path = web_root;
	path /= request->path;
	INFO("Http request: " << request->path);
	if (boost::filesystem::exists(path)) {
		path = boost::filesystem::canonical(path);
		//Check if path is within web_root
		if (std::distance(web_root.begin(), web_root.end())
				<= std::distance(path.begin(), path.end())
				&& std::equal(web_root.begin(), web_root.end(), path.begin())) {
			if (boost::filesystem::is_directory(path))
				path /= "index.html";
			if (boost::filesystem::exists(path)
					&& boost::filesystem::is_regular_file(path)) {
				auto ifs = std::make_shared<std::ifstream>();
				ifs->open(path.string(), std::ifstream::in | std::ios::binary);

				if (*ifs) {
					//read and send 128 KB at a time
					std::streamsize buffer_size = 131072;
					auto buffer = std::make_shared<std::vector<char> >(
							buffer_size);

					ifs->seekg(0, std::ios::end);
					auto length = ifs->tellg();

					ifs->seekg(0, std::ios::beg);

					// FIXME mime types ?

					*response << "HTTP/1.1 200 OK\r\nContent-Length: " << length
							<< "\r\n\r\n";
					defaultResourceSend(*server, response, ifs, buffer);
					return;
				}
			}
		}
	}
	badRequest(response, request);
}

void WebServer::setup() {
	FelixConfiguration::Values cfg = FelixConfiguration::instance().values();

	// Configure web directory
	// FIXME check that web directory is available.

	server = new HttpServer();
	server->config.port = cfg.web_port;

	std::string url =
			"^((http[s]?|ftp):\\/)?\\/?([^:\\/\\s]+)((\\/\\w+)*\\/)([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?$";
	std::string jsonUrl =
			"^/(v\\d+(\\.\\d+)*)/json/([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?";

	server->resource[jsonUrl]["GET"] =
			[&](std::shared_ptr<HttpServer::Response> response,
					std::shared_ptr<HttpServer::Request> request) {
				// INFO("Json request: " << request->path);
				// INFO("Json query_string: " << request->query_string);
				std::string version = request->path_match[URLPOSITIONVERSION];
				std::string path=request->path_match[URLPOSITIONPATH];
				std::vector<std::string> path_segments;
				boost::split( path_segments, path, boost::is_any_of("/"), boost::token_compress_on );

				std::string query = request->query_string;
				std::vector<std::string> query_segments;
				boost::split( query_segments, query, boost::is_any_of("&"), boost::token_compress_on );

				// TODO Make this dynamic and useful
				if (version == "v1") {
					// TODO Change 2s below to 3s (and with use of definitions  )
					if ((	boost::starts_with(path_segments[0],"configuration")	||
							boost::starts_with(path_segments[0],"statistics"))		&&
							path_segments.size() > 2) {
						send(response, jsonStatistics.pathDependentJSONrespone(path_segments));
					} else if (path == "bus/elinks") {
						send(response, elinkTable.toJson());
					} else if (path == "bus/felix") {
						send(response, felixTable.toJson());
					} else {
						badRequest(response, request);
					}
				} else {
					badRequest(response, request);
				}
			};

	//Default GET. If no other matches, this anonymous function will be called.
	//Will respond with content in the web/-directory, and its subdirectories.
	//Default file: index.html
	//Can for instance be used to retrieve an HTML 5 client that uses REST-resources on this server
	server->default_resource["GET"] = std::bind(&WebServer::defaultGet, this, std::placeholders::_1, std::placeholders::_2);
}

void WebServer::defaultResourceSend(const HttpServer &server,
		std::shared_ptr<HttpServer::Response> response,
		std::shared_ptr<std::ifstream> ifs,
		std::shared_ptr<std::vector<char> > buffer) {
	std::streamsize read_length;
	if ((read_length = ifs->read(&(*buffer)[0], buffer->size()).gcount()) > 0) {
		response->write(&(*buffer)[0], read_length);
		if (read_length == static_cast<std::streamsize>(buffer->size())) {
			response->send(
					[this, &server, response, ifs, buffer](const boost::system::error_code &ec) {
						if(!ec)
							defaultResourceSend(server, response, ifs, buffer);
						else
							std::cerr << "Connection interrupted" << std::endl;
					});
		}
	}
	return;
}

void WebServer::send(std::shared_ptr<HttpServer::Response> response,
		nlohmann::json o) {
	std::stringstream contentStream;
	contentStream << o;
	std::string message = contentStream.str();
	*response << "HTTP/1.1 200 OK\r\n";
	*response << "Content-Length: " << message.length() << "\r\n";
	*response << "Content-Type: application/json" << "\r\n\r\n";
	*response << message;
	return;
}

} /* namespace core */
} /* namespace felix */
