#include "busythread.hpp"
#include <iomanip>

using namespace felix::core;

BusyThread::BusyThread(unsigned int card_no, unsigned short port)
: card_no(card_no), port(port),
  ctx("posix"),
  socket(&ctx, port,
        std::bind(&BusyThread::busy_callback, this, std::placeholders::_1, std::placeholders::_2),
        netio::sockcfg::cfgll()(netio::sockcfg::ZERO_COPY))
{
  cfg = FelixConfiguration::instance().values();
}


BusyThread::~BusyThread()
{
  ctx.event_loop()->stop();
	bg_thread->join();
  flx.card_close();
}

void BusyThread::start() {
  bg_thread = std::unique_ptr<std::thread>(new std::thread([&](){
    ctx.event_loop()->run_forever();
  }));

  try {
    flx.card_open(card_no, LOCK_NONE);
  } catch(FlxException &ex) {
    ERROR("Exception while opening BUSY device (FLX #" << card_no << "): "
            << ex.what());
    throw;
  }
}


void
BusyThread::busy_callback(netio::endpoint& ep, netio::message& msg)
{
    if(msg.size() != sizeof(felix::base::BusyMessage))
    {
        WARNING("BUSY request rejeceted from " << ep.address() << ":" << ep.port()
                << " ('malformatted BUSY message')");
    }

    felix::base::BusyMessage bm;
    msg.serialize_to_usr_buffer((uint8_t*)&bm);

    std::string command;
    if(bm.command == felix::base::BusyMessage::XON) {
      command = "XON";
    } else if (bm.command == felix::base::BusyMessage::XOFF) {
      command = "XOFF";
    } else if (bm.command == felix::base::BusyMessage::BUSYON) {
      command = "BUSYON";
      flx.cfg_set_option("TTC_DEC_CTRL_MASTER_BUSY", 1);
    } else if (bm.command == felix::base::BusyMessage::BUSYOFF) {
      command = "BUSYOFF";
      flx.cfg_set_option("TTC_DEC_CTRL_MASTER_BUSY", 0);
    } else {
      command = "UNRECOGNIZED COMMAND";
    }

    DEBUG("BUSY message from " << ep.address() << ":" << ep.port()
          << " for E-Link " << bm.elink << ": " << command);
}
