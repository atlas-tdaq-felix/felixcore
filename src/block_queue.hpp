#ifndef FELIXCORE_BLOCK_QUEUE_HPP
#define FELIXCORE_BLOCK_QUEUE_HPP

#include "packetformat/block_format.hpp"

#include "felixbase/queue.hpp"

namespace felix
{
namespace core
{

typedef felix::packetformat::block Block;

typedef felix::base::ConcurrentQueue BlockQueue;
typedef std::vector<BlockQueue*> BlockQueueVector;

}
}

#endif
