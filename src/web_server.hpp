#ifndef FELIX_WEB_SERVER
#define FELIX_WEB_SERVER

#include <string>
#include <fstream>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <memory>
#include <ios>

#define URLPOSITIONVERSION 1
#define URLPOSITIONPATH 3

#define BOOST_SPIRIT_THREADSAFE
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include "nlohmann/json.hpp"
#include "ToJSON.hpp"
#include "felixbase/logging.hpp"
#include "felixbus/bus.hpp"
#include "felixbus/elinktable.hpp"
#include "felixbus/felixtable.hpp"

#include "server_http.hpp"

namespace felix
{
namespace core
{

typedef SimpleWeb::Server<SimpleWeb::HTTP> HttpServer;

class WebServer {

// using json = nlohmann::json;

public:
	WebServer(boost::filesystem::path web_root);

	~WebServer();

	void setup();

	inline void start() { server->start(); }

	inline void stop() { server->stop(); }

	inline void badRequest(std::shared_ptr<HttpServer::Response>& response,
			std::shared_ptr<HttpServer::Request>& request) {
		std::string content="Could not open path " + request->path;
		*response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
	}

private:
	HttpServer* server;
	boost::filesystem::path web_root;
	felix::bus::FelixTable felixTable;
	felix::bus::ElinkTable elinkTable;
	ToJSON jsonStatistics;
	void defaultResourceSend(const HttpServer&				server,
			std::shared_ptr<HttpServer::Response>			response,
			std::shared_ptr<std::ifstream>					ifs,
			std::shared_ptr<std::vector<char>>				buffer);

	void defaultGet(std::shared_ptr<HttpServer::Response>	response,
			std::shared_ptr<HttpServer::Request>			request);

	void send(std::shared_ptr<HttpServer::Response>			response,
			nlohmann::json									o);
};

}
}

#endif
