#ifndef FELIX_CORE_PIPELINE_BLOCK
#define FELIX_CORE_PIPELINE_BLOCK

#include <atomic>

#include "packetformat/block_format.hpp"
#include "packetformat/block_parser.hpp"

#include "configuration.hpp"
#include "common.hpp"
#include "chunk_pipeline.hpp"

namespace felix
{
namespace core
{

/**
 * Pipeline Element to parse blocks and enqueue the chunks in a ChunkPipeline.
 */
struct BlockParserPipelineElement
{
  struct ParserOps : public felix::packetformat::ParserOperations
  {
    ParserOps(unsigned e, ChunkPipeline& p, ShortChunkPipeline& sp, netio::publish_socket* socket, unsigned queueIndex, unsigned index, StatsN* stats)
      : elink(e), chunk_pipeline(p), shortchunk_pipeline(sp), socket(socket), chunk_meta(socket, queueIndex, index, stats), queueIndex(queueIndex), index(index),
		stats(stats) {
      cfg = FelixConfiguration::instance().values();
      chunk_meta.elink = e;
    }

    unsigned elink;
    ChunkPipeline& chunk_pipeline;
    ShortChunkPipeline& shortchunk_pipeline;
    netio::publish_socket* socket;
    ChunkMetadata chunk_meta;
    unsigned queueIndex;
    unsigned index;
    SmartBlockPtr current_block;
    FelixConfiguration::Values cfg;
    StatsN* stats;

    void chunk_processed(const felix::packetformat::chunk& chunk)
    {
      chunk_meta.last_block = current_block;
      chunk_pipeline.process(chunk, chunk_meta);
      chunk_meta = ChunkMetadata(socket, queueIndex, index, stats);
      chunk_meta.elink = this->elink;
    }

    void shortchunk_processed(const felix::packetformat::shortchunk& chunk)
    {
      chunk_meta.last_block = current_block;
      shortchunk_pipeline.process(chunk, chunk_meta);
      chunk_meta = ChunkMetadata(socket, queueIndex, index, stats);
      chunk_meta.elink = this->elink;
    }

    void subchunk_processed(const felix::packetformat::subchunk& subchunk)
    {
      if (!cfg.nostats) {
        if (subchunk.error) {
          stats->sizeFaultyChunksPP();
        }
        if (subchunk.err_flag) {
          stats->faultyChunksPP();
        }
        if (subchunk.trunc_flag) {
          stats->truncatedChunksPP();
        }
        if (subchunk.crcerr_flag) {
          stats->crcerrChunksPP();
        }
        if (!cfg.nohistos) {
          stats->subchunkLengthIncr(subchunk.length);
          stats->subchunkTypesIncr(subchunk.type);
        }
      }
    }

    void block_processed(const felix::packetformat::block&)
    {
    }


    void chunk_processed_with_error(const felix::packetformat::chunk& c)
    {
      chunk_processed(c);
      return;

      //DEBUG("Chunk error");
      if (!cfg.nostats) {
        stats->malformedChunksPP();
      }
    }

    void subchunk_processed_with_error(const felix::packetformat::subchunk& subchunk)
    {
      if (!cfg.nostats) {
        if (subchunk.error) {
          stats->sizeFaultyChunksPP();
        }
        if (subchunk.err_flag) {
          stats->faultyChunksPP();
        }
        if (subchunk.trunc_flag) {
          stats->truncatedChunksPP();
        }
        if (subchunk.crcerr_flag) {
          stats->crcerrChunksPP();
        }
        if (!cfg.nohistos) {
          stats->subchunkLengthIncr(subchunk.length);
          stats->subchunkTypesIncr(subchunk.type);
        }
      }
    }

    void block_processed_with_error(const felix::packetformat::block& b)
    {
      //ERROR("BLOCK CONTAINS ERROR CHUNK");
      //felix::base::dump_buffer((u_long)&b, 1024);

      block_processed(b);
      return;

      //DEBUG("Block error");
      if (!cfg.nostats) {
        stats->malformedBlocksPP();
      }
    }
  };

  typedef felix::packetformat::BlockParser<ParserOps> Parser;

  std::map<unsigned, std::unique_ptr<ParserOps>> ops;
  std::map<unsigned, std::unique_ptr<Parser>> parser;
  ChunkPipeline chunk_pipeline;
  ShortChunkPipeline shortchunk_pipeline;

  netio::context ctx;
  netio::publish_socket socket;
  std::thread bg_thread;

  static std::atomic_uint global_thread_id;
  unsigned thread_id;

  unsigned fetch_thread_id()
  {
    thread_id = global_thread_id++;
    return thread_id;
  }

  BlockParserPipelineElement()
    : ctx(FelixConfiguration::instance().values().netio_backend),
      socket(&ctx, FelixConfiguration::instance().values().port+fetch_thread_id())
  {    
    bg_thread = std::thread([this](){
      ctx.event_loop()->run_forever();
    });
    char tname[16];
    snprintf(tname, 16, "wrkr-evl-%02d", thread_id);
    set_thread_name(bg_thread, tname);

    socket.register_subscribe_callback([](netio::tag t, netio::endpoint ep) {
      INFO("Subscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t);
    });
  }

  ~BlockParserPipelineElement()
  {
    ctx.event_loop()->stop();
    bg_thread.join();
  }

  void process(SmartBlockPtr b, BlockMetadata block_meta)
  {
    felix::packetformat::block* b_ptr = b.get();
    unsigned index = block_meta.index;
    unsigned queueIndex = block_meta.queueIndex;
    unsigned elink = b_ptr->elink + index * MAX_ELINKS;
    if(ops.count(elink) == 0)
    {
      ops[elink] = std::move(std::unique_ptr<ParserOps>(new ParserOps(elink, chunk_pipeline,
                                                        shortchunk_pipeline, &socket, queueIndex, index, block_meta.stats)));
      parser[elink] = std::move(std::unique_ptr<Parser>(new Parser(*ops[elink])));

      // The parser needs to know from the FLX-card the block size and trailer size;
      // I have no idea how to get that info all the way from card to here;
      // perhaps through FelixConfiguration::instance()? (Henk, 25 Nov 2020)
      unsigned blocksize = BLOCK_SIZE;
#if REGMAP_VERSION < 0x500
      bool trailer_is_32bit = false;
#else
      bool trailer_is_32bit = true;
#endif // REGMAP_VERSION
      parser[elink]->configure( blocksize, trailer_is_32bit );
    }
    b.set_prev(ops[elink]->current_block);
    ops[elink]->current_block = b;
    parser[elink]->process(b_ptr);
  }
};


/**
 * A pipeline to process blocks.
 */
typedef StaticPipeline<SmartBlockPtr, BlockMetadata,
        CountingPipelineElement,
        BlockParserPipelineElement
        > Pipeline;

}
}

#endif
