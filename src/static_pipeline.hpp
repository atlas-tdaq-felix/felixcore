#ifndef FELIX_NETIO_STATIC_PIPELINE
#define FELIX_NETIO_STATIC_PIPELINE


template <typename Packet, typename Metadata, typename... Elements>
class StaticPipeline;

template <typename Packet, typename Metadata, typename Head, typename... Tail>
class StaticPipeline<Packet, Metadata, Head, Tail...>
{
  typedef Head Element;
  typedef StaticPipeline<Packet, Metadata, Tail...> Next;

  Element element;
  Next next;

public:
  void process(const Packet packet, Metadata metadata)
  {
    element.process(packet, metadata);
    next.process(packet, metadata);
  }
};

template<typename Packet, typename Metadata>
class StaticPipeline<Packet, Metadata>
{
public:
  void process(const Packet, Metadata) {}
};


#endif
