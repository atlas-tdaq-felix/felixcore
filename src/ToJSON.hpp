/*
 * ToJSON.hpp
 *
 *  Created on: Oct 18, 2018
 *      Author: gleventi
 */

#ifndef FELIXCORE_SRC_TOJSON_HPP_
#define FELIXCORE_SRC_TOJSON_HPP_

#include <vector>
#include <string>
#include "StatCountersStruct.hpp"
#include "WebServerPathBranches.hpp"		// Legend struct
#include "statistics.hpp"
#include "nlohmann/json.hpp"							// Nlohmann JSON implementation

namespace felix {
namespace core {

class ToJSON {
public:
	ToJSON();
	nlohmann::json pathDependentJSONrespone(std::vector<std::string> path);
private:
	static nlohmann::json constructContHist(SingleCounterStruct*& data,
			LegendStruct& legend);

	static nlohmann::json constructChunkTypeHist(SingleCounterStruct*& data,
			LegendStruct& legend);

	static void prepareSeriesJson(nlohmann::json& cj, std::string title, std::string unit);
	static inline unsigned long long int getCurrentTimestamp() { return time(NULL); };

	template<typename T>
	nlohmann::json toTimeSeriesJson(std::string name, T value,
			unsigned long long timestamp, std::string unit = "");
	nlohmann::json 					memoryGraph();
	nlohmann::json 					ratesGraph();
	nlohmann::json 					queueSizeGraph();
	nlohmann::json 					throughputGraph();
	nlohmann::json 					generalTable();
	nlohmann::json 					cardsTable();
	nlohmann::json 					errorsTable();
	StatCountersStruct			*	accumulatedData;
	Statistics					*	mainStatistics;
	WebPathsLegends					wpl;

//protected:
	static unsigned long int		maxeLink;
	static unsigned long int		maxThreadIndex;
	static unsigned long int		devices;

};
} /* namespace core */
} /* namespace felix */

#endif /* FELIXCORE_SRC_TOJSON_HPP_ */
