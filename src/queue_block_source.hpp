#ifndef FELIXCORE_QUEUE_BLOCK_SOURCE_HPP
#define FELIXCORE_QUEUE_BLOCK_SOURCE_HPP

#include "common.hpp"
#include "block_source.hpp"
#include "block_queue.hpp"
#include "configuration.hpp"
#include "statistics.hpp"

namespace felix {
namespace core {

class QueueBlockSource: public BlockSource {

public:
	QueueBlockSource(BlockQueue* buffer, BlockQueue* queue, unsigned int index,
			StatsN& statsn) :
			buffer(buffer), queue(queue), index(index), statsn(statsn) {
		cfg = FelixConfiguration::instance().values();
	}

	virtual ~QueueBlockSource() {
		delete queue;
	}

	SmartBlockPtr pop() {
		Block* block = NULL;
		queue->pop(&block);
		bool shouldDelete = true;
		SmartBlockPtr n = SmartBlockPtr(block, buffer, index, shouldDelete);
		if (!cfg.nostats)
			statsn.setBlocksInQueue(index, queue->size());
		return n;
	}

private:
	FelixConfiguration::Values cfg;
	BlockQueue* buffer;
	BlockQueue* queue;
	unsigned int index;
	StatsN& statsn;
};

} /* namespace core */
} /* namespace felix */

#endif
