 /*
 * StatsN.cpp
 *
 *  Created on: Sep 14, 2018
 *      Author: gleventi
 */

#include "StatsN.hpp"

#include "statistics.hpp"

namespace felix {
namespace core {

unsigned long int StatsN::maxeLink;
unsigned long int StatsN::maxThreadIndex;

StatsN::StatsN(StatisticsCategory statisticsCategory) {
	sCounters.setCategory(statisticsCategory);
	StatsN::maxeLink = getMaxeLinksGeneral();
	StatsN::maxThreadIndex = getMaxThreadAllLinks();
	Statistics::addStatisticsWorkerThread(&sCounters);
}



} /* namespace core */
} /* namespace felix */
