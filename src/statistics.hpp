#ifndef FELIX_CORE_STATISTICS
#define FELIX_CORE_STATISTICS

#include <iostream>
//#include <ctime>
#include <forward_list>
#include <tbb/tick_count.h>

#include "felixbase/logging.hpp"

#include "StatCountersStruct.hpp"

#define STATISTICSUPDATEINTERVAL (100000)

namespace felix {
namespace core {

/**
 * A singleton class to track statistics of the pipeline(s).
 */
class Statistics {
public:
	static Statistics& getInstance();

	~Statistics();

	void update();

	void display_statistics(std::ostream& out = std::cout);

	static inline void addStatisticsWorkerThread(StatCountersStruct* newNode)
	{ Statistics::getInstance().statisticsWorkersList.emplace_front(newNode); }

	StatCountersStruct	*&	getAccumulatedDataStruct()		{ return accumulatedData;	}
	inline double		&	getMaxQueueSize()				{ return maxQueueSize;		}
	inline double		&	getAvgQueueSize()				{ return avgQueueSize;		}
	inline double		&	getThroughput()					{ return throughput;		}
	inline double		&	getBlockRate()					{ return blockRate;			}
	inline double		&	getChunkRate()					{ return chunkRate;			}
	inline double		*&	getMessageRate()				{ return messageRate;		}
//	inline void				stopMainStatisticsThread()		{ keepAlive = false;		}

private:
	static StatCountersStruct					*	accumulatedData;
	static std::mutex								mtx;
	static std::unique_ptr		<Statistics>		m_instance;
	static std::thread								mainStatisticsThread;
	static bool										keepAlive;

	Statistics(unsigned int no_of_devices, unsigned int no_of_threads);
	void runStatisticsThread();


	double											blockRate					= 0;
	double											chunkRate					= 0;
	double											maxQueueSize				= 0;
	double											avgQueueSize				= 0;
	double											throughput					= 0;
	unsigned int									number_of_devices			= 0;
	unsigned int									number_of_queues			= 0;
	unsigned long long int							timestamp					= 0;
	unsigned long long int							blocks_at_last_sync			= 0;
	unsigned long long int							chunks_at_last_sync			= 0;
	unsigned long long int							shortchunks_at_last_sync	= 0;

	double										*	messageRate;
	unsigned long long int						*	message_size_at_last_sync	= NULL;

	tbb::tick_count									t0;
	std::forward_list			<StatCountersStruct*> statisticsWorkersList;

public:
	Statistics		(Statistics const&)				= delete;
	void operator=	(Statistics const&)				= delete;
};

} /* namespace core */
} /* namespace felix */

#endif
