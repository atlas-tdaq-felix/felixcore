#ifndef FELIX_CORE_PIPELINE_COMMON
#define FELIX_CORE_PIPELINE_COMMON

#include <cstdio>
#include <vector>
#include <functional>
#include <iostream>
#include <iomanip>
#include <pthread.h>

#include "felixbase/common.hpp"

#include "metadata.hpp"
#include "configuration.hpp"

#define MAX_ELINKS 2048
#define BLOCK_SIZE felix::packetformat::BLOCKSIZE

namespace felix {
namespace core {
class Constant {
	public:
    // Constants
    static const u_long Kilo = 1024;
    static const u_long Mega = 1024 * Kilo;
    static const u_long Giga = 1024 * Mega;
  };

  inline unsigned int getLinkId(unsigned int felixId, unsigned int elink) {
    return elink | (felixId << 16);
  }

/**
 * A NOP pipeline element.
 */
struct NullPipelineElement
{
  template <typename C, typename M>
  void process(const C, M) {}
};

/**
 * A pipeline elements to count packets that are passed through it.
 */
struct CountingPipelineElement
{
  CountingPipelineElement() {
    cfg = FelixConfiguration::instance().values();
  }

  FelixConfiguration::Values cfg;

  void process(SmartBlockPtr, BlockMetadata)
  {
    // blocks are counted at dispatch to avoid contention
  }

  void process(const felix::packetformat::chunk chunk, ChunkMetadata& chunk_meta)
  {
    if (!cfg.nostats) {
    	chunk_meta.stats->chunksPP();
      if (!cfg.nohistos) {
    	  chunk_meta.stats->chunkLengthIncr(chunk.length());
      }
    }
  }

  void process(const felix::packetformat::shortchunk shortChunk, ChunkMetadata& chunk_meta)
  {
    if (!cfg.nostats) {
    	chunk_meta.stats->shortchunksPP();
      if (!cfg.nohistos) {
    	  chunk_meta.stats->chunkLengthIncr(shortChunk.length);
      }
    }
  }

};

/**
 * A pipeline element to offset the elinkId (11 bits) by the felixId (16 bits)
 */
struct ElinkPipelineElement {
  ElinkPipelineElement() {
    cfg = FelixConfiguration::instance().values();
  }

  void process(SmartBlockPtr, BlockMetadata) {
  }

  void process(const felix::packetformat::chunk, ChunkMetadata& meta) {
    updateElink(meta);
  }

  void process(const felix::packetformat::shortchunk, ChunkMetadata& meta) {
    updateElink(meta);
  }

  void updateElink(ChunkMetadata& meta) {
    meta.elink = getLinkId(cfg.felix_id.value, meta.elink);
  }

  FelixConfiguration::Values cfg;
};


/**
 * Associate a name with the given thread. The name is displayed in
 * debugging tools like GDB. Max len is 16, including \0.
 */
inline void set_thread_name(std::thread& thread, const char* name) {
    char tname[16];
    snprintf(tname, 16, "flx-%.11s", name);
    auto handle = thread.native_handle();
    pthread_setname_np(handle, tname);
}

/**
 * Pin a thread to a specific (hardware) core/thread to only run from.
 */
inline void pinThreadToCPUThread(int cpuThread) {
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(cpuThread, &cpuset);
	pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
}

} /* namespace core */
} /* namespace felix */

#endif
