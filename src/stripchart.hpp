#ifndef STRIPCHART_HPP
#define STRIPCHART_HPP
#include<set>
#include<map>
#include<cmath>
#include<vector>
#include<cfloat>
#include<cstddef>
#include<iomanip>
#include<sstream>
#include<iostream>
#include<algorithm>

using namespace std;

template<typename T>
struct ring
{
public:
  ring(size_t cap)
    : _buf(cap) {
  }

  void push(T t) {
    if( _tail >= _buf.size() )
    {
      _tail = 0;
    }
    _buf[_tail] = t;
    ++_tail;
    if( _sz == _buf.size() )
    {
      _head = (_head+1) % _buf.size();
    }
    else
    {
      ++ _sz;
    }
  }

  T& operator[] (size_t pos) {
    auto p = (_head + pos) % _buf.size();
    return _buf[p];
  }

  T& max() {
    return * max_element(_buf.begin(), _buf.begin()+_sz);
  }

  T& min() {
    return * min_element(_buf.begin(), _buf.begin()+_sz);
  }

  size_t& size() {
    return _sz;
  }

  vector<T> _buf;
  size_t _head = 0;
  size_t _tail = 0;
  size_t _sz = 0;
};

class approx_rolling_average
{
public:
  // In statistics, a moving average is a calculation to analyze data
  // points by creating series of averages of different subsets of the
  // full data set.
  approx_rolling_average()
    : _num_of_samples(1), _avg(0.) {
  }

  double update(double new_sample) {
    // Calculating moving average
    _avg = _avg - _avg / _num_of_samples;
    _avg = _avg + new_sample / _num_of_samples;
    ++ _num_of_samples;
    return _avg;
  }

  double get() {
    return _avg;
  }

  size_t _num_of_samples;
  double _avg;
};


template<unsigned dt=1,   // time keeping, delta t
         unsigned cap=60> // capacity of the ring buffers (60s)
class stripchart
{
public:
  // This chart has a scrolling display that is similar to a paper tape strip
  // chart recorder. This chart first plots values from left to right. From here,
  // it continues to plot new points at the rightmost point and shifts old values
  // to the left.

  map<string, string> _bcolours =
  { {"white",   "\033[97m"},
    {"aqua",    "\033[96m"},
    {"pink",    "\033[95m"},
    {"blue",    "\033[94m"},
    {"yellow",  "\033[93m"},
    {"green",   "\033[92m"},
    {"red",     "\033[91m"},
    {"grey",    "\033[90m"},
    {"black",   "\033[30m"},
    {"default", "\033[39m"}};

  typedef set<pair<int, double>> pairset;

  typedef vector<double> vectord;
  typedef vector<int> vectori;

  typedef ring<double> ringd;
  typedef ring<int> ringi;

  stripchart(string title="", string colour="default", char pch='o', int precision=3)
    : _title(title), _colour(colour), _tseries(cap), _yseries(cap), _pch('o'), _background(' '),
      _dt( !dt ? 1 : dt ), _time(_dt), _precision(precision) {

    _hist_min = DBL_MAX;
    _hist_max = DBL_MIN;
  }

  bool _is_close(double a, double b, double epsilon=0.0005) {
    // Nearly equals - whether or not two values are considered close is determined
    // according to given epsilon.
    return fabs(a - b) < epsilon;
  }

  vectori _range(int start, int stop, int delta) {
    if(delta == 0)
    {
      delta = 1;
    }
    vectori vec;
    for(int i(start);i<=stop; i = i + delta)
    {
      vec.push_back(i);
    }
    return vec;
  }

  vectord _range(double start, double stop, double delta) {
    if(_is_close(delta, 0.0))
    {
      delta = 0.01;
    }
    vectord vec;
    for(double o(start);;o = o + delta) {
      vec.push_back(o);
      if(o >= stop)
      {
        break;
      }
    }
    return vec;
  }

  vectord _y_scale(double min, double max, unsigned steps=16) {
    vectord vec = _range(min, max, (max - min)/steps );
    reverse(vec.begin(), vec.end());
    return vec;
  }

  void push(double new_sample) {
    _tseries.push(_time);
    _time = _time + _dt;
    _yseries.push(new_sample);

    // Keeping track of which is the smallest and which is
    // the largest value so far.
    if(new_sample > _hist_max)
    {
      _hist_max = new_sample;
    }
    if(new_sample < _hist_min)
    {
      _hist_min = new_sample;
    }

    // Keep track of rolling average
    _avg.update(new_sample);

    _latest_sample = new_sample;
  }

  string _getcolour(string text, string colour) {
    // Print color text using escape codes
    string s(_bcolours[colour]);
    s.append(text);
    s.append(_bcolours["default"]);
    return s;
  }

  string _to_string_with_precision(const double o_value) {
    std::ostringstream out;
    out << std::setprecision(_precision) << o_value;
    return out.str();
  }

  // Merge two strings
  void _merge(const char* s1, char* s2, const char pch) {
    while(*s1 != '\0' && *s2 != '\0') {
      if(*s2 == pch)
        *s2 = *s1;
      s2++;
      s1++;
    }
  }

  string plot(bool clrscr=true) {
    // plot() creates a 2-D scatter plot of the data in Y versus the
    // corresponding values in T.
    pairset plotted;

    vectori xs = _range(_tseries.min(), _tseries.min() + (cap-1)*_dt, _dt);
    vectord ys;
    if(_is_close(_yseries.max(), 0.00))
    {
      ys = _y_scale(0.00, 1.00);
    }
    else
    {
      ys = _y_scale(0.00, _yseries.max()*1.05);
    }

    // Clear console
    string p( clrscr ? "\x1B[2J\x1B[H" : "" );

    // Add summary to plot
    p.append(" AVG: "+_to_string_with_precision(_avg.get())+"\n"
             " MIN: "+_to_string_with_precision(_hist_min) +"\n"
             " MAX: "+_to_string_with_precision(_hist_max) +"\n"
             " CUR: "+_to_string_with_precision(_latest_sample)+"\n");

    string s(cap, '-');
    // Add title
    _merge(_title.c_str(), &s[0u], '-');
    p.append(" "+s+"\n");

    for(auto y : ys)
    {
      p.append("|");
      for(auto x : xs)
      {
        string point(1, _background);
        for(unsigned i=0; i<_tseries.size(); ++i)
          if(_tseries[i] <= x && _yseries[i] >= y &&
            plotted.find(make_pair(_tseries[i], _yseries[i])) == plotted.end())
          {
            point[0u] = _pch;
            plotted.insert(make_pair(_tseries[i], _yseries[i]));
          }
        p.append(_getcolour(point, _colour));
      }
      // Add Y-axis
      p.append("|"+_to_string_with_precision(y)+"\n");
    }

    // Add X-axis
    s = to_string(_tseries.min() + (cap-1)*_dt);
    s.insert(s.begin(), cap - s.size(), '-');

    _merge(to_string(_tseries.min()).c_str(), &s[0u], '-');
    p.append(" "+s+"\n");

    return p;
  }

  approx_rolling_average _avg;

  string _title;
  string _colour;

  ringi _tseries;
  ringd _yseries;

  char _pch;
  char _background;

  unsigned _dt;
  unsigned _time;
  unsigned _precision;

  double _hist_min;
  double _hist_max;
  double _latest_sample;
};

#endif
