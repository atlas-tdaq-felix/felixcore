#ifndef TOFLX_HPP
#define TOFLX_HPP

#include "configuration.hpp"
#include "statistics.hpp"
#include "netio/netio.hpp"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixbase/client.hpp"
#include "StatsN.hpp"
#include <chrono>

namespace felix {
namespace core {

const size_t TOFLX_MAX_RECV_SIZE = 16*1024*1024; // 16 MB
const size_t TOFLX_CMEM_BUFFERSIZE = 20*1024*1024; // 20 MB

typedef std::function<void(char* buf, size_t size)> TransferCallback;

class ToFLXThread {
public:
	ToFLXThread(unsigned int card_no, unsigned short port, int toflx_dmaid, int toflx_format);
	~ToFLXThread();
	void start();

private:
	FelixConfiguration::Values cfg;
	bool continuous_dma;
	bool initialized;
	unsigned int card_no;
	unsigned short port;
	netio::context ctx;
	netio::low_latency_recv_socket socket;
	std::unique_ptr<std::thread> bg_thread;
	StatsN stats;

	uint64_t counter_written_blocks;
	int flxcard_max_tlp;
	int dma_id;
	int fromhost_data_format;

	FlxCard flxcard;
	int cmem_handle;
	u_long cmem_size;
	u_long cmem_paddr;       // Start of allocated CMEM buffer (physical address)
	u_long cmem_paddr_end;   // Where the allocated CMEM buffer ends (physical address)
	u_long cmem_vaddr;       // Start of allocated CMEM buffer (virtual address)
	u_long cmem_vaddr_write; // Where to write next into the allocated CMEM buffer (virtual address)
	u_long dma_softw_ptr;    // Copy of the DMA 'software-controlled' pointer for updates and write-back

	std::array<uint8_t, TOFLX_MAX_RECV_SIZE> msgbuf;
	std::array<char, TOFLX_CMEM_BUFFERSIZE> cardbuf;
	char* cardbuf_ptr;

	void alloc_cmem();
	void toflx_callback(netio::endpoint& ep, netio::message& msg);
	void handle_message(felix::base::ToFELIXHeader header, uint8_t* data);
	void transfer_buffer_via_dma(char* buf, size_t size);
	void transfer_buffer_via_continuous_dma(char* buf, size_t size);

	TransferCallback transfer_buffer;
	void transfer_callback( TransferCallback transfer );
};

} /* namespace core */
} /* namespace felix */

#endif
