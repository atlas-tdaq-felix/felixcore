#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <atomic>
#include <unistd.h>

#include "felixtag.h"

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: fm_client [options]

Connects to a remote FULL-mode FELIX host and subscribes to all links.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -c CARD         Number of (virtual) interface card. Default: 0.
  -t THREADS      Number of threads (=ports) of the felixcore instance per device. Default: 2.
  -V              Display version.
  -h              Display this help message.
)FOO";


static std::atomic_ulong messages_received_vec[12];
static std::atomic_ulong bytes_received_vec[12];


 inline void set_thread_name(const char* name, unsigned n)
 {
     char tname[16];
     snprintf(tname, 16, "fm-%s-%d", name, n);
     pthread_setname_np(pthread_self(), tname);
 }


void callback(uint8_t* data, size_t len, void* usr)
{
  uint64_t i = reinterpret_cast<uint64_t>(usr);
  messages_received_vec[i].fetch_add(1);
  bytes_received_vec[i].fetch_add(len);
}

void subscriber_thread(std::string backend, const char* host, unsigned port, unsigned card, unsigned link, uint64_t i)
{
  netio::context ctx(backend.c_str());
  std::thread bg_thread([&ctx, card, link](){
    set_thread_name("evl", 12*card+link);
    ctx.event_loop()->run_forever();
  });

  set_thread_name("sub", 12*card+link);

  auto cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::CALLBACK, (uint64_t)callback);
  cfg(netio::sockcfg::CALLBACK_DATA, i);

  netio::subscribe_socket socket(&ctx, cfg);
  for(unsigned i=0; i<8; i++)
  {
    socket.subscribe((card*2048)+(link*64)+8+i, netio::endpoint(host, port));
  }
  messages_received_vec[i].store(0);
  bytes_received_vec[i].store(0);

  while(true) sleep(1);
}



int
main(int argc, char** argv)
{
  std::string host = "127.0.0.1";
  unsigned short port = 12350;
  std::string backend = "posix";
  unsigned card = 0;
  unsigned num_threads = 2;
  std::vector<std::thread> subscriber_threads;

    char opt;
  	while ((opt = getopt(argc, argv, "b:H:t:c:p:zVh")) != -1)
  	{
  		switch (opt)
  		{
  		case 'b':
  			backend = optarg;
  			break;
  		case 'H':
		  host = optarg;
  			break;
		case 't':
		         num_threads = atoi(optarg);
		         break;
		case 'c':
		         card = atoi(optarg);
		         break;
  		case 'p':
  			port = atoi(optarg);
  			break;
  		case 'h':
  		default:
  			std::cerr << HELP << std::endl;
  			return -1;
  		}
  	}

	unsigned i=0;
	for(unsigned local_link=0; local_link<12; local_link++, i++)
	  {
	    unsigned local_thread = local_link % num_threads;
	    unsigned p = port + card*num_threads + local_thread;
	    subscriber_threads.emplace_back(subscriber_thread,
					    backend, host.c_str(), p,
					    card, local_link,
					    i);
	  }

  typedef std::chrono::high_resolution_clock clock;
  auto t0 = clock::now();

  while(true)
  {
    sleep(1);
    unsigned long long messages_received = 0;
    unsigned long long bytes_received = 0;
    for(unsigned i=0; i<12; i++)
    {
      unsigned long long m = messages_received_vec[i].exchange(0);
      unsigned long long b = bytes_received_vec[i].exchange(0);
      messages_received += m;
      bytes_received += b;
    }

    auto t1 = clock::now();
    double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
    std::cout << "timestamp:          " << std::chrono::duration_cast<std::chrono::seconds>(t1.time_since_epoch()).count() << std::endl;
    std::cout << "message rate [MHz]: " << messages_received/seconds/1000./1000. << std::endl;
    std::cout << "data rate [GB/s]:   " << bytes_received/seconds/1024./1024./1024. << std::endl;
    std::cout << "data rate [Gb/s]:   " << bytes_received/seconds/1024./1024./1024.*8 << std::endl;
    std::cout << std::endl;
    t0 = t1;
  }
}
