#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <atomic>
#include <unistd.h>

#include "felixtag.h"

#include "netio/netio.hpp"

const char* HELP =
    R"FOO(Usage: felix_client [options]

Connets to a remote FELIX host and subscribes to data.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -f FILENAME     Write data to specified file.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -t TAG          Subscribe to the given tag. Can be used multiple times.
  -i              Interactive mode, prints received messages to the screen.
  -V              Display version.
  -h              Display this help message.
)FOO";


std::atomic<unsigned long long> messages_received;
std::atomic<unsigned long long> bytes_received;


// TODO: Use the class defined in felixcore
struct FELIXHeader
{
    uint16_t length;
    uint16_t status;
    uint32_t elinkid : 6;
    uint32_t gbtid : 26;
};



static void
run_subscribe(netio::context& ctx, const char* host, unsigned short port, std::vector<netio::tag> tags, std::string filename)
{
	std::ostream* out = NULL;
	if (filename != "") {
	    out = new std::ofstream(filename);
	}

	netio::subscribe_socket socket(&ctx);
	for(unsigned i=0; i<tags.size(); i++)
	socket.subscribe(tags[i], netio::endpoint(host, port));

	messages_received.store(0);

	while(true)
	{
		netio::message m;
		socket.recv(m);
    if (out) {
      std::vector<uint8_t> d = m.data_copy();
      for (size_t i=0; i<d.size(); i++) {
        *out << d[i];
      }
    }
		messages_received++;
		bytes_received += m.size();
	}
}


static void
run_subscribe_interactive(netio::context& ctx, const char* host, unsigned short port, std::vector<netio::tag> tags)
{
	netio::subscribe_socket socket(&ctx);
	for(unsigned i=0; i<tags.size(); i++)
	socket.subscribe(tags[i], netio::endpoint(host, port));

	messages_received.store(0);

	while(true)
	{
		netio::message m;
		socket.recv(m);
		auto d = m.data_copy();
		uint8_t* d_c = d.data();
		FELIXHeader* h = (FELIXHeader*)d_c;

		std::cout << "Message received:"
		          << " size=" << m.size()
		          << " len-in-header=" << h->length
		          << " status=" << h->status
		          << " gbtid=" << h->gbtid
		          << " elink=" << h->elinkid
		          << ": ";
		for(unsigned i=8; i<m.size(); i++)
		{
			std::cout << std::hex << "'" << d_c[i] << "[0x" << (int)d_c[i] << "]" << "'" << std::dec << " ";
		}
		std::cout << std::endl;
	}
}


int
main(int argc, char** argv)
{
	std::string host = "127.0.0.1";
	unsigned short port = 12345;
	std::string backend = "posix";
	std::vector<netio::tag> tags;
	bool interactive = false;
    std::string filename = "";


	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:t:iVh")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'f':
			filename = optarg;
			break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 't':
			tags.push_back(atoi(optarg));
			break;
		case 'i':
			interactive = true;
			break;
	        case 'V':
			std::cerr << "Version: " << FELIX_TAG << std::endl;
			return -1;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	netio::context ctx(backend.c_str());

	typedef std::chrono::high_resolution_clock clock;
	auto t0 = clock::now();

	netio::timer tmr(ctx.event_loop(), [&](void*){
		auto t1 = clock::now();
		double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
		unsigned long long messages_delta = std::atomic_exchange(&messages_received, 0ull);
		unsigned long long bytes_delta = std::atomic_exchange(&bytes_received, 0ull);

		std::cout << "Statistics - timestamp " << std::chrono::duration_cast<std::chrono::seconds>(t1.time_since_epoch()).count() << std::endl;
		std::cout << "  timedelta [s]: " << seconds << std::endl;
		std::cout << "  data received [MB]: " << bytes_delta/1024./1024. << std::endl;
		std::cout << "  messages_received: " << messages_delta << std::endl;
		std::cout << "  message rate [kHz]: " << messages_delta/seconds/1000 << std::endl;
		std::cout << "  data rate [GB/s]: " << bytes_delta/seconds/1024./1024./1024. << std::endl;
		std::cout << "  data rate [Gb/s]: " << bytes_delta/seconds/1024./1024./1024.*8 << std::endl;
		if(messages_delta > 0)
			std::cout << "  average message size [Byte]: " << bytes_delta/messages_delta << std::endl;
		t0 = t1;
	}, NULL);

	if(!interactive)
		tmr.start(1000);

	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});

	if(interactive)
	{
		run_subscribe_interactive(ctx, host.c_str(), port, tags);
	}
	else
	{
		run_subscribe(ctx, host.c_str(), port, tags, filename);
	}
}
