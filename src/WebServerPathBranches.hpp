/*
 * WebServerPathBranches.hpp
 *
 *  Created on: Oct 17, 2018
 *      Author: gleventi
 */

#ifndef FELIXCORE_SRC_WEBSERVERPATHBRANCHES_HPP_
#define FELIXCORE_SRC_WEBSERVERPATHBRANCHES_HPP_


#include <string>
#include <map>


namespace felix {
namespace core {


struct LegendStruct {
	std::string title;
	std::string unitX;
	std::string unitY;
};

enum pathType	:	unsigned short int {
	tHistogram				= 0x40,
	tGraph					= 0x80,
	tTable					= 0xC0
};

enum pathName	:	unsigned short int {
	nElinksToHost			= 0x00,
	nElinksFromHost			= 0x01,
	nChunksLength			= 0x02,
	nSubchunksLength		= 0x03,
	nQueues					= 0x04,
	nBlocksPerThread		= 0x05,
	nBlocksPerTransfer		= 0x06,
	nElinksPorts			= 0x07,
	nSubchunkType			= 0x08,
	nMemory					= 0x09,
	nRates					= 0x0A,
	nQueueSize				= 0x0B,
	nThroughput				= 0x0C,
	nGeneral				= 0x0D,
	nCard					= 0x0E,
	nErrors					= 0x0F,
	nCmd					= 0x10,
	nDebug					= 0x11,
	nDataGenerator			= 0x12
};

struct WebPathsLegends {
	WebPathsLegends();

	static std::map<std::string, pathType> pathTypeMap;
	static std::map<std::string, pathName> pathNameMap;
	static std::map<unsigned short int, LegendStruct> pathLegends;
};



} /* namespace core */
} /* namespace felix */

#endif /* FELIXCORE_SRC_WEBSERVERPATHBRANCHES_HPP_ */
