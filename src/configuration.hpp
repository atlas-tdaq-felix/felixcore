#ifndef FELIXCORE_CONFIGFILE_HPP
#define FELIXCORE_CONFIGFILE_HPP

#include <unordered_map>
#include <vector>
#include <istream>
#include <ostream>
#include <stdexcept>
#include <string>

#include "boost/algorithm/string.hpp"
#include "docopt/docopt.h"
#include "nlohmann/json.hpp"

#include "felixbase/common.hpp"
#include "felixbase/logging.hpp"

namespace felix
{
namespace core
{

struct hex_uint {
  // FIXME, is this correct
  hex_uint(): value(0) {};
  hex_uint(unsigned val): value(val) {};
  friend std::istream &operator>>(std::istream &in, hex_uint &cd) { return in >> cd.value; }
  friend std::ostream &operator<<(std::ostream &out, hex_uint const &cd) { return out << cd.value; }

  uint value;
};
//void validate(boost::any& v, std::vector<std::string> const& values, hex_uint* /* target_type */, int);

class FelixConfiguration
{

public:
  struct Values
  {
    // General
    std::string logfile;
    bool debug_dump_chunks;
    unsigned threads;
    unsigned port;
    unsigned port_recv;
    unsigned port_busy;
    std::string netio_backend;
    bool data_generator;
    int ttc_generator;
    unsigned buffer;
    hex_uint felix_id;
    std::string data_interface;
    std::string monit_interface;
    bool noweb;
    unsigned web_port;

    // Commandline
    bool verbose;
    bool quiet;
    std::string file;
    std::string config_file;

    // Card
    std::vector<std::string> devices;
    unsigned memory;
    unsigned megabyte;
    bool polling;
    unsigned poll_time;
    unsigned dma;
    unsigned toflx_dma;
    bool toflx_cdma;
    std::string elink_numbers;
    bool notoflx;
    bool nobusy;
    std::string streams_str;
    std::set<unsigned> streamEnabledElinks;

    // Data Generator
    bool fixed_chunks;
    bool rate_control_bool;

    int period;
    int max_overshoot;
    int duration;

    unsigned e_link_min;
    unsigned e_link_max;

    hex_uint e_link_id_min;
    hex_uint e_link_id_max;

    int chunk_size_fixed;
    int chunk_size_min;
    int chunk_size_max;

    // Debug
    bool nostats;
    bool display_stats;
    bool nohistos;
    bool pmstats;
    bool trace;
  };

  static FelixConfiguration& instance();
  void clear();

  void parse_args(int argc, char** argv);
  const Values& values();

  nlohmann::json toJson(std::vector<std::string> path, std::vector<std::string> query);

private:
  static std::unique_ptr<FelixConfiguration> m_instance;
  Values m_values;

  FelixConfiguration() {}
  std::map<std::string, docopt::value> load_file(std::istream& in);
  void store_file(std::ostream& out, std::map<std::string, docopt::value>& args, std::map<std::string, docopt::value>& defaults);
  void set_members(std::map<std::string, docopt::value>& args);
};
}
}


#endif
