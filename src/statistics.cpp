#include "statistics.hpp"

#include "card_queue_source.hpp"

namespace felix {
namespace core {

// Static variables implementation
StatCountersStruct	*	Statistics::accumulatedData;
std::thread				Statistics::mainStatisticsThread;
bool					Statistics::keepAlive;


Statistics::Statistics(unsigned int no_of_devices, unsigned int no_of_threads) {
	INFO("Statistics with Devices " << no_of_devices << " and Threads " << no_of_threads);
	timestamp							= time(NULL);
	number_of_devices					= no_of_devices;
	number_of_queues					= no_of_devices * no_of_threads;
	message_size_at_last_sync			= new unsigned long long[number_of_queues];
	messageRate							= new double			[number_of_queues];
	accumulatedData						= new StatCountersStruct;
	for (unsigned int i = 0; i < number_of_queues; ++i) {
		message_size_at_last_sync[i]	= 0;
		messageRate[i]					= 0;
	}
	keepAlive = true;
	mainStatisticsThread = std::thread(&Statistics::runStatisticsThread, this);
	set_thread_name(mainStatisticsThread, "Main Statistics");
}

Statistics::~Statistics() {
	keepAlive = false;
	mainStatisticsThread.join();
	delete[] message_size_at_last_sync;
	delete[] messageRate;
	delete accumulatedData;
}

// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
Statistics& Statistics::getInstance() {
	static Statistics instance(getNoOfDevices(), getMaxThreads());
	return instance;
}

void Statistics::runStatisticsThread() {
	if (FelixConfiguration::instance().values().nostats) {
		INFO("No statistics gathered, remove --nostats");
		return;
	}
	try {
		INFO("Starting Stats Thread");
		if (!FelixConfiguration::instance().values().display_stats)
			INFO("No statistics printed, use --display_stats");
		while(keepAlive) {
			update();
			usleep(STATISTICSUPDATEINTERVAL);
			if (FelixConfiguration::instance().values().display_stats)
				display_statistics();
		}
	} catch (std::exception& e) {
		CRITICAL("Uncaught exception in statistics thread: " << e.what());
		throw;
	}
	INFO("Stopping statistics thread...");
}

void Statistics::update() {
	for (unsigned int i = 0; i < number_of_devices; ++i)
		accumulatedData->freeBlocksC[i] = 0;
	for (unsigned int i = 0; i < number_of_queues; ++i) {
		accumulatedData->freeBufferC[i] = 0;
		accumulatedData->queueInstancesCurrentSizeC[i] = 0;
	}
	for (std::forward_list<StatCountersStruct *,std::allocator<StatCountersStruct *>>::iterator it = statisticsWorkersList.begin(); it != statisticsWorkersList.end(); ++it )
		StatCountersStruct::accumulate(**it, *accumulatedData);

	timestamp = time(NULL);
	tbb::tick_count t1 = tbb::tick_count::now();
	double seconds = (t1-t0).seconds();

	// Averages and totals
	double totalQueueSize = 0.0;
	throughput = 0;
	maxQueueSize = 0;
	long long unsigned int temp;
	for (unsigned i = 0; i < number_of_queues; i++) {
		temp =  accumulatedData->queueInstancesCurrentSizeC[i].load();
		totalQueueSize += temp;
		if (temp > maxQueueSize) {
			maxQueueSize = temp;
		}

		messageRate[i] = (accumulatedData->messageSizeInQueue->counter[i].load() - message_size_at_last_sync[i])	/ seconds * 1e-6;
		throughput += messageRate[i];
	}
	avgQueueSize = totalQueueSize / number_of_queues;

	// rates
	unsigned long long int tempBlocks		= accumulatedData->blocks		.load();
	unsigned long long int tempChunks		= accumulatedData->chunks		.load();
	unsigned long long int tempShortchunks	= accumulatedData->shortchunks	.load();
	blockRate = (tempBlocks - blocks_at_last_sync)																	/ seconds * 1e-6;
	chunkRate = (tempChunks - chunks_at_last_sync + tempShortchunks - shortchunks_at_last_sync)						/ seconds * 1e-6;

	blocks_at_last_sync = tempBlocks;
	chunks_at_last_sync = tempChunks;
	shortchunks_at_last_sync = tempShortchunks;
	for (unsigned i = 0; i < number_of_queues; i++)
		message_size_at_last_sync[i] = accumulatedData->messageSizeInQueue->counter[i].load();
	t0 = t1;
}

void Statistics::display_statistics(std::ostream& out) {
	out		<< 	"Statistics - timestamp "			<<	timestamp			 									<< std::endl
			<<	"  Blocks (total): \t\t"			<<	accumulatedData->blocks									<< std::endl
			<<	"  Buffer Free (total): \t\t"		<<	accumulatedData->freeBufferC							<< std::endl
			<<	"  Queue Size (avg): \t\t"			<<	avgQueueSize											<< std::endl
			<<	"  Queue Size (max): \t\t"			<<	maxQueueSize											<< std::endl
			<<	"  ";
	for (unsigned i = 0; i<number_of_queues; i++)
		out <<	accumulatedData->queueInstancesCurrentSizeC[i]													<<	" ";
	out																											<< std::endl
			<<	"  Malformed subchunks: \t\t"		<<	accumulatedData->malformedSubchunks						<< std::endl
			<<	"  Malformed chunks: \t\t"			<<	accumulatedData->malformedChunks						<< std::endl
			<<	"  Malformed blocks: \t\t"			<<	accumulatedData->malformedBlocks						<< std::endl
			<<	"  Long Chunks (total): \t\t"		<<	accumulatedData->chunks									<< std::endl
			<<	"  Short Chunks (total): \t"		<<	accumulatedData->shortchunks							<< std::endl
			<<	"  SizeError Chunks (total): \t"	<<	accumulatedData->sizeFaultyChunks						<< std::endl
			<<	"  Error Chunks (total): \t"		<<	accumulatedData->faultyChunks							<< std::endl
			<<	"  Truncated Chunks (total): \t"	<<	accumulatedData->truncatedChunks						<< std::endl
			<<	"  CRC Error Chunks (total): \t"	<<	accumulatedData->crcerrChunks						<< std::endl
			<<	"  All Chunks (total): \t\t"		<<	accumulatedData->chunks + accumulatedData->shortchunks	<< std::endl
			<<	"  Chunk Rate [MChunks/s]: \t"		<<	chunkRate												<< std::endl
			<<	"  Block Rate [MBlocks/s]: \t"		<<	blockRate												<< std::endl;
}

} /* namespace core */
} /* namespace felix */
