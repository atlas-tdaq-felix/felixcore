/*
 * StatCountersStruct.cpp
 *
 *  Created on: Oct 11, 2018
 *      Author: gleventi
 */

#include "StatCountersStruct.hpp"

#include "definitions.hpp"      // Max and min of counters
#include <iostream>

namespace felix {
namespace core {

void SingleCounterStruct::initAtomicArray(
    std::atomic<unsigned long long int>*& arrayPtr,
    unsigned long int arraySize) {
  arrayPtr = new std::atomic_ullong[arraySize];
  for (unsigned long int index = 0; index < arraySize; ++index)
    arrayPtr[index] = ATOMIC_VAR_INIT(0ull);
}

SingleCounterStruct::SingleCounterStruct(unsigned long int mainCounterArraySize) :
    counterSize(mainCounterArraySize) {
  initAtomicArray(counter, mainCounterArraySize);
}

SingleCounterStruct::~SingleCounterStruct() {
  if (counter)
    delete[] counter;
}

void SingleCounterStruct::accumulate(SingleCounterStruct*& part, SingleCounterStruct*& accumulated) {
  for (unsigned long int index = 0; index < part->counterSize; ++index)
    accumulated->counter[index] += part->counter[index].exchange(0);
  accumulated->underFlowCounter += part->underFlowCounter.exchange(0);
  accumulated->overFlowCounter  += part->overFlowCounter.exchange(0);
  accumulated->totalCounter     += part->totalCounter.exchange(0);
}

StatCountersStruct::StatCountersStruct() {
  SingleCounterStruct::initAtomicArray(queueInstancesCurrentSizeC, getMaxThreadAllLinks());
  SingleCounterStruct::initAtomicArray(freeBufferC, getMaxThreadAllLinks());
  SingleCounterStruct::initAtomicArray(freeBlocksC, getNoOfDevices());
  //SingleCounterStruct::initAtomicArray(seqnoC, SEQ_NUM_MAX - SEQ_NUM_MIN + 1);

  eLinkFromFelix     = new SingleCounterStruct(getMaxeLinksGeneral());
  eLinkToFelix       = new SingleCounterStruct(getMaxeLinksGeneral());
  chunkLength        = new SingleCounterStruct(CHUNK_LENGTH_RANGE);
  subchunkLength     = new SingleCounterStruct(SUBCHUNK_LENGTH_RANGE);
  subchunkType       = new SingleCounterStruct(SUBCHUNK_TYPE_ENUM_RANGE);
  blocksInQueue      = new SingleCounterStruct(BLOCKS_IN_A_QUEUE_RANGE);
  blocksPerThread    = new SingleCounterStruct(getMaxThreadAllLinks());
  blocksPerTransfer  = new SingleCounterStruct(BLOCKS_PER_TRANSFER_RANGE);
  elinkPorts         = new SingleCounterStruct(getMaxeLinksGeneral());
  messageSizeInQueue = new SingleCounterStruct(getMaxThreadAllLinks());
  statisticsCategory = allStats;
}

StatCountersStruct::~StatCountersStruct() {
  delete  []  queueInstancesCurrentSizeC;
  delete  []  freeBufferC;
  delete  []  freeBlocksC;
  //delete  []  seqnoC;

  delete eLinkFromFelix;
  delete eLinkToFelix;
  delete chunkLength;
  delete subchunkLength;
  delete subchunkType;
  delete blocksInQueue;
  delete blocksPerThread;
  delete blocksPerTransfer;
  delete elinkPorts;
  delete messageSizeInQueue;
}

void StatCountersStruct::accumulate(StatCountersStruct& part, StatCountersStruct& accumulated) {

  if (part.statisticsCategory & pipelineStats) {
    accumulated.chunks             += part.chunks.exchange(0);
    accumulated.shortchunks        += part.shortchunks.exchange(0);
    accumulated.faultyChunks       += part.faultyChunks.exchange(0);
    accumulated.sizeFaultyChunks   += part.sizeFaultyChunks.exchange(0);
    accumulated.truncatedChunks    += part.truncatedChunks.exchange(0);
    accumulated.crcerrChunks       += part.crcerrChunks.exchange(0);
    accumulated.malformedSubchunks += part.malformedSubchunks.exchange(0);
    accumulated.malformedChunks    += part.malformedChunks.exchange(0);
    accumulated.malformedBlocks    += part.malformedBlocks.exchange(0);

    SingleCounterStruct::accumulate(part.chunkLength, accumulated.chunkLength);
    SingleCounterStruct::accumulate(part.subchunkLength, accumulated.subchunkLength);
    SingleCounterStruct::accumulate(part.subchunkType, accumulated.subchunkType);
    SingleCounterStruct::accumulate(part.blocksInQueue, accumulated.blocksInQueue);
    SingleCounterStruct::accumulate(part.messageSizeInQueue, accumulated.messageSizeInQueue);
  }
  if (part.statisticsCategory & queueSourceStats) {
    accumulated.blocks += part.blocks.exchange(0);

    SingleCounterStruct::accumulate(part.eLinkFromFelix, accumulated.eLinkFromFelix);
    SingleCounterStruct::accumulate(part.blocksPerThread, accumulated.blocksPerThread);
    SingleCounterStruct::accumulate(part.blocksPerTransfer, accumulated.blocksPerTransfer);
    SingleCounterStruct::accumulate(part.elinkPorts, accumulated.elinkPorts);

    unsigned int index = 0;
    for (; index < getMaxThreadAllLinks(); ++index)
      accumulated.queueInstancesCurrentSizeC[index] += part.queueInstancesCurrentSizeC[index].exchange(0);
    for (index = 0; index < getMaxThreadAllLinks(); ++index)
      accumulated.freeBufferC[index] += part.freeBufferC[index].exchange(0);
    for (index = 0; index < getNoOfDevices(); ++index)
      accumulated.freeBlocksC[index] += part.freeBlocksC[index].exchange(0);
  }
  if (part.statisticsCategory & toFLXTrheadStats) {
    SingleCounterStruct::accumulate(part.eLinkToFelix, accumulated.eLinkToFelix);
  }

//    This code is here in chance we want to reverse / remove the changes that categorize stats structures so every structure is copied in full
//    accumulated.chunks             += part.chunks.exchange(0);
//    accumulated.shortchunks        += part.shortchunks.exchange(0);
//    accumulated.blocks             += part.blocks.exchange(0);
//    accumulated.faultyChunks       += part.faultyChunks.exchange(0);
//    accumulated.sizeFaultyChunks   += part.sizeFaultyChunks.exchange(0);
//    accumulated.truncatedChunks    += part.truncatedChunks.exchange(0);
//    accumulated.malformedSubchunks += part.malformedSubchunks.exchange(0);
//    accumulated.malformedChunks    += part.malformedChunks.exchange(0);
//    accumulated.malformedBlocks    += part.malformedBlocks.exchange(0);
////  accumulated.seqnoTotalC        += part.seqnoTotalC.exchange(0);
//
//    SingleCounterStruct::accumulate(part.eLinkFromFelix,    accumulated.eLinkFromFelix);
//    SingleCounterStruct::accumulate(part.eLinkToFelix ,     accumulated.eLinkToFelix);
//    SingleCounterStruct::accumulate(part.chunkLength ,      accumulated.chunkLength);
//    SingleCounterStruct::accumulate(part.subchunkLength ,   accumulated.subchunkLength);
//    SingleCounterStruct::accumulate(part.subchunkType,      accumulated.subchunkType);
//    SingleCounterStruct::accumulate(part.blocksInQueue,     accumulated.blocksInQueue);
//    SingleCounterStruct::accumulate(part.blocksPerThread,   accumulated.blocksPerThread);
//    SingleCounterStruct::accumulate(part.blocksPerTransfer,   accumulated.blocksPerTransfer);
//    SingleCounterStruct::accumulate(part.elinkPorts,      accumulated.elinkPorts);
//    SingleCounterStruct::accumulate(part.messageSizeInQueue,  accumulated.messageSizeInQueue);
//
//    unsigned int index = 0;
//    for (; index < getMaxThreadAllLinks(); ++index)
//      accumulated.queueInstancesCurrentSizeC[index] += part.queueInstancesCurrentSizeC[index].exchange(0);
//    for (index = 0; index < getMaxThreadAllLinks(); ++index)
//      accumulated.freeBufferC[index] += part.freeBufferC[index].exchange(0);
//    for (index = 0; index < getNoOfDevices(); ++index)
//      accumulated.freeBlocksC[index] += part.freeBlocksC[index].exchange(0);
////    for (index = 0; index < 0; ++index)
////      accumulated.seqnoC[index]    += part.seqnoC[index].exchange(0);

}

} /* namespace core */
} /* namespace felix */
