#ifndef FELIXCORE_TTCGenerator_QueueSource_HPP
#define FELIXCORE_TTCGenerator_QueueSource_HPP

#include <random>
#include <iostream>

#include <cstdint>
#include <ctime>
#include <vector>

#include "felixbase/block_checker.hpp"

#include "rate_limiter.hpp"
#include "queue_source.hpp"

namespace felix
{
namespace core
{

class TTCGeneratorQueueSource : public QueueSource
{
private:
  struct State {
    unsigned seqnr;
    unsigned continuation_size;
    unsigned BCID;
    unsigned L1ID;
  };

public:
  TTCGeneratorQueueSource(BlockQueue** buffers, BlockQueue** queues, size_t num_queues);
  virtual ~TTCGeneratorQueueSource();
  virtual bool read();

private:

  felix::base::BlockChecker *blockChecker;
  RateLimiter limit;



  bool rate_control_bool;


  static const timestamp_t ns = 1;
  static const timestamp_t us = 1000 * ns;
  static const timestamp_t ms = 1000 * us;
  static const timestamp_t s  = 1000 * ms;

  timestamp_t now;
  timestamp_t deadline;
  timestamp_t period_ts;
  timestamp_t max_overshoot_ts;
  timestamp_t duration_ts;


  State* state;
};

}
}

#endif
