#ifndef FELIXCORE_CARD_QueueSource_HPP
#define FELIXCORE_CARD_QueueSource_HPP

#include <cstring>
#include <set>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <thread>
#include <atomic>
#include <fcntl.h>

#include "DFDebug/DFDebug.h"

#include "felixbase/logging.hpp"
#include "felixbase/block_checker.hpp"

#include "packetformat/block_format.hpp"
#include "packetformat/block_parser.hpp"

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

#include "common.hpp"
#include "block_queue.hpp"
#include "queue_source.hpp"
#include "interrupts.hpp"
#include "circular_dma.hpp"

namespace felix {
namespace core {

class CardQueueSource : public QueueSource {
public:
  static u_long getNumberOfDevices() {
    u_long n = FlxCard::number_of_devices();
    return n > 0 ? n : 0;
  }
  static u_long probeDevice(int deviceNo) {
    try {
      FlxCard card;
      card.card_open(deviceNo, LOCK_NONE);
      u_long cardModel = card.card_model();
      card.card_close();

      return cardModel;
    } catch(FlxException& ex) {
      WARNING("Unrecognized card model: " << ex.what());
      return 0;
    }
  }
  void resetInterruptFlags(void);
  void resetInterruptCounters(void);

  CardQueueSource(unsigned int index, unsigned int deviceNo, BlockQueue** buffers, BlockQueue** queues, size_t num_queues);
  ~CardQueueSource();
  void reset();
  bool read();

  bool trailer32Bit() { return trailerIs32Bit; }
  int fromHostFormat() { return fromHostDataFormat; }
  int nrOfDescriptors() { return numberOfDescriptors; }

private:
  unsigned int deviceNo;
  FlxCard flxCard;

  int card_fd;
  std::unique_ptr<CircularDMA> circular_dma;


  u_long opt_emu_ena_to_host, opt_emu_ena_to_frontend, opt_tohost_fo, opt_tofrontend_fo;

  int cmem_handle;            // handle to the DMA memory block
  u_long memSize;             // size of the DMA memory block

  u_long startAddress;        // pointer to the start of the DMA memory block
  u_long currentAddress;      // pointer to the current write position for the card
  u_long blockIndex;          // last block index set to read_ptr
  u_long virtualAddress;      // virtual address of the DMA memory block
  // u_long tlpMask = ~(m_maxTLPBytes -1);
  u_long tlp;
  u_long tlpMask = ~ (0x3FF);
  bool   trailerIs32Bit;
  int    fromHostDataFormat;
  int    numberOfDescriptors;

  u_long numberOfBlocks;

  felix::base::BlockChecker* blockChecker;
};

} /* namespace core */
} /* namespace felix */

#endif
