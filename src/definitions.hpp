#ifndef FELIXCORE_DEFINITIONS_HPP
#define FELIXCORE_DEFINITIONS_HPP

#define LINKS 24
#define ELINK_MIN 0
#define ELINKS_PER_LINK_MAX 64
#define ELINKS_PER_DEVICE_MAX 2048
//#define ELINKS_PER_DEVICE_MAX (LINKS*ELINKS_PER_LINK_MAX)
#define ELINK_OF_DEVICE_INDEX_MAX (ELINKS_PER_DEVICE_MAX - 1)
unsigned short int getNoOfDevices();
inline unsigned long int getMaxeLinksGeneral() { return getNoOfDevices() * ELINKS_PER_DEVICE_MAX; }

#define ELINK_GENERAL_INDEX_MIN 0
inline unsigned long int getMaxeLinkGeneralIndex() { return getMaxeLinksGeneral() - 1; }
inline unsigned long int geteLinkGeneralIndexRange() { return getMaxeLinksGeneral(); }

long unsigned int getBlockSize();

#define CHUNK_LENGTH_MIN 0
#define CHUNK_LENGTH_MAX 4095
#define CHUNK_LENGTH_RANGE CHUNK_LENGTH_MAX - CHUNK_LENGTH_MIN + 1

#define SUBCHUNK_LENGTH_MIN 0
#define SUBCHUNK_LENGTH_MAX 1018
#define SUBCHUNK_LENGTH_RANGE SUBCHUNK_LENGTH_MAX - SUBCHUNK_LENGTH_MIN + 1

#define SUBCHUNK_TYPE_ENUM_MIN 0
#define SUBCHUNK_TYPE_ENUM_MAX 7
#define SUBCHUNK_TYPE_ENUM_RANGE SUBCHUNK_TYPE_ENUM_MAX - SUBCHUNK_TYPE_ENUM_MIN + 1

#define SEQ_NUM_MIN 0
#define SEQ_NUM_MAX 150
#define SEQ_NUM_RANGE SEQ_NUM_MAX - SEQ_NUM_MIN + 1

#define THREADS_MIN 0
unsigned long int getMaxThreads();
inline unsigned long int getMaxThreadIndex() { return getMaxThreads() - 1; }
inline unsigned long int getThreadIndexRange() { return getMaxThreadIndex(); }
inline unsigned long int getMaxThreadAllLinks() { return getMaxThreads() * getNoOfDevices(); }

#define BLOCKS_PER_TRANSFER_MIN 0
#define BLOCKS_PER_TRANSFER_MAX 10000
#define BLOCKS_PER_TRANSFER_RANGE BLOCKS_PER_TRANSFER_MAX - BLOCKS_PER_TRANSFER_MIN + 1

#define PORTS_MIN 0
#define PORTS_MAX 65535
#define PORTS_RANGE PORTS_MAX - PORTS_MIN + 1

#define DEVICES_MIN 0
#define DEVICES_MAX 6
#define DEVICES_RANGE DEVICES_MAX - DEVICES_MIN + 1

#define MESSAGE_SIZE_MIN 0
#define MESSAGE_SIZE_MAX 8191
#define MESSAGE_SIZE_RANGE MESSAGE_SIZE_MAX - MESSAGE_SIZE_MIN + 1

#define BLOCKS_IN_A_QUEUE_MIN 1
#define BLOCKS_IN_A_QUEUE_MAX 1000
#define BLOCKS_IN_A_QUEUE_RANGE BLOCKS_IN_A_QUEUE_MAX - BLOCKS_IN_A_QUEUE_MIN + 1

#define QUEUE_INDEX_MIN 0
#define QUEUE_INDEX_MAX 1000
#define QUEUE_INDEX_RANGE QUEUE_INDEX_MAX - QUEUE_INDEX_MIN + 1

unsigned int getAvailableHardwareCores();
inline unsigned int getMinSourceThread() { return 0; }
unsigned short int getNoOfSourceThreads();
inline short int getNoOfWorkerThreads() { return getAvailableHardwareCores() - getNoOfSourceThreads(); }

namespace felix {
namespace core {

class Definitions {
public:
	static unsigned short int numberOfDevices;
};
}}

#endif
