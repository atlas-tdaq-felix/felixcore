#include "block_source.hpp"

felix::core::SmartBlockPtr::SmartBlockPtr(felix::packetformat::block* b, BlockQueue* buffer, unsigned queueIndex, bool shouldDelete)
{
  data = new SmartBlockPtrData(b, buffer, queueIndex, shouldDelete);
}

felix::core::SmartBlockPtr::SmartBlockPtr()
{
  data = NULL;
}

felix::core::SmartBlockPtr::SmartBlockPtr(const SmartBlockPtr& s)
{
  data = s.data;
  data->count++;
  // INFO("Copied " << data->count);
}

felix::core::SmartBlockPtr::~SmartBlockPtr()
{
  reset();
}

felix::core::SmartBlockPtr& felix::core::SmartBlockPtr::operator=(const felix::core::SmartBlockPtr& other)
{
  if(this==&other)
  return *this;

  reset();

  data = other.data;
  if(data == NULL)
  return *this;

  data->count++;
  // INFO("Assigned " << data->count);

  return *this;
}

felix::packetformat::block& felix::core::SmartBlockPtr::operator*()
{
  return *(data->pointee);
}

felix::packetformat::block* felix::core::SmartBlockPtr::get()
{
  return data->pointee;
}

void felix::core::SmartBlockPtr::reset()
{
  if(data == NULL)
  return;

  data->count--;
  //INFO("Reset " << data->count);
  if(data->count == 0)
  {
    destroy();
    delete data;
  }
  data = NULL;
}

unsigned felix::core::SmartBlockPtr::use_count() const
{
  if(data == NULL)
    return 0;
  return data->count;
}

void felix::core::SmartBlockPtr::set_prev(felix::core::SmartBlockPtr& p)
{
  if(data)
  {
    data->prev = p;
  }
}

void felix::core::SmartBlockPtr::cut_reference_chain()
{
  if(data)
  {
    //INFO("cutting reference chain " << data->count);
    data->prev.reset();
  }
}



void felix::core::SmartBlockPtr::destroy()
{
  if (data) {
    if (data->shouldDelete) {
      data->buffer->push(data->pointee);
    }
  }
}
