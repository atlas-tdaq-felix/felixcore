#pragma once

#include "configuration.hpp"
#include "netio/netio.hpp"
#include "felixbase/client.hpp"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

namespace felix {
namespace core {

class BusyThread
{
public:
  BusyThread(unsigned int card_no, unsigned short port);
  ~BusyThread();
  void start();

private:
  FelixConfiguration::Values cfg;
  unsigned int card_no;
  unsigned short port;
  netio::context ctx;
	netio::low_latency_recv_socket socket;
	std::unique_ptr<std::thread> bg_thread;
  FlxCard flx;

  void busy_callback(netio::endpoint& ep, netio::message& msg);

};

}
}
