#ifndef FELIXCORE_RANDOM_QueueSource_HPP
#define FELIXCORE_RANDOM_QueueSource_HPP

#include <random>
#include <iostream>

#include <cstdint>
#include <ctime>
#include <vector>

#include "felixbase/block_checker.hpp"

#include "rate_limiter.hpp"
#include "queue_source.hpp"

namespace felix
{
namespace core
{

class RandomQueueSource : public QueueSource
{
private:
  struct State {
    unsigned seqnr;
    unsigned continuation_size;
    unsigned BCID;
    unsigned L1ID;
  };

public:
  RandomQueueSource(BlockQueue** buffers, BlockQueue** queues, size_t num_queues);
  virtual ~RandomQueueSource();
  virtual bool read();

private:
  void generateSizes();

  void generateElinks();

  felix::base::BlockChecker *blockChecker;
  RateLimiter limit;

  int chunk_size_fixed;

  const unsigned int header_length = 4;
  const unsigned int data_trailer_length = 4;
  const unsigned int trailer_length = 2;
  const unsigned int block_length = 1024;

  bool fixed_chunks;

  bool rate_control_bool;

  static const timestamp_t ns = 1;
  static const timestamp_t us = 1000 * ns;
  static const timestamp_t ms = 1000 * us;
  static const timestamp_t s  = 1000 * ms;

  timestamp_t now;
  timestamp_t deadline;
  timestamp_t period_ts;
  timestamp_t max_overshoot_ts;
  timestamp_t duration_ts;

  std::vector<unsigned int> random_elink_vec;
  int random_elink_index = 0;

  std::vector<uint16_t> random_size_vec;
  int random_size_index = 0;

  State* state;
  unsigned int elinkOffset;
};

}
}

#endif
