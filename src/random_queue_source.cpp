#include "random_queue_source.hpp"

felix::core::RandomQueueSource::RandomQueueSource(BlockQueue** buffers, BlockQueue** queues, size_t num_queues) : QueueSource(0, buffers, queues, num_queues, 0) {
  rate_control_bool = cfg.rate_control_bool;
  fixed_chunks = cfg.fixed_chunks;
  chunk_size_fixed = cfg.chunk_size_fixed;

  limit = RateLimiter();
  period_ts = cfg.period * us;
  max_overshoot_ts = cfg.max_overshoot * ms;
  duration_ts = cfg.duration * s;

  now = limit.gettime();
  deadline = now + period_ts;

  elinkOffset = cfg.e_link_id_min.value;
  blockChecker = new felix::base::BlockChecker(elinkOffset, cfg.e_link_id_max.value);

  generateSizes();
  generateElinks();

  unsigned int range = 1 + cfg.e_link_id_max.value - elinkOffset;
  state = new State[range];

  for(unsigned int loop = 0; loop < range; loop++) {
    state[loop].seqnr = 0;
    state[loop].continuation_size = 0;
    state[loop].BCID = 0;
    state[loop].L1ID = 0;
  }

  queueSourceThread = std::thread(&QueueSource::runQueueSourceThread<RandomQueueSource>, this);
}

felix::core::RandomQueueSource::~RandomQueueSource() {
  delete blockChecker;
  delete state;
}

bool felix::core::RandomQueueSource::read() {
  unsigned int elink = random_elink_vec.at(random_elink_index);
  random_elink_index = (random_elink_index + 1) % random_elink_vec.size();

  Block* block = new Block();

  //block header
  block->elink = elink + elinkOffset;
  block->seqnr = state[elink].seqnr;
  block->sob = 0xABCD;

  // increment sequence number
  state[elink].seqnr = (state[elink].seqnr+1) % 32;

  uint16_t total_chunk_size = header_length;
  std::vector<uint16_t> random_chunk_size_vec;
  std::vector<uint16_t> random_chunk_type_vec;
  std::vector<uint16_t> buffer_vec;

  while (total_chunk_size < block_length) {
    uint16_t random_chunk_size;
    unsigned int type;
    bool padding = false;

    if (state[elink].continuation_size == 0) {
      uint16_t random;
      if (rate_control_bool) {
        if (now > deadline + max_overshoot_ts) {
          deadline = now + period_ts;

          if (fixed_chunks) {
            random = chunk_size_fixed;
          } else {
            //random = dist6(rng);
            random = random_size_vec.at(random_size_index);
            random_size_index = (random_size_index + 1) % random_size_vec.size();
          }
          // FIXME random not set ???
        } else {
          while (now < deadline) {
            now = limit.gettime();
          }

          if (fixed_chunks) {
            random = chunk_size_fixed;
          } else {
            //random = dist6(rng);
            random = random_size_vec.at(random_size_index);
            random_size_index = (random_size_index + 1) % random_size_vec.size();
          }
          now = limit.gettime();
          deadline += period_ts;
        }
      } else {

        if (fixed_chunks) {
          random = chunk_size_fixed;
        } else {
          // random = dist6(rng);
          random = random_size_vec.at(random_size_index);
          random_size_index = (random_size_index + 1) % random_size_vec.size();
        }
      }

      //std::cout<<"RANDOM: "<<random<<std::endl;
      if (random + total_chunk_size + trailer_length  + header_length + data_trailer_length <= block_length) {
        type = felix::packetformat::subchunk::BOTH;
        if ((random % 2) == 1) {
          total_chunk_size = random + total_chunk_size + trailer_length + header_length + data_trailer_length + 1;
          padding = true;
        } else {
          total_chunk_size = random + total_chunk_size + trailer_length + header_length + data_trailer_length;
          padding = false;
        }

        if (total_chunk_size >= (block_length - trailer_length - header_length)){
          random += block_length-total_chunk_size;
          total_chunk_size = block_length;
        }

        random_chunk_size = random;
        state[elink].continuation_size = 0;
      } else {
        type = felix::packetformat::subchunk::FIRST;
        padding = false;
        total_chunk_size += header_length + trailer_length;
        random_chunk_size = block_length - total_chunk_size;
        //random_chunk_size += header_length;

        total_chunk_size = block_length;
        state[elink].continuation_size = random-random_chunk_size;
        if (state[elink].continuation_size == 0){
          type = felix::packetformat::subchunk::BOTH;
          random_chunk_size -= data_trailer_length;
        }
      }
    } else {
      if((state[elink].continuation_size + total_chunk_size + trailer_length + data_trailer_length) <= block_length) {
        type = felix::packetformat::subchunk::LAST;
        if ((state[elink].continuation_size % 2) == 1) {
          total_chunk_size = state[elink].continuation_size + total_chunk_size+trailer_length+data_trailer_length+1;


          padding = true;
        } else {
          total_chunk_size = state[elink].continuation_size + total_chunk_size+trailer_length+data_trailer_length;
          padding = false;
        }

        if (total_chunk_size >= (block_length - header_length - trailer_length)){
          state[elink].continuation_size += block_length-total_chunk_size;
          total_chunk_size = block_length;
        }

        random_chunk_size = state[elink].continuation_size;

        state[elink].continuation_size = 0;
      } else {
        type = felix::packetformat::subchunk::MIDDLE;
        total_chunk_size += trailer_length;
        random_chunk_size = block_length-total_chunk_size;
        total_chunk_size = block_length;
        padding = false;
        state[elink].continuation_size = state[elink].continuation_size - random_chunk_size;

        if (state[elink].continuation_size == 0){
          type = felix::packetformat::subchunk::LAST;
          random_chunk_size -= data_trailer_length;
        }
      }
    }
    //if (state[elink].seqnr< 100)
    //std::cout<<"chunk:  "<<random_chunk_size<<"type:  "<<type<<"continuation_number:  "<<state[elink].continuation_size<<"ELINK ID:  "<<elink<<"SQNR:  "<<seqnr[elink]<<"PADDING:  "<<padding<<"TOTAL CHUNK SIZE:  "<<total_chunk_size<<std::endl;
    random_chunk_size_vec.push_back(random_chunk_size);
    random_chunk_type_vec.push_back(type);
    buffer_vec.push_back(padding ? 1 : 0);
  }

  //Loop over each subchunk in block
  uint16_t i = 0;
  uint16_t total_data_size = 0;
  //INFO("START");
  for (uint16_t j = 0; j < random_chunk_size_vec.size(); j++) {
    //fill data for length of subchunk
    uint16_t data_size = random_chunk_size_vec.at(j);

    uint16_t data_type = random_chunk_type_vec.at(j);
    uint16_t padding_size = buffer_vec.at(j);

    total_data_size += data_size + padding_size;
    //INFO("DATA SIZE: "<<total_data_size);
    //INFO("DATA TYPE: "<<data_type);
    felix::packetformat::subchunk_trailer s;


    s.type = data_type;
    //s.length = data_size;

    if ((data_type == 1) || (data_type == 3)){
      //INFO("FIRST:  "<<(char)(state[elink].BCID & 0xFF));
      //INFO("SECOND:  "<<(char)(state[elink].BCID & 0xF00));
      //INFO("THIRD:  "<<(char)(state[elink].L1ID & 0xFF));
      //INFO("FOURTH:  "<<(char)(state[elink].L1ID & 0xFF00));
      // std::cout<<std::showbase<<std::hex<<state[elink].BCID<<std::endl;
      data_size += header_length;
      block->data[i++] = (char)(state[elink].BCID & 0xFF);
      block->data[i++] = (char)((state[elink].BCID & 0xF00) >> 8);
      block->data[i++] = (char)(state[elink].L1ID & 0xFF);
      block->data[i++] = (char)((state[elink].L1ID & 0xFF00) >> 8);
      state[elink].BCID = state[elink].BCID+1;
      state[elink].L1ID = state[elink].L1ID+1;
      if (state[elink].BCID == 65536){
        state[elink].BCID = 0;
      }
      if (state[elink].L1ID == 65536){
        state[elink].L1ID = 0;
      }
      total_data_size += header_length;
    }
    if ((data_type == 2) || (data_type == 3)){
      data_size += data_trailer_length;
      total_data_size += data_trailer_length;
    }

    s.length = data_size;


    while (i<total_data_size) {
      block->data[i++] = 0x00; //data
    }

    //trailer for each subchunk
    block->data[i++] = (char)(s.length & 0xFF);


    block->data[i] = (char)(s.type << 5);

    block->data[i++] += (char)((s.length & 0x300) >> 8);


    total_data_size += trailer_length;

  }
  //INFO("THIS IS I:"<<i);

  if (i != 1020) {
    ERROR("SOMETHING WRONG"<<i);
    return false;
  }

  if (!blockChecker->check(block)) {
    return false;
  }

  dispatch(block);

  delete block;

  return true;
}

void felix::core::RandomQueueSource::generateSizes() {
  // pre-generate random chunk sizes
  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_int_distribution<std::mt19937::result_type> dist6(cfg.chunk_size_min, cfg.chunk_size_max);
  INFO("Using Chunk sizes in range [" << cfg.chunk_size_min << ", " << cfg.chunk_size_max << "]");

  for (int i = 0; i < 10000; i++) {
    random_size_vec.push_back(dist6(rng));
  }
}

void felix::core::RandomQueueSource::generateElinks() {
  unsigned int range = 1 + cfg.e_link_id_max.value - cfg.e_link_id_min.value;

  if (range < 0) {
    ERROR("Range (e-link-id-max - e-link-id-min) < 0" << range);
    exit(1);
  }

  // check max(noOfElinks) <= range
  if (cfg.e_link_max > range) {
    ERROR("Param e_link_max " << cfg.e_link_max << " > range (e-link-id-max - e-link-id-min)" << range);
    exit(1);
  }

  // generate number of elinks
  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_int_distribution<std::mt19937::result_type> dist6(cfg.e_link_min, cfg.e_link_max);
  unsigned int noOfElinks = dist6(rng);
  INFO("Using " << noOfElinks << " Elinks in range [" << cfg.e_link_id_min << ", " << cfg.e_link_id_max << "]");

  // initialize picking array
  unsigned int elinks_picking[MAX_ELINKS];
  for (unsigned int i=0; i < range; i++) {
    elinks_picking[i] = i;
  }

  std::mt19937 rng_elink;
  rng_elink.seed(std::random_device()());

  for (unsigned int i=0; i < noOfElinks; i++) {
    // generate e-linkIndex and pick it up
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0,range-i-1);
    unsigned int elinkIndex = dist6(rng_elink);
    unsigned int elink = elinks_picking[elinkIndex];
    random_elink_vec.push_back(elink);
    elinks.insert(elink + elinkOffset);

    // replace used index with elinkId at last position
    elinks_picking[elinkIndex] = elinks_picking[range-i-1];
  }
}
