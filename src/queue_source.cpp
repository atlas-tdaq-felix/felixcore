/*
 * queue_source.cpp
 *
 *  Created on: Nov 8, 2018
 *      Author: gleventi
 */

#include "queue_source.hpp"
#include "configuration.hpp"

namespace felix {
namespace core {

std::atomic<unsigned short int>	QueueSource::queueSourceThreadCounter	= ATOMIC_VAR_INIT(0);
std::atomic<bool>				QueueSource::populationHasStarted		= ATOMIC_VAR_INIT(false);

QueueSource::QueueSource(unsigned int index, BlockQueue** buffers,
		BlockQueue** queues, size_t num_queues, unsigned int offset) :
		index(index), trace(tracer::instance()), buffers(buffers), queues(queues),
		num_queues(num_queues), blockSize(BLOCK_SIZE), offset(offset) {
	cfg = FelixConfiguration::instance().values();
	queueSelector = new int[MAX_ELINKS];

	for (unsigned int elink = 0; elink < MAX_ELINKS; elink++)
		queueSelector[elink] = -1;
}

QueueSource::~QueueSource() {
	queueSourceThread.join();
	delete[] queueSelector;
}



unsigned int QueueSource::getQueueIndex(unsigned int elink, unsigned int noOfQueues) {
	unsigned index = ((elink % MAX_ELINKS / 64) % noOfQueues);

	if (!cfg.nostats && !cfg.nohistos) {
		if (queueSelector[elink] < 0) {
			unsigned int port = cfg.port + index
					+ (offset / MAX_ELINKS) * cfg.threads;
			queueSelector[elink] = port;
			nstats.setPortsOfELink(elink + offset, queueSelector[elink]);
		}
	}
	return index;
}

void QueueSource::dispatch(const Block* block) {

	trace.trace_tohost_block_read(block->elink);

	unsigned int queueIndex = getQueueIndex(block->elink, num_queues);
	if (cfg.buffer > 0) {
		Block* b = NULL;
		buffers[queueIndex]->pop(&b);
		memcpy(b, block, blockSize);
		queues[queueIndex]->push(b);
	}

	if (!cfg.nostats) {
		unsigned int device_offset = index * cfg.threads;
		if (!cfg.nohistos) {
			nstats.blocksPerThreadIncr(device_offset + queueIndex);
			nstats.eLinkFromFelixIncr(offset + block->elink);
		}

		nstats.blocksPP();
		nstats.setBufferFree(device_offset + queueIndex,
				buffers[queueIndex]->size());
	}
}

} /* namespace core */
} /* namespace felix */
