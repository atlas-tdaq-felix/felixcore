#pragma once

#include "cmem.hpp"

namespace felix
{
namespace core
{

class CircularDMA
{
public:

    CircularDMA(FlxCard& flxcard, unsigned dmaid, size_t bufsize)
    : flxcard(flxcard), dmaid(dmaid), cmembuf(bufsize)
    {
        auto cfg = FelixConfiguration::instance().values();
        use_interrupts = !cfg.polling;
        poll_time = cfg.poll_time;

        flxcard.dma_stop(dmaid);
        flxcard.dma_to_host(dmaid, cmembuf.paddr(), bufsize, FLX_DMA_WRAPAROUND);

        pstart = cmembuf.paddr();
        pend = pstart + bufsize;
        pc_ptr = flxcard.dma_get_read_ptr(dmaid);
    }


    ~CircularDMA()
    {
        flxcard.dma_stop(dmaid);
    }


    /**
     This function indicates how many data have been written to the circular
     buffer. This data may not be contiguous as it may have "wrapped around" and
     thus it may not be assumed that this many bytes can be consumed at once.
    */
    size_t bytes_available()
    {
        u_long dma_ptr = flxcard.dma_get_current_address(dmaid);

        // No need to call dma_cmp_even_bits() every time (Henk B, 2 Oct 2020)
        /*
        if(flxcard.dma_cmp_even_bits(dmaid)) {
            return dma_ptr - pc_ptr;
        }
        else {
            return cmembuf.size() + dma_ptr - pc_ptr;
        }
        */
        u_long bytes;
        if( pc_ptr < dma_ptr ) {
          bytes = dma_ptr - pc_ptr;
        }
        else if( pc_ptr > dma_ptr ) {
          bytes = cmembuf.size() + dma_ptr - pc_ptr;
        }
        // pc_ptr == dma_ptr
        else if( flxcard.dma_cmp_even_bits(dmaid) ) {
          bytes = 0; // Empty
        }
        else {
          bytes = cmembuf.size(); // Full
        }
        return bytes;
    }


    /**
     This function returns how many bytes can be safely consumed. This
     amount of bytes is guaranteed to be contiguous.

     Use this function as opposed to bytes_available() as an argument to
     consume();
    */
    size_t bytes_available_to_read()
    {
        return std::min(bytes_available(), pend - pc_ptr);
    }


    size_t unused_bytes()
    {
        return cmembuf.size() - bytes_available();
    }


    void* ptr()
    {
        return (void*)(cmembuf.vaddr() + (pc_ptr-pstart));
    }


    void consume(size_t bytes)
    {
        pc_ptr += bytes;
        if(pc_ptr == pend)
            pc_ptr = pstart;

        flxcard.dma_set_ptr(dmaid, pc_ptr);
    }


    bool wait(size_t bytes)
    {
        while (bytes_available() < bytes)
        {
            if (use_interrupts) {
#if REGMAP_VERSION < 0x500
                flxcard.irq_wait(IRQ_DATA_AVAILABLE);
#else
                // An interrupt per ToHost DMA controller
                flxcard.irq_wait(IRQ_DATA_AVAILABLE + dmaid);
#endif // REGMAP_VERSION
            } else {
                usleep(poll_time);
            }
        }
        return true; // TODO return false after timeout
    }


    size_t total_buf_size() const
    {
        return cmembuf.size();
    }

private:
    FlxCard& flxcard;
    unsigned dmaid;
    CMEMBuffer cmembuf;

    bool use_interrupts;
    unsigned poll_time;

    u_long pstart, pend, pc_ptr;

};

}
}
