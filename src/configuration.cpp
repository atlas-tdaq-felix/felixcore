#include <memory>
#include <iostream>
#include <fstream>
#include <regex>

#include "docopt/docopt.h"
#include "yaml-cpp/yaml.h"

#include "felixtag.h"

#include "configuration.hpp"
#include "felixcore.hpp"

using namespace felix::base;
using namespace nlohmann;

// NOTE: repeatable options MUST be listed.
static const std::string USAGE =
R"(felixcore - FELIX Core Application

Usage: felixcore [(--verbose | --quiet) --device N... options]

  Run with card: 'felixcore'
  Run with file: 'felixcore -f <file> --notoflx --nobusy'
  Run with data generator: 'felixcore -g --notoflx --nobusy'

All Options:

General Options:
  -t, --threads N                       Run with the specified number of threads [default: 1]
  -p, --port N                          Receive daq data starting from port [default: 12350]
  -r, --recv-port N                     Receive slow control data starting from port [default: 12340]
  -P, --busy-port N                     Receive busy control signals on this port [default: 12330]
  -w, --web-port N                      Port for webserver [default: 8080]
  -b, --buffer N                        Free Buffer size in kBlocks per thread [default: 2000]
  -g, --data-generator                  Use internal data generator
  -l, --logfile <file>                  Write logs to the given filename
  --ttc-generator N                     Use internal ttc generator and elink #, default is off [default: -1]
  --felix-id <id>                       Elink offset for this FelixCore [default: 0]
  --data-interface <iface>              Interface to publish data [default: eth0]
  --monitoring-interface <iface>        Interface to publish monitoring information [default: eth0]
  --noweb                               Disable web server

Netio Options:
  -B, --netio-backend (posix|fi-verbs)  Use the given NetIO backend [default: posix]

Commandline Options:
  -h, --help                            Display this help
  -V, --version                         Display version
  -v, --verbose                         Switch logging on, level = debug
  -q, --quiet                           Switch logging off, level = warning
  -c, --config <file>                   Load configuration from the specified YAML file
  --config-write <file>                 Store configuration in the specified YAML file
  -f, --file <file>                     Use the given filename as input

Card Options:
  -d, --device N...                     Use only listed devices to look for cards
  -m, --memory N                        Allocate CMEM memory in Gbyte [default: 1]
  -M, --megabyte N                      Allocate CMEM memory in Mbyte, overrides -m [default: 0]
  --polling                             Use polling
  --poll-time T                         Poll time in micro-seconds [default: 50000]
  --dma D                               Use toHost dma channel [default: 0]
  --toflx-dma D                         Use toFlx dma channel [default: 1]
  --toflx-cdma                          Use circular DMA toFlx dma channel
  --elinks <elinkrange>                 E-link numbers/number range, comma separated for instance
  --streams <elinkrange>                List of E-Links that carry streams
  --notoflx                             Do not start the To-FLX thread(s)
  --nobusy                              Do not start the Busy thread(s)

Data Generator Options:
  --fixed-chunks                        Use fixed chunk size
  --chunk-size-fixed N                  Fixed size of chunks [default: 1018]
  --rate-control-bool                   Use rate control
  --chunk-size-min N                    Minimum size of chunks [default: 128]
  --chunk-size-max N                    Maximum size of chunks [default: 4096]
  --e-link-min N                        Minimum number of elinks [default: 50]
  --e-link-max N                        Maximum number of elinks [default: 150]
  --e-link-id-min <id>                  Minimum ID of elinks [default: 1]
  --e-link-id-max <id>                  Maximum ID of elinks (<2048) [default: 255]
  --period T                            Period in ms [default: 100]
  --max-overshoot T                     Max overshoot in ms [default: 10]
  --duration T                          Duration in s (0 = run forever) [default: 0]

Debug Options:
  --nostats                             Disable statistics
  --display-stats                       Print out statistics
  --nohistos                            Disable histograms
  --pmstats                             Enable poor man's statistics, implies --nostats
  --trace                               Enable tracing
)";


void felix::core::FelixConfiguration::set_members(std::map<std::string, docopt::value>& args) {
    // General
    m_values.threads = args["--threads"].asLong();
    m_values.port = args["--port"].asLong();
    m_values.port_recv = args["--recv-port"].asLong();
    m_values.port_busy = args["--busy-port"].asLong();
    m_values.netio_backend = args["--netio-backend"].asString();
    m_values.logfile = args["--logfile"] ? args["--logfile"].asString() : "";
    m_values.buffer = args["--buffer"].asLong();
    m_values.data_generator = args["--data-generator"].asBool();
    m_values.ttc_generator = args["--ttc-generator"].asLong();
    m_values.felix_id = args["--felix-id"].asLong(); // <hex_uint>
    m_values.data_interface = args["--data-interface"].asString();
    m_values.monit_interface = args["--monitoring-interface"].asString();
    m_values.noweb = args["--noweb"].asBool();
    m_values.web_port = args["--web-port"].asLong();

    // Command
    m_values.verbose = args["--verbose"].asBool();
    m_values.quiet = args["--quiet"].asBool();
    m_values.config_file = args["--config"] ? args["--config"].asString() : "";
    m_values.file = args["--file"] ? args["--file"].asString() : "";

    // Card
    m_values.devices = args["--device"].asStringList();
    m_values.memory = args["--memory"].asLong();
    m_values.megabyte = args["--megabyte"].asLong();
    m_values.polling = args["--polling"].asBool();
    m_values.poll_time = args["--poll-time"].asLong();
    m_values.dma = args["--dma"].asLong();
    m_values.toflx_dma = args["--toflx-dma"].asLong();
    m_values.toflx_cdma = args["--toflx-cdma"].asBool();
    m_values.elink_numbers = args["--elinks"] ? args["--elinks"].asString() : "";
    m_values.notoflx = args["--notoflx"].asBool();
    m_values.nobusy = args["--nobusy"].asBool();
    m_values.streams_str = args["--streams"] ? args["--streams"].asString() : "";
    felix::base::parse_range(m_values.streamEnabledElinks, m_values.streams_str);

    // Data Generator
    m_values.fixed_chunks = args["--fixed-chunks"].asBool();
    m_values.chunk_size_fixed = args["--chunk-size-fixed"].asLong();
    m_values.rate_control_bool = args["--rate-control-bool"].asBool();
    m_values.chunk_size_min = args["--chunk-size-min"].asLong();
    m_values.chunk_size_max = args["--chunk-size-max"].asLong();
    m_values.e_link_min = args["--e-link-min"].asLong();
    m_values.e_link_max = args["--e-link-max"].asLong();
    m_values.e_link_id_min = args["--e-link-id-min"].asLong(); // <hex_uint>
    m_values.e_link_id_max = args["--e-link-id-max"].asLong(); // <hex_uint>
    m_values.period = args["--period"].asLong();
    m_values.max_overshoot = args["--max-overshoot"].asLong();
    m_values.duration = args["--duration"].asLong();

    // Debug
    m_values.display_stats = args["--display-stats"].asBool();
    m_values.nohistos = args["--nohistos"].asBool();
    m_values.pmstats = args["--pmstats"].asBool();
    m_values.nostats = m_values.pmstats ? true : args["--nostats"].asBool();
    m_values.trace = args["--trace"].asBool();

    // Config file only
    // FIXME, why not just also allow this from the commandline, then also no need to check for null
    m_values.debug_dump_chunks = args["--debug.dump-chunks"] ? args["--debug.dump-chunks"].asBool() : false;
    // FIXME Should we not just use file here rather than this funny override
    m_values.file = args["--debug.file-input"] ? args["--debug.file-input"].asString() : m_values.file;
}

void felix::core::FelixConfiguration::parse_args(int argc, char** argv) {
    std::vector<std::string> const& cmd = { argv + 1, argv + argc };

    // Get command line arguments (full list)
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
                                                    cmd,
                                                    true,      // show help if requested
                                                    FELIX_TAG);  // version string

    // Get defaults (full list, null or false where not set)
    std::map<std::string, docopt::value> defaults = docopt::docopt(USAGE,
                                                    { },
                                                    false,      // show help if requested
                                                    FELIX_TAG);  // version string

    // for(auto const& c : cmd) {
    //     std::cout << "cmd: " << c << std::endl;
    // }
    // for(auto const& arg : defaults) {
    //     std::cout << "def" << arg.first << ": " << arg.second << std::endl;
    // }
    // for(auto const& arg : args) {
    //   std::cout << "act" << arg.first << ": " << arg.second << std::endl;
    // }
    // exit(0);


    // Read Config file if provided
    if (args["--config"]) {
        std::ifstream fin(args["--config"].asString());
        if(!fin.good()) {
            ERROR(APPLICATION_NAME << ": error: could not open configuration file '"
                                   << args["--config"].asString() << "'");
            exit(1);
        }
        std::map<std::string, docopt::value> config = load_file(fin);
        fin.close();

        // build map of long to short options
        std::map<std::string, std::string> short_options;
        std::string str = USAGE;
        std::smatch m;
        std::regex r(R"(\n\s*(-\w),?\s*(--\w+))");
        while(std::regex_search(str, m, r)) {
            short_options[m[2]] = m[1];
            str = m.suffix();
        }

        // override each value if needed
        for(auto const& arg : config) {
            std::string key = arg.first;
            const docopt::value& value = arg.second;
            // std::cout << "con" << key << ": " << value << std::endl;
            // long: use config value if command line option NOT set (equals to default)
            if (std::find(cmd.begin(), cmd.end(), key) != cmd.end()) {
                continue;
            }

            // short: if exists, find if short options was used...
            std::map<std::string, std::string>::iterator it = short_options.find(key);
            if (it != short_options.end()) {
                if (std::find(cmd.begin(), cmd.end(), short_options[key]) != cmd.end()) {
                    continue;
                }
            }

            args[key] = value;
        }
    }

    // Write Config file if requested (and exit)
    // Only values different from defaults are written
    if (args["--write-config"]) {
      std::ofstream fout(args["--write-config"].asString());
      if(!fout.good()) {
          ERROR(APPLICATION_NAME << ": error: could not write configuration file '"
                                 << args["--config-write"].asString() << "'");
          exit(1);
      }
      INFO(APPLICATION_NAME << ": writing config file: " << args["--config-write"].asString());
      store_file(fout, args, defaults);
      fout.close();
      exit(1);
    }

    set_members(args);

    // TODO remove this line when configuration is working
    //m_values.streamEnabledElinks[0] = false;

}


json felix::core::FelixConfiguration::toJson(std::vector<std::string> path, std::vector<std::string> query) {
    json o;

    std::string name = path[1];
    std::string type = path[2];

    if (name == "general") {
        o["data"] = json::array({
            json::array({"logfile", values().logfile, ""}),
            json::array({"debug-dump-chunks", values().debug_dump_chunks, ""}),
            json::array({"threads", values().threads, ""}),
            json::array({"port", values().port, ""}),
            json::array({"port-recv", values().port_recv, ""}),
            json::array({"port-busy", values().port_busy, ""}),
            json::array({"netio-backend", values().netio_backend, ""}),
            json::array({"data-generator", values().data_generator, ""}),
            json::array({"ttc-generator", values().ttc_generator, ""}),
            json::array({"buffer", values().buffer, "kBlocks"}),
            json::array({"felix-id", hex(values().felix_id.value), ""}),
            json::array({"data-interface", values().data_interface, ""}),
            json::array({"monitoring-interface", values().monit_interface, ""}),
            json::array({"noweb", values().noweb, ""}),
            json::array({"web-port", values().web_port, ""})
        });
    } else if (name == "cmd") {
        o["data"] = json::array({
            json::array({"file", values().file, ""}),
            json::array({"config-file", values().config_file, ""}),
        });
    } else if (name == "card") {
        o["data"] = json::array({
            json::array({"memory", (double)(values().memory)/1000, "GBlocks"}),
            json::array({"polling", values().polling, ""}),
            json::array({"poll-time", values().poll_time, "us"}),
            json::array({"dma", values().dma, ""}),
            json::array({"toflx-dma", values().toflx_dma, ""}),
            json::array({"elinks", values().elink_numbers, ""}),
            json::array({"notoflx", values().notoflx, ""}),
            json::array({"nobusy", values().nobusy, ""}),
            // json::jumap({"streamEnabledElinks", values().streamEnabledElinks, ""}), TODO add it
        });
    } else if (name == "data_generator") {
        o["data"] = json::array({
            json::array({"fixed-chunks", values().fixed_chunks, ""}),
            json::array({"rate-control-bool", values().rate_control_bool, ""}),
            json::array({"period", values().period, "s"}),
            json::array({"max-overshoot", values().max_overshoot, "ms"}),
            json::array({"duration", values().duration, "ms"}),
            json::array({"e-link-min", values().e_link_min, ""}),
            json::array({"e-link-max", values().e_link_max, ""}),
            json::array({"e-link-id-min", hex(values().e_link_id_min.value), ""}),
            json::array({"e-link-id-max", hex(values().e_link_id_max.value), ""}),
            json::array({"chunk-size-fixed", values().chunk_size_fixed, "byte"}),
            json::array({"chunk-size-min", values().chunk_size_min, "byte"}),
            json::array({"chunk-size-max", values().chunk_size_max, "byte"}),
        });
    } else if (name == "debug") {
        o["data"] = json::array({
            json::array({"nostats", values().nostats, ""}),
            json::array({"display-stats", values().display_stats, ""}),
        });
    }

    return o;
}

// void felix::core::validate(boost::any& v, std::vector<std::string> const& values, hex_uint* /* target_type */, int) {
//
//     uint d;
//     try {
//         if (starts_with(s, "0x")) {
//             d = std::stoul(s.substr(2), nullptr, 16);
//         } else {
//             d = std::stoul(s, nullptr, 10);
//         }
//
//         v = boost::any(hex_uint(d));
//     } catch(std::invalid_argument) {
//         throw validation_error(validation_error::invalid_option_value);
//     }
// }


std::map<std::string, docopt::value> felix::core::FelixConfiguration::load_file(std::istream& in) {
    YAML::Node yaml = YAML::Load(in);
    std::map<std::string, docopt::value> config;
    for(auto const& node : yaml) {
        std::string key, value;
        key = node.first.as<std::string>();
        value = node.second.as<std::string>();
        // std::cout << "Key: " << key << ", value: " << value << std::endl;
        config["--" + key] = value == "true" ? docopt::value(true) : value == "false" ? docopt::value(false) : docopt::value(value);
    }
    return config;
}

void felix::core::FelixConfiguration::store_file(std::ostream& out, std::map<std::string, docopt::value>& args, std::map<std::string, docopt::value>& defaults) {
    YAML::Emitter yaml(out);
    yaml << YAML::BeginMap;
    for(auto const& arg : args) {
        std::string key = arg.first;
        const docopt::value& value = arg.second;

        // Skip some options
        if (key == "--config" || key == "--write-config") {
            continue;
        }
        // Skip values equal to defaults
        if (value == defaults[key]) {
            continue;
        }
        yaml << YAML::Key << key.substr(2) << YAML::Value;
        if (!value) {
            yaml << "~";
        } else if (value.isBool()) {
            yaml << value.asBool();
        } else if (value.isLong()) {
            yaml << value.asLong();
        } else {
            yaml << value.asString();
        }
    }
    yaml << YAML::EndMap;
}


std::unique_ptr<felix::core::FelixConfiguration> felix::core::FelixConfiguration::m_instance;

const felix::core::FelixConfiguration::Values& felix::core::FelixConfiguration::values() {
    return m_values;
}


felix::core::FelixConfiguration& felix::core::FelixConfiguration::instance() {
//    if(!m_instance)
//        m_instance = std::unique_ptr<FelixConfiguration>(new FelixConfiguration());
//    return *m_instance;
	static FelixConfiguration instance;
	return instance;
}


void felix::core::FelixConfiguration::clear() {
    m_values = Values();
}
