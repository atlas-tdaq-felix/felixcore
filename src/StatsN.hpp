/*
 * StatsN.hpp
 *
 *  Created on: Sep 14, 2018
 *      Author: gleventi
 */

#ifndef FELIXCORE_SRC_STATSN_HPP_
#define FELIXCORE_SRC_STATSN_HPP_

#include <atomic>
#include "definitions.hpp"						// Max and min of atomics
#include "StatCountersStruct.hpp"				// All the atomic vairables


namespace felix {
namespace core {

class StatsN {
public:
	StatsN(StatisticsCategory statisticsCategory);
protected:
	static unsigned long int		maxeLink;
	static unsigned long int		maxThreadIndex;

private:
	StatCountersStruct				sCounters;

	/*________________________________________________________________________________________________________________*/
	/* Functions for Atomics */
	/*________________________________________________________________________________________________________________*/

public:

	inline void chunksPP()				{	++sCounters.chunks;				}
	inline void shortchunksPP()			{	++sCounters.shortchunks;		}
	inline void blocksPP()				{	++sCounters.blocks;				}
	inline void faultyChunksPP()		{	++sCounters.faultyChunks;		}
	inline void sizeFaultyChunksPP()	{	++sCounters.sizeFaultyChunks;	}
	inline void truncatedChunksPP()		{	++sCounters.truncatedChunks;	}
	inline void crcerrChunksPP()		{	++sCounters.crcerrChunks;	}
	inline void malformedSubchunksPP()	{	++sCounters.malformedSubchunks;	}
	inline void malformedChunksPP()		{	++sCounters.malformedChunks;	}
	inline void malformedBlocksPP()		{	++sCounters.malformedBlocks;	}


	#define DEFCOUNTINCR(FUNCNAME, MINVAL, MAXVAL, COUNTERNAME) \
	inline void FUNCNAME (uint64_t xGraphValue) { \
		if (xGraphValue < MINVAL) \
			++sCounters.COUNTERNAME->underFlowCounter; \
		else if (xGraphValue > MAXVAL) \
			++sCounters.COUNTERNAME->overFlowCounter; \
		else \
			++(sCounters.COUNTERNAME->counter[xGraphValue]); \
		++sCounters.COUNTERNAME->totalCounter; \
	}

	DEFCOUNTINCR(eLinkFromFelixIncr,		ELINK_MIN,					maxeLink,					eLinkFromFelix);
	DEFCOUNTINCR(eLinkToFelixIncr,			ELINK_MIN,					maxeLink,					eLinkToFelix);
	DEFCOUNTINCR(chunkLengthIncr,			CHUNK_LENGTH_MIN,			CHUNK_LENGTH_MAX,			chunkLength);
	DEFCOUNTINCR(subchunkLengthIncr,		SUBCHUNK_LENGTH_MIN,		SUBCHUNK_LENGTH_MAX,		subchunkLength);
	DEFCOUNTINCR(subchunkTypesIncr,			SUBCHUNK_TYPE_ENUM_MIN,		SUBCHUNK_TYPE_ENUM_MAX,		subchunkType);
	DEFCOUNTINCR(blocksPerTransferInrc,		BLOCKS_PER_TRANSFER_MIN,	BLOCKS_PER_TRANSFER_MAX,	blocksPerTransfer);
	DEFCOUNTINCR(blocksPerThreadIncr,		THREADS_MIN,				maxThreadIndex,				blocksPerThread);


	inline void seqNoIncr(unsigned int expected, unsigned int received) {
//		// TODO fix this (was not working already)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		++(sCounters.seqnoC[expected]);
//		++(sCounters.seqnoC[received + 50]);
//		++(sCounters.seqnoC[((expected + 32 - received) % 32) + 100]);
//		sCounters.seqnoTotalC += 3;
	}

	inline void setBlocksInQueue(unsigned int queueIndex, unsigned long int blocksInQueue) {
		if (blocksInQueue < BLOCKS_IN_A_QUEUE_MIN - 1)
			++sCounters.blocksInQueue->underFlowCounter;
		else if (blocksInQueue > BLOCKS_IN_A_QUEUE_MAX)
			++sCounters.blocksInQueue->overFlowCounter;
		else
			if (blocksInQueue > 0)
				++(sCounters.blocksInQueue->counter[blocksInQueue]);
		if (blocksInQueue > 0 && blocksInQueue > sCounters.queueInstancesCurrentSizeC[queueIndex].load())
			sCounters.queueInstancesCurrentSizeC[queueIndex] = blocksInQueue;
		++sCounters.blocksInQueue->totalCounter;
	}

	inline void setPortsOfELink(unsigned int elink, unsigned int port) {
		if (port < PORTS_MIN)
			++sCounters.elinkPorts->underFlowCounter;
		else if (port > PORTS_MAX)
			++sCounters.elinkPorts->overFlowCounter;
		else
			sCounters.elinkPorts->counter[elink] = port;
		++sCounters.elinkPorts->totalCounter;
	}

	inline void setBufferFree(unsigned int buffersQueueIndex, unsigned long freeBlocks) {
		if (buffersQueueIndex >= 0 && buffersQueueIndex <= maxThreadIndex)
			sCounters.freeBufferC[buffersQueueIndex] = freeBlocks;
	}

	inline void setBlocksFree(unsigned int blocksCardIndex, unsigned long freeBlocks) {
		if (blocksCardIndex >= DEVICES_MIN && blocksCardIndex <= DEVICES_MAX)
			sCounters.freeBlocksC[blocksCardIndex] = freeBlocks;
	}

	inline void fill_message_size(unsigned int queueIndex, unsigned int messageSize) {
		if (messageSize < MESSAGE_SIZE_MIN)
			++sCounters.messageSizeInQueue->underFlowCounter;
		else if (messageSize > MESSAGE_SIZE_MAX)
			++sCounters.messageSizeInQueue->overFlowCounter;
		else
			sCounters.messageSizeInQueue->counter[queueIndex] += messageSize;
		++sCounters.messageSizeInQueue->totalCounter;
	}

};

} /* namespace core */
} /* namespace felix */

#endif /* FELIXCORE_SRC_STATSN_HPP_ */
