#include <atomic>
#include <cstdint>
#include <ios>
#include <iostream>
#include <limits>
#include <numa.h>
#include <string>
#include <thread>
#include <unistd.h>
#include <utility>
#include <vector>
#include <set>
#include <condition_variable>
#include <chrono>

#include "packetformat/block_reader.hpp"

#include "felixbase/logging.hpp"

#include "felixbus/bus.hpp"
#include "felixbus/felixtable.hpp"
#include "felixbus/elinktable.hpp"

#include "pipeline.hpp"
#include "card_queue_source.hpp"
#include "random_queue_source.hpp"
#include "file_queue_source.hpp"
#include "ttc_generator_queue_source.hpp"

#include "queue_block_source.hpp"
#include "configuration.hpp"
#include "common.hpp"
#include "felixcore.hpp"
#include "toflx.hpp"
#include "busythread.hpp"
#include "web_server.hpp"
#include "trace.hpp"

typedef std::uint64_t timestamp_t;

static const timestamp_t ns = 1;
static const timestamp_t us = 1000 * ns;
static const timestamp_t ms = 1000 * us;
static const timestamp_t s  = 1000 * ms;

using namespace std;
using namespace felix::base;
using namespace felix::bus;
using namespace felix::core;

/*
 * Returns the full path to the currently running executable,
 * or an empty string in case of failure.
 */
std::string getExecutablePath() {
	char exePath[PATH_MAX];
	ssize_t len = ::readlink("/proc/self/exe", exePath, sizeof(exePath));
	if (len == -1 || len == sizeof(exePath))
		len = 0;
	exePath[len] = '\0';
	return std::string(exePath);
}

static void run(std::atomic_bool& running, std::atomic_int& not_ready,
		FelixConfiguration::Values cfg, BlockQueue* buffer, BlockQueue* queue,
		unsigned int index, std::condition_variable& cv,
		unsigned source_index) {
	running.store(true);
	sleep(2);

  pinThreadToCPUThread((index % getNoOfWorkerThreads()) + getNoOfSourceThreads());
	Pipeline pipeline;
	std::unique_ptr<BlockSource> block_queue;
	std::string filename = cfg.file;

	INFO("Input is coming from queue " << index << " (using CPU thread " << (index % getNoOfWorkerThreads()) + getNoOfSourceThreads() << ")");
	StatsN workersStats { pipelineStats };
	block_queue = std::unique_ptr<BlockSource>(
			new QueueBlockSource(buffer, queue, index, workersStats));

	try {
		not_ready--;
		while (running)
			pipeline.process(block_queue->pop(),
					BlockMetadata(index, source_index, &workersStats));
		cv.notify_all();
	} catch (std::exception& e) {
		CRITICAL("Uncaught exception in processing thread: " << e.what());
		throw;
	}
}

int main(int argc, char** argv)
{
  set_log_level(level::debug);

  FelixConfiguration& configuration = FelixConfiguration::instance();
  configuration.clear();
  configuration.parse_args(argc, argv);
  FelixConfiguration::Values cfg = configuration.values();
  spdlog::level::level_enum log_level = cfg.verbose ? level::debug : cfg.quiet ? level::warn : level::info;
  INFO("Setting log level to " << log_level);
  set_log_level(log_level);

  if(cfg.logfile != "")
    Logger::init_file(cfg.logfile);

  // Find Web Root
  boost::filesystem::path executable = getExecutablePath();
  INFO("Executable path: " << executable);
  // development path
  boost::filesystem::path web_root = executable.parent_path() / "share/felixcore/web";
  INFO("Looking for Web Root: " << web_root);
  // for the distribution
  if(!boost::filesystem::exists(web_root)) {
    web_root = executable.parent_path().parent_path() / "share/felixcore/web";
    INFO("Looking for Web Root: " << web_root);
  }

  // for the real installation
  if(!boost::filesystem::exists(web_root)) {
    web_root = "/usr/local/share/felixcore/web";
    INFO("Looking for Web Root: " << web_root);
  }

  // final verdict on webroot
  if (boost::filesystem::exists(web_root)) {
    INFO("Web Root found at: " << web_root);
  } else {
    ERROR("Web Root could NOT be found in above locations");
  }

  // NOTE: one of the two is set.
  bool useFile = cfg.file != "";
  bool useDataGenerator = !useFile && cfg.data_generator == true;
  bool useCard = false;
  bool useTTCGenerator = cfg.ttc_generator >= 0;

  INFO("Starting felixcore (ctrl\\ to quit)");
  std::atomic_int not_ready;
  not_ready.store(0);

  // set default number of threads. we may again look at some point to number of cores...
  unsigned int threads = cfg.threads;
  INFO("Number of threads: " << threads);

  std::pair<std::string, std::string> data_address = getIpAddress(cfg.data_interface);
  if (data_address.second == "") {
    ERROR("Cannot find IP address for interface: " << data_address.first);
    exit(1);
  }
  INFO("Using " << data_address.first << " IP address: " << data_address.second << " for FelixBus and NetIO");

  bool monitoringIsEnabled = true;
  std::pair<std::string, std::string> monit_address = getIpAddress(cfg.monit_interface);
  if (monit_address.second == "") {
    WARNING("Cannot find IP address for the Monitoring interface: " << monit_address.first);
    monitoringIsEnabled = false;
  } else {
    INFO("Using " << monit_address.first <<
         " IP address: " << monit_address.second <<
         " for Monitoring");
  }

  INFO("Starting Bus");
  Bus dbus;
  dbus.setInterface(data_address.first);
  // dbus.setVerbose(true);
  // dbus.setZyreVerbose(true);

  Bus mbus;
  if (monitoringIsEnabled) {
    mbus.setInterface(monit_address.first);
    // mbus.setVerbose(true);
    // mbus.setZyreVerbose(true);
  }



  // Start Trace framework
  tracer& t = tracer::instance();
  if(cfg.trace)
  {
    INFO("Starting Tracer");
    t.start("trace.csv");
  }

  unsigned nb_used_devices = 0;
  std::vector<unsigned int> used_devices;
  std::vector<bool> busy_devices;

  if (!useDataGenerator) {
    // Probing all cards to establish real number of devices to be used
    unsigned int const total_no_of_devices = CardQueueSource::getNumberOfDevices();
    INFO("Found total of " << total_no_of_devices << " Devices");

    if ((threads + 1) * (cfg.devices.size() > 0 ? cfg.devices.size() : total_no_of_devices) > getAvailableHardwareCores())
      WARNING("You are oversubscribing the CPU, try lowering the threads (-t option)");

    bool busy = true;
    std::set<std::string> devices(cfg.devices.begin(), cfg.devices.end());
    // check all devices if no specific one
    if (devices.empty()) {
      for(unsigned int device_no=0; device_no < total_no_of_devices; device_no++) {
        devices.insert(std::to_string(device_no));
      }
    }

    for(unsigned device_no=0; device_no < total_no_of_devices; device_no++) {
      int cardModel = CardQueueSource::probeDevice(device_no);
      switch (cardModel) {
        case 712:
        case 711:
          if (devices.count(std::to_string(device_no)) > 0) {
            INFO("  Found device " << device_no << " of model " << cardModel << (busy ? " Busy" : ""));
            used_devices.push_back(device_no);
            busy_devices.push_back(busy);
          } else {
            INFO("  Skip  device " << device_no << " of model " << cardModel << (busy ? " Busy" : ""));
          }
          busy  = !busy;
          break;
        case 709:
          if (devices.count(std::to_string(device_no)) > 0) {
            INFO("  Found device " << device_no << " of model " << cardModel << " Busy");
            used_devices.push_back(device_no);
            busy_devices.push_back(true);
          } else {
            INFO("  Skip  device " << device_no << " of model " << cardModel << " Busy");
          }
          break;
        default:
          INFO("  Skip  Device: " << device_no << " of model " << cardModel);
          continue;
      }
    }
    nb_used_devices = used_devices.size();
    INFO("  Using " << nb_used_devices << " Devices");
  } else {
    // data generator
    nb_used_devices = 1;
    INFO("  Using internal data generator (no devices)");
  }

  // Starting web server
  WebServer webServer(web_root);
  thread webServerThread;
  if (!cfg.noweb) {
    // FIXME configure web dir, make sure it works in distribution, depends on executable path
    INFO("Starting Web Server on port: " << cfg.web_port);
    webServer.setup();
    webServerThread = thread([&webServer]() {
      //Start server
      webServer.start();
    });
    set_thread_name(webServerThread, "webserver");
  }

  // starting felixcore threads
  INFO("Starting Worker Threads from port " << cfg.port);
  BlockQueueVector queues;
  BlockQueueVector buffer;
  std::vector<std::thread> processing_threads;
  std::vector<std::string> uuids;
  std::condition_variable cv;

  // Start run threads
  INFO("Using RWQ");

  unsigned kBlocks = cfg.buffer;
  INFO("Starting Queue Threads " << threads << " with buffer " << kBlocks << " kBlocks per thread...");
  FelixTable felixTable;
  std::atomic_bool process_threads_running;

  for(unsigned index=0; index < threads*nb_used_devices; index++) {
    int const capacity = 100000;
    BlockQueue *b = new BlockQueue(kBlocks * Constant::Kilo);
    BlockQueue *q = new BlockQueue(capacity);

    for (unsigned i=0; i < kBlocks * Constant::Kilo; i++) {
      b->push(new Block());
    }

    buffer.push_back(b);
    queues.push_back(q);

    int port = cfg.port + index;
    std::stringstream stream;
    stream << "tcp://" << data_address.second << ":" << port;
    uuids.push_back(felixTable.addFelix(stream.str()));

    not_ready++;
    processing_threads.emplace_back(run, std::ref(process_threads_running), std::ref(not_ready), cfg, buffer[index], queues[index], index, std::ref(cv), index/threads);

		INFO("Starting worker thread " << index << " for device " << used_devices[index/threads] << " on port " << port);

    char tname[16];
    snprintf(tname, 16, "wkr-proc-%02d", index);
    set_thread_name(processing_threads.back(), tname);
  }

  FelixTable monitTable;
  {
    std::stringstream stream;
    stream << "tcp://" << monit_address.second << ":" << cfg.web_port;
    monitTable.addFelix(stream.str(), true);
  }

  if (kBlocks <= 0) {
    WARNING("No Buffer, discarding blocks after check");
  }

  // FIXME use one of many (switch)
  std::vector<QueueSource*> sources;
//  std::atomic_bool source_threads_running;
  std::vector<ToFLXThread*> toflx_threads;
  std::vector<BusyThread*> busy_threads;


  if (useFile) {
    // start file thread
    std::string filename = cfg.file;
    INFO("Setting up Source Threads from File: " << filename);

    std::ifstream in(filename, std::ios::binary);
     if(!in.good()) {
      ERROR("Could not open " << filename << " for reading");
      exit(EXIT_FAILURE);
    }
    sources.emplace_back(new FileQueueSource(buffer.data(), queues.data(), buffer.size(), in));
    sources.emplace_back(new RandomQueueSource(buffer.data(), queues.data(), buffer.size()));
    if (useTTCGenerator) {
      // start the TTC generator thread
      INFO("Setting up Source Threads from TTC Generator");
      sources.emplace_back(new TTCGeneratorQueueSource(buffer.data(), queues.data(), buffer.size()));
    }
  }
  else if (useDataGenerator) {
    if (useTTCGenerator) {
      // start the TTC generator thread
      INFO("Setting up Source Threads from TTC Generator");
      sources.emplace_back(new TTCGeneratorQueueSource(buffer.data(), queues.data(), buffer.size()));
    }

    // start the data generator thread
    INFO("Setting up Source Threads from Standard Data generator");
    sources.emplace_back(new RandomQueueSource(buffer.data(), queues.data(), buffer.size()));

  } else if (useTTCGenerator) {

    // start the TTC generator thread
    INFO("Setting up Source Threads from TTC Generator");
    sources.emplace_back(new TTCGeneratorQueueSource(buffer.data(), queues.data(), buffer.size()));
  } else {
    useCard = true;

    // start the card(s) thread(s)
    for (unsigned int index = 0; index < used_devices.size(); index++) {
      unsigned int device_no = used_devices[index];
      bool const busy = busy_devices[index];
      INFO("Setting up for device " << device_no << (busy ?  " Busy" :  ""));

      unsigned n = /* j*threads*cfg.num_sources + */ index*threads;
      CardQueueSource *cqs = new CardQueueSource(index, device_no, buffer.data()+n, queues.data()+n, threads);
      sources.emplace_back( cqs );
      if (!cfg.notoflx) {
        int toflx_dmaid = cqs->nrOfDescriptors() - 1;
        int toflx_format = cqs->fromHostFormat();
        toflx_threads.emplace_back(new ToFLXThread(device_no, cfg.port_recv + device_no, toflx_dmaid, toflx_format));
      }
      if (!cfg.nobusy && busy) {
        busy_threads.emplace_back(new BusyThread(device_no, cfg.port_busy + device_no));
      }
    }
  }

  INFO("Publishing elinks and ports");
  INFO("LinkId: " << hex(getLinkId(cfg.felix_id.value, 0)));
  // FIXME should the toFLX threads be published here
  std::stringstream stream;
  stream << "Elinks [";
  ElinkTable elinkTable;
  for(unsigned i=0; i<sources.size(); i++) {
    stream << (i > 0 ? ", ": "") << "[";
    std::set<unsigned int> elinks = sources[i]->getElinks();
    std::set<unsigned int>::iterator it;

    for (it = elinks.begin(); it != elinks.end(); it++) {
      unsigned int elink = *it;
      stream << (it != elinks.begin() ? ", " : "") << hex(elink);
      unsigned linkId = getLinkId(cfg.felix_id.value, elink);

      unsigned int queueIndex = i*threads + sources[i]->getQueueIndex(elink % MAX_ELINKS, threads);
      elinkTable.addElink(linkId, uuids[queueIndex]);
    }
    stream << "]";
  }
  stream << "]";
  INFO(stream.str());

	stream.str("");
	stream << "Elinks enabled for streams: [ ";
	for(auto it = cfg.streamEnabledElinks.begin(); it != cfg.streamEnabledElinks.end(); it++)
	{
		stream << (it != cfg.streamEnabledElinks.begin() ? ", " : "") << hex(*it);
	}
	stream << " ]";
	INFO(stream.str());

  // publish tables on bus
  dbus.publish("FELIX", felixTable);
  dbus.publish("ELINKS", elinkTable);
  if (monitoringIsEnabled) {
    mbus.publish("MONITORING", monitTable);
  }

  if (monitoringIsEnabled) {
    mbus.connect();
  }
  dbus.connect();

  // starting upload thread(s)
  if (useCard && !cfg.notoflx)
  {
    INFO("Starting up " << toflx_threads.size() << " To_FLX threads");
    for(unsigned i=0; i<toflx_threads.size(); i++) {
      not_ready++;
      toflx_threads[i]->start();
      not_ready--;
      // FIXME: should we kill those threads at exit ?
    }
  }

  // starting busy thread
  if (useCard && !cfg.nobusy) {
    INFO("Starting Up " << busy_threads.size() << " Busy Threads");
    for(unsigned i=0; i<busy_threads.size(); i++) {
      not_ready++;
      busy_threads[i]->start();
      not_ready--;
    }
  }

  if (!cfg.noweb) {
    // FIXME should look up control network name
    INFO("Information " << (cfg.nostats ? "" : "and Statistics ") << "available from: " << "http://" << "localhost" << ":" << cfg.web_port << "/");
  }


  while (not_ready > 0) {
    INFO("Not Ready Threads: " << not_ready);
    sleep(1);
  }
  INFO("Final Not Ready Threads: " << not_ready);

  INFO("FelixCore Up and Running...");

  while (!QueueSource::populationHasStarted.load() || QueueSource::getSourceThreadsCounter())
	  sleep(10);

  ERROR("*********************************************");
  ERROR("FelixCore shutting down due to error above...");
  ERROR("*********************************************");

  process_threads_running.store(false);
  INFO("Stopping processing threads...");
  for(unsigned i=0; i<processing_threads.size(); i++) {
    processing_threads[i].join();
  }

  if (!cfg.nostats) {
//    statistics_thread_running.store(false);
//    INFO("Stopping statistics thread...");
//    statistics_thread.join();
  }

  INFO("Leaving webserver running, type ctrl-\\ to stop process.");
  while(true) {
    sleep(10);
  }

  // wait for webserver
  if (!cfg.noweb) {
    webServer.stop();
    webServerThread.join();
  }

  INFO("Deleting Queues and Buffer");
  // FIXME cleanup queues and buffer

  INFO("Disconnecting Bus");
  dbus.disconnect();

  if (monitoringIsEnabled) {
    mbus.disconnect();
  }
}
