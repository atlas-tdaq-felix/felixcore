#ifndef FELIXCORE_FILE_QueueSource_HPP
#define FELIXCORE_FILE_QueueSource_HPP

#include <iostream>

#include "packetformat/block_format.hpp"

#include "block_queue.hpp"

namespace felix {
namespace core {

class FileQueueSource : public QueueSource {
public:
	FileQueueSource(BlockQueue** buffers, BlockQueue** queues, size_t num_queues, std::istream& in) : QueueSource(0, buffers, queues, num_queues, 0) {
    char buf[felix::packetformat::BLOCKSIZE];
    while( in.read(buf, felix::packetformat::BLOCKSIZE) ) {
      const felix::packetformat::block *block = felix::packetformat::block_from_bytes(buf);
      elinks.insert(block->elink);
      block_ptrs.push_back(new felix::packetformat::block(*block));
    }

    next = std::begin(block_ptrs);

    queueSourceThread = std::thread(&QueueSource::runQueueSourceThread<FileQueueSource>, this);
  }

  virtual ~FileQueueSource() {
    block_ptrs.clear();
  }

  virtual bool read() {
    // FIXME get a higher throughput...
    for (int i=0; i< 100; i++) {
      felix::packetformat::block* block(*next);
      next++;
      if(next == std::end(block_ptrs)) {
        next = std::begin(block_ptrs);
      }

      dispatch(block);
    }

    return true;
  }

private:
  std::vector<felix::packetformat::block*> block_ptrs;
  std::vector<felix::packetformat::block*>::iterator next;
};

}
}

#endif
