#pragma once

#include "configuration.hpp"
#include "felixbase/queue.hpp"
#include "felixbase/logging.hpp"
#include <atomic>
#include <thread>
#include <fstream>
#include <time.h>
#include <ios>
#include <iomanip>

namespace felix{
namespace core{

class tracer {
private:
  typedef std::thread::id tid_t;
  typedef struct timespec timestamp_t;

  enum event_t { TOHOST_BLOCK_READ, FROMHOST_BLOCK_WRITE_START, FROMHOST_BLOCK_WRITE_COMPLETE, MSG_RECV, MSG_SEND, NUM_EVENTS };
  const char* EVENTNAMES[NUM_EVENTS] = {
    "TOHOST_BLOCK_READ",
    "FROMHOST_BLOCK_WRITE_START",
    "FROMHOST_BLOCK_WRITE_COMPLETE",
    "MSG_RECV",
    "MSG_SEND"
  };

  struct tracepoint {
    timestamp_t timestamp;
    tid_t tid;
    unsigned elink;
    event_t event;
  };

  tracer()
  {
    t0 = now();
    running.store(false);
    cfg = FelixConfiguration::instance().values();
  }

public:
  ~tracer()
  {
    stop();
  }

  static tracer& instance()
  {
    if(!m_instance)
      m_instance = std::unique_ptr<tracer>(new tracer());
    return *m_instance;
  }



  /** Traces blocks at the timepoint when they are read from the FLX */
  inline void trace_tohost_block_read(unsigned elink)
  {
    trace(TOHOST_BLOCK_READ, elink);
  }

  /** Traces blocks at the timepoint when they are written to the FLX */
  inline void trace_fromhost_block_write_start(unsigned elink)
  {
    trace(FROMHOST_BLOCK_WRITE_START, elink);
  }

  /** Traces blocks at the timepoint when has been written to the FLX */
  inline void trace_fromhost_block_write_complete(unsigned elink)
  {
    trace(FROMHOST_BLOCK_WRITE_COMPLETE, elink);
  }

  /** Traces messages at the timepoint when they are received from the network */
  inline void trace_msg_recv(unsigned elink)
  {
    trace(MSG_RECV, elink);
  }

  /** Traces messages at the timepoint when they are send to the NetIO stack */
  inline void trace_msg_send(unsigned elink)
  {
    trace(MSG_SEND, elink);
  }


  void run(std::string filename)
  {
    std::ofstream out(filename);
    while(running.load())
    {
      tracepoint tp;
      if(events.try_dequeue(tp))
      {
        std::ios::fmtflags f(out.flags());
        out << tp.timestamp.tv_sec << "." << std::setfill('0') << std::setw(9) << tp.timestamp.tv_nsec << ","
            << EVENTNAMES[tp.event] << ","
            << tp.elink << ","
            << tp.tid << "\n";
        out.flags(f);
        out.flush();
      }
      else
      {
        usleep(10000);
      }
    }
  }


  /**
   * Starts a background thread that writes trace events to the specified
   * output file.
   */
  void start(std::string filename)
  {
    running.store(true);
    bg_thread = std::thread(std::bind(&tracer::run, this, filename));
  }

  /**
   * Stop the background thread and join.
   */
  void stop()
  {
    if(running.load())
    {
      running.store(false);
      bg_thread.join();
    }
  }

private:
  static std::unique_ptr<tracer> m_instance;
  FelixConfiguration::Values cfg;
  moodycamel::BlockingConcurrentQueue<tracepoint> events;
  std::thread bg_thread;
  std::atomic_bool running;
  timestamp_t t0;


  /**
   * Current timestamp in microseconds
   */
  timestamp_t now()
  {
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts;
  }

  /**
   * Timestamp since start in microseconds
   */
  timestamp_t timestamp()
  {
    return now();
  }

  /**
   * Traces an event
   */
  inline void trace(event_t event, unsigned elink)
  {
    if(!cfg.trace)
      return;
    timestamp_t t = timestamp();
    tid_t tid = std::this_thread::get_id();
    tracepoint tp;
    tp.timestamp = t;
    tp.tid = tid;
    tp.elink = elink;
    tp.event = event;
    events.enqueue(tp);
  }
};

}
}
