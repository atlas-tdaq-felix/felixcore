#ifndef FELIX_CORE_PIPELINE_METADATA
#define FELIX_CORE_PIPELINE_METADATA

#include "netio/netio.hpp"
#include "block_source.hpp"
#include "StatsN.hpp"

namespace felix {
namespace core {

/**
 * Metadata for a chunk that is passed through a ChunkPipeline.
 */
struct ChunkMetadata
{
  ChunkMetadata(netio::publish_socket* socket, unsigned queueIndex, unsigned index, StatsN* stats) : socket(socket), queueIndex(queueIndex), index(index), stats(stats) {};

  unsigned elink;
  SmartBlockPtr last_block;
  netio::publish_socket* socket;
  unsigned queueIndex;
  unsigned index;
  StatsN* stats;
};

/**
 * Metadata for a block that is passed through a BlockPipeline.
 */
struct BlockMetadata
{
  BlockMetadata(unsigned queueIndex, unsigned index, StatsN* stats) : queueIndex(queueIndex), index(index), stats(stats) {};

  unsigned queueIndex;
  unsigned index;
  StatsN* stats;
};

}
}

#endif
