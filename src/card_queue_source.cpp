
#include "card_queue_source.hpp"

#if REGMAP_VERSION < 0x500
#define VERSION_MIN_MAJOR 4
#else
#define VERSION_MIN_MAJOR 5
#endif // REGMAP_VERSION
#define VERSION_MIN_MINOR 0

// non public include
using namespace felix::base;

void felix::core::CardQueueSource::resetInterruptCounters(void)
{
    flxCard.irq_reset_counters();
}

felix::core::CardQueueSource::CardQueueSource(unsigned int index, unsigned int deviceNo, BlockQueue** buffers, BlockQueue** queues, size_t num_queues) :
    QueueSource(index, buffers, queues, num_queues, deviceNo * MAX_ELINKS), deviceNo(deviceNo)
{
  // FIXME card needs to be initialized with min/max elink ids
  blockChecker = new felix::base::BlockChecker(0, MAX_ELINKS);

  // always publish given elinks
  if(isoneaway((char *)cfg.elink_numbers.c_str(), (char *)"random")
     || isoneaway((char *)cfg.elink_numbers.c_str(), (char *)"rand"))
  {
    std::vector<int> links = felix::base::populate_random();
    // BE AWARE: A Set object cannot contain duplicate elements.
    elinks.insert(begin(links), end(links));
  }
  else
  {
    std::set<unsigned int> cfg_elinks;
    felix::base::parse_range(cfg_elinks, cfg.elink_numbers);
    if (cfg_elinks.empty()) {
      WARNING("Elinks containing no elements at all - {}");
    } else {
      for (unsigned int elink: cfg_elinks) {
        if ((elink >= (deviceNo * MAX_ELINKS)) && (elink < ((deviceNo + 1) * MAX_ELINKS))) {
          elinks.insert(elink);
        }
      }
    }
  }

  try
  {
    INFO("Opening DEVICE " << deviceNo);
    flxCard.card_open(deviceNo, LOCK_DMA(cfg.dma));
  }
  catch(FlxException& ex)
  {
    ERROR("Exception thrown: " << ex.what());
    exit(EXIT_FAILURE);
  }
  INFO("DEVICE Opened");

  u_long cardModel = flxCard.card_model();
  if(cardModel != 712 && cardModel != 711 && cardModel != 709)
  {
    ERROR("Card model not recognized " << cardModel);
    flxCard.card_close();
    exit(EXIT_FAILURE);
  }
  INFO("CARD Model: " << cardModel);

  // Register map version stored as hex: 0x0300 -> 3.0
  u_long reg_map_version = flxCard.cfg_get_option(BF_REG_MAP_VERSION);
  u_long major = (reg_map_version & 0xFF00) >> 8;
  u_long minor = (reg_map_version & 0x00FF) >> 0;
  if ((major < VERSION_MIN_MAJOR) || (minor < VERSION_MIN_MINOR))
  {
    ERROR("Reg Map Version: " <<  major << "." << minor << " below minimum of " << VERSION_MIN_MAJOR << "." << VERSION_MIN_MINOR);
    flxCard.card_close();
    exit(EXIT_FAILURE);
  }
  INFO("Register Map Version: "<<  major << "." << minor);

  // save current state
#if REGMAP_VERSION < 0x500
  opt_emu_ena_to_host = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOHOST);
  opt_emu_ena_to_frontend = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOFRONTEND);
#else
  opt_emu_ena_to_host = flxCard.cfg_get_option(BF_FE_EMU_ENA_EMU_TOHOST);
  opt_emu_ena_to_frontend = flxCard.cfg_get_option(BF_FE_EMU_ENA_EMU_TOFRONTEND);
#endif // REGMAP_VERSION
  opt_tohost_fo = flxCard.cfg_get_option(BF_GBT_TOHOST_FANOUT_SEL);
  opt_tofrontend_fo = flxCard.cfg_get_option(BF_GBT_TOFRONTEND_FANOUT_SEL);

  blockSize = flxCard.cfg_get_option( BF_BLOCKSIZE );
  if( blockSize == 0 )
    blockSize = BLOCK_SIZE;
  trailerIs32Bit = (flxCard.cfg_get_option( BF_CHUNK_TRAILER_32B ) == 1);
#if REGMAP_VERSION < 0x500
  fromHostDataFormat = 0;
#else
  fromHostDataFormat = flxCard.cfg_get_option( BF_FROMHOST_DATA_FORMAT );
#endif // REGMAP_VERSION
  numberOfDescriptors = flxCard.cfg_get_option( BF_GENERIC_CONSTANTS_DESCRIPTORS );

  // allocate CMEM buffer
  unsigned memory = cfg.memory;
  unsigned megabyte = cfg.megabyte;
  memSize = memory == 0 ? 512 * Constant::Mega : memory * Constant::Giga;
  memSize = megabyte == 0 ? memSize : megabyte * Constant::Mega;
  printf("MNEMORY: %lu\n", memSize);
  if (memSize % blockSize != 0) {
    ERROR("ERROR memSize (" << memSize << ") not a multiple of FLX block size (" << blockSize << ")");
    exit(1);
  }

  numberOfBlocks = memSize / blockSize;

  reset();

  queueSourceThread = std::thread(&QueueSource::runQueueSourceThread<CardQueueSource>, this);
}

void felix::core::CardQueueSource::reset() {
  // stop all DMA
  uint number_of_descriptors = flxCard.cfg_get_option(BF_GENERIC_CONSTANTS_DESCRIPTORS);
  for(uint i = 0; i < number_of_descriptors; i++) {
    flxCard.dma_stop(i);
  }

  if (!cfg.polling) {
    flxCard.irq_disable();
  }

  // set DMA and Emulators
  flxCard.dma_reset();
  flxCard.soft_reset();

  // enable interrupts or polling
  if (cfg.polling) {
    INFO("Using busy-wait: " << cfg.poll_time << " ms.");
  } else {
    INFO("Using interrupts");
    // FIXME: no longer needed
    if ((card_fd = ::open("/dev/flx", O_RDWR)) < 0) {
      ERROR("Cannot open /dev/flx");
      exit(1);
    }
    resetInterruptCounters();

#if REGMAP_VERSION < 0x500
    flxCard.irq_enable(IRQ_DATA_AVAILABLE);
#else
    // An interrupt per ToHost DMA controller
    flxCard.irq_enable(IRQ_DATA_AVAILABLE + cfg.dma);
#endif // REGMAP_VERSION
  }

  INFO("Setup DMA: " << cfg.dma);
  circular_dma = std::make_unique<CircularDMA>(flxCard, cfg.dma, memSize);

  if (!cfg.nostats) {
    nstats.setBlocksFree(index, circular_dma->unused_bytes()/blockSize);
  }
}

felix::core::CardQueueSource::~CardQueueSource()
{
  // reset to initial state
#if REGMAP_VERSION < 0x500
  flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, opt_emu_ena_to_host);
  flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, opt_emu_ena_to_frontend);
#else
  flxCard.cfg_set_option(BF_FE_EMU_ENA_EMU_TOHOST, opt_emu_ena_to_host);
  flxCard.cfg_set_option(BF_FE_EMU_ENA_EMU_TOFRONTEND, opt_emu_ena_to_frontend);
#endif // REGMAP_VERSION
  flxCard.cfg_set_option(BF_GBT_TOHOST_FANOUT_SEL, opt_tohost_fo);
  flxCard.cfg_set_option(BF_GBT_TOFRONTEND_FANOUT_SEL, opt_tofrontend_fo);

  // close card
  INFO("Closing DEVICE " << deviceNo);
  flxCard.card_close();

  delete blockChecker;
}


bool felix::core::CardQueueSource::read() {
  // update stats
  if (!cfg.nostats) {
	  nstats.setBlocksFree(index, circular_dma->unused_bytes()/blockSize);
  }

  circular_dma->wait(blockSize);
  size_t bytes_to_consume = circular_dma->bytes_available_to_read();
  bytes_to_consume -= bytes_to_consume % blockSize;
  if(bytes_to_consume % blockSize != 0)
  {
    ERROR("DMA transfer truncated");
    return false;
  }

  for(unsigned i = 0; i < bytes_to_consume; i += blockSize)
  {
    const Block* block = felix::packetformat::block_from_bytes((const char*)circular_dma->ptr()+i);
    if (!blockChecker->checkSignature(block)) {
      ERROR("No Valid Signature found in block");
      dump_buffer((u_long)block, blockSize);
      return false;
    }

    int expected = blockChecker->checkSeqNo(block);
    if (expected >= 0) {
      ERROR("Wrong Sequence Number found in block, found " << block->seqnr <<
           " instead of  " << expected <<
           " for elink " << hex(block->elink));
      dump_buffer((u_long)block, blockSize);
      if (!cfg.nostats) {
        nstats.seqNoIncr((unsigned)expected, block->seqnr);
      }
      // continue, block should still be ok
    }

    dispatch(block);
  }

  circular_dma->consume(bytes_to_consume);
  if (!cfg.nostats) {
    nstats.blocksPerTransferInrc(bytes_to_consume/blockSize);
  }

  return true;
}
