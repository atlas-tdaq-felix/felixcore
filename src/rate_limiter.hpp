#ifndef FELIXCORE_RateLimiter_HPP
#define FELIXCORE_RateLimiter_HPP

#include <cstdint>
#include <iostream>
#include <time.h>
typedef std::uint64_t timestamp_t;


namespace felix
{
namespace core
{
class RateLimiter
{
public:
  RateLimiter()
  {
  }
  timestamp_t gettime()
  {
    ::timespec ts;
    ::clock_gettime(CLOCK_MONOTONIC, &ts);
    return timestamp_t(ts.tv_sec) * s + timestamp_t(ts.tv_nsec) * ns;
  }

private:

  static const timestamp_t ns = 1;
  static const timestamp_t us = 1000 * ns;
  static const timestamp_t ms = 1000 * us;
  static const timestamp_t s  = 1000 * ms;
};
}
}

#endif
