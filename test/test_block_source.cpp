#include <sstream>

#include <catch2/catch_test_macros.hpp>
#include "block_source.hpp"

using namespace felix::core;


TEST_CASE( "Read block from file_block_source", "[block_source]" )
{
  /* To be redone...
  char b[felix::packetformat::BLOCKSIZE];
  felix::packetformat::block* b_ptr = reinterpret_cast<felix::packetformat::block*>(b);
  b_ptr->elink = 5;
  b_ptr->data[7] = 'Q';

  std::stringstream s;
  for(unsigned i=0; i < felix::packetformat::BLOCKSIZE; i++)
    {
      s << b[i];
    }

  file_block_source source(s);
  SmartBlockPtr p = source.pop();

  REQUIRE( (*p).data[7] == 'Q' );
  REQUIRE( (*p).elink == 5 );
  REQUIRE( p.use_count() == 2 );
  */
}


TEST_CASE( "Read two identical blocks from file_block_source", "[block_source]" )
{
  /* To be redone...
  char b[felix::packetformat::BLOCKSIZE];
  b[4] = '3';

  std::stringstream s;
  for(unsigned i=0; i < felix::packetformat::BLOCKSIZE; i++)
    {
      s << b[i];
    }

  file_block_source source(s);
  SmartBlockPtr p1 = source.pop();
  REQUIRE( p1.use_count() == 2 );

  SmartBlockPtr p2 = source.pop();

  (*p1).data[3] = '1';
  (*p2).data[3] = '2';

  REQUIRE( p2.use_count() == 3 );

  REQUIRE( (*p1).data[3] == '2' );
  REQUIRE( (*p1).data[0] == '3' );
  */
}

/* To be redone...
static char
first_data_char(SmartBlockPtr p)
{
  REQUIRE( p.use_count() == 3 );
  return (*p).data[0];
}
*/

TEST_CASE( "Pass SmartBlockPtr as parameter", "[block_source]" )
{
  /* To be redone...
  char b[felix::packetformat::BLOCKSIZE];

  std::stringstream s;
  for(unsigned i=0; i < felix::packetformat::BLOCKSIZE; i++)
    {
      s << b[i];
    }

  file_block_source source(s);
  SmartBlockPtr p = source.pop();
  (*p).data[0] = 'A';

  REQUIRE( first_data_char(p) == 'A' );
  REQUIRE( p.use_count() == 2);
  */
}
