#include <catch2/catch_test_macros.hpp>
#include "flxwrapper.hpp"

using namespace felix::core;

TEST_CASE( "Create FLX object", "[.][flxwrapper]" ) {
  Flx flx(0, LOCK_NONE);
}

TEST_CASE( "Create CMEM buffer", "[.][flxwrapper]" ) {
  CmemBuffer buf(1024, "test");
}

TEST_CASE( "Simple DMA transfer", "[.][flxwrapper]" ) {
  Flx flx(0, LOCK_DMA0);
  CmemBuffer cmembuf0(4096, "test0");
  CmemBuffer cmembuf1(4096, "test1");

  uint8_t* buf0 = cmembuf0.access_ptr();

  buf0[0] = 0xFF;
  buf0[1] = 0xAB;
  buf0[2] = 0xBC;

  FlxSimpleDMA dma(flx, 0);
  dma.transfer_from_host(cmembuf0, 0, 4096);
  dma.transfer_to_host(cmembuf1, 0, 4096);
}

TEST_CASE( "Circular buffer DMA transfer", "[.][flxwrapper]" ) {
  Flx flx(0, LOCK_DMA0);
  CmemBuffer cmembuf0(4096, "test0");

  FlxCircularBufferDMA circularbuf(flx, 0, cmembuf0, FlxCircularBufferDMA::FROM_HOST);
  uint8_t* buf = cmembuf0.access_ptr();

  REQUIRE( circularbuf.current_pos() == buf );
  REQUIRE( circularbuf.bytes_available() == 4096 );
  buf[0] = 0xCC;
  buf[1] = 0xDD;
  circularbuf.advance(2);
  REQUIRE( (circularbuf.current_pos()-buf) == 2 );
  size_t bytes_available = circularbuf.bytes_available();
  CAPTURE(bytes_available);
  REQUIRE( 4094 <= bytes_available );
  REQUIRE( bytes_available <= 4096 );
}
