#include <sstream>
#include <ostream>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include "configuration.hpp"

TEST_CASE( "Parse configuration file", "[configuration]" ) {
        char fname[] = "config-XXXXXX";
        int fd = mkstemp(fname);
        std::ofstream file(fname);
        file << "threads: 5" << std::endl
             << "debug.dump-chunks: true" << std::endl
             << "debug.file-input: test.dat" << std::endl;

        char* args[] = { (char *)"felixcore", (char *)"--config", (char *)fname, NULL };

        felix::core::FelixConfiguration& cfg = felix::core::FelixConfiguration::instance();
        cfg.clear();
        cfg.parse_args(3, args);

        auto values = cfg.values();

        REQUIRE( values.threads == 5 );
        REQUIRE( values.debug_dump_chunks == true );
        REQUIRE( values.file == "test.dat");

        close(fd);
        unlink(fname);
}


TEST_CASE( "Parse override configuration file", "[configuration]" ) {
	char fname[] = "config-XXXXXX";
	int fd = mkstemp(fname);
	std::ofstream file(fname);
	file << "debug.dump-chunks: true" << std::endl
		   << "debug.file-input: test.dat" << std::endl
			 << "threads: 5" << std::endl
	     << "dma: 5" << std::endl
	     << "memory: 5" << std::endl;

	// Note: override currently only works with 'long' options
	char* args[] = { (char *)"felixcore",
	                 (char *)"--config", (char *)fname,
									 (char *)"--threads", (char *)"2",
									 (char *)"--dma", (char *)"0",
									 (char *)"-m", (char *)"2",
									 NULL };

	felix::core::FelixConfiguration& cfg = felix::core::FelixConfiguration::instance();
	cfg.clear();
	cfg.parse_args(9, args);

	auto values = cfg.values();

	REQUIRE( values.debug_dump_chunks == true );
	REQUIRE( values.file == "test.dat");
	REQUIRE( values.threads == 2 );
	REQUIRE( values.dma == 0 );
	REQUIRE( values.memory == 2 );

	close(fd);
	unlink(fname);
}


TEST_CASE( "Test default values", "[configuration]" ) {
	char* args[] = { (char *)"felixcore", NULL };

	felix::core::FelixConfiguration& cfg = felix::core::FelixConfiguration::instance();
	cfg.clear();
	cfg.parse_args(1, args);

	auto values = cfg.values();

	REQUIRE( values.threads == 1 );
	REQUIRE( values.debug_dump_chunks == false );
	REQUIRE( values.file == "");
}

TEST_CASE( "Test command line parameters", "[configuration]") {
	char* args[] = { (char *)"felixcore", (char *)"-t", (char *)"5", NULL };

	felix::core::FelixConfiguration& cfg = felix::core::FelixConfiguration::instance();
	cfg.clear();
	cfg.parse_args(3, args);

	auto values = cfg.values();

	REQUIRE( values.threads == 5 );
	REQUIRE( values.debug_dump_chunks == false );
	REQUIRE( values.file == "");
}
