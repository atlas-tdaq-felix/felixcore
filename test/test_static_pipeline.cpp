#include <catch2/catch_test_macros.hpp>
#include <functional>

#include "static_pipeline.hpp"


template<typename Packet, typename Metadata>
struct NullPipelineElement
{
  static void process(const Packet, Metadata)	{}
};

template<typename Packet>
struct CounterPipelineElement
{
  static void process(const Packet, unsigned& count)
  {
    count++;
  }
};


TEST_CASE( "Simple Static Pipeline", "[pipeline]" )
{
  typedef std::string Packet;
  typedef int Metadata;
  typedef StaticPipeline<Packet, Metadata, NullPipelineElement<Packet, Metadata>, NullPipelineElement<Packet, Metadata>>
      Pipeline;

  Pipeline pipeline;
  std::string data("Hello, world!");
  pipeline.process(std::string("Hello, world!"), 0);

  REQUIRE( true );
}


TEST_CASE( "Simple Static Pipeline with Data", "[pipeline]" )
{
  typedef unsigned& Metadata;
  typedef std::string Packet;
  typedef StaticPipeline<Packet, Metadata, CounterPipelineElement<Packet>> Pipeline;

  Pipeline pipeline;
  unsigned count = 0;
  pipeline.process(std::string("Hello, world!"), std::ref(count));

  REQUIRE( count == 1 );
}
